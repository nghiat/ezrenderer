from subprocess import call
import glob

cppFiles = [file for file in glob.glob('src' + '/**/*.cpp', recursive=True)]
hFiles = [file for file in glob.glob('include' + '/**/*.h', recursive=True)]

for file in cppFiles:
    call(["clang-format", "-i", "-style=file", file])
for file in hFiles:
    call(["clang-format", "-i", "-style=file", file])
