#ifndef TEST_EZRENDERER_MYAPP_H
#define TEST_EZRENDERER_MYAPP_H

#include <Base/Base.h>
#include <UniformBufferObject.h>
#include <GLAPI/GLShader.h>
#include <GLAPI/GLBuffer.h>
#include <GLAPI/GLTextureCube.h>
#include <GLAPI/GLRenderTarget.h>
#include <Util/Model.h>
#include <memory>

class MyApp : public Ez::Base {
public:
  MyApp(int width, int heigth, std::string title)
      : Ez::Base(Ez::Base::Renderer::GL, width, heigth, title){};

  void InitShaders();

  void InitObjects();
  void Init() override;
  void Update() override;
  void Draw() override;
  void InitTexture();

  void MouseButtonCallback(Mouse button, Action action) override;

  void PointerPosCallback(double xPos, double yPos) override;

  void ScrollCallback(double xOffset, double yOffset) override;

  void ResizeCallback(int width, int height) override;

private:
  void DrawSkybox();

  //Draw the model
  void DrawModel();

  void DrawAxisAlignedQuad();
  //Downsample 2x in each dimension
  void Downsample(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Downsample 4x in each dimension
  void Downsample4x(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Extract highlight area
  void ExtractHL(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Apply vertical and horizontal gaussian blur
  void Blur(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des, std::unique_ptr<Ez::GLRenderTarget>& temp, float radius, float s);

  //Compose all the blur framebuffers
  void ComposeEffect();

  //Map the original scene with the post-processed scene
  void ToneMappingPass();

  std::unique_ptr<Ez::GLTextureCube> m_SkyboxTexture;

  std::shared_ptr<Ez::GLBufferPool> m_BufferPool;

  std::shared_ptr<Ez::GLBufferPool> m_UniformBufferPool;

  std::unique_ptr<Ez::GLBuffer> m_SkyboxVertexBuffer;

  std::unique_ptr<Ez::GLBuffer> m_SkyboxIndexBuffer;

  std::unique_ptr<Ez::GLShader> m_ModelShaders;

  std::unique_ptr<Ez::GLShader> m_SkyboxShaders;

  std::unique_ptr<Ez::GLShader> m_DownsampleShaders;

  std::unique_ptr<Ez::GLShader> m_Downsample4xShaders;

  std::unique_ptr<Ez::GLShader> m_ExtractHLShaders;

  std::unique_ptr<Ez::GLShader> m_BlurShaders;

  std::unique_ptr<Ez::GLShader> m_GaussianComposeShaders;

  std::unique_ptr<Ez::GLShader> m_GlareComposeShaders;

  std::unique_ptr<Ez::GLShader> m_ToneMappingShaders;

  std::shared_ptr<Ez::GLBuffer> m_ModelVertUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_ModelFragUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_SkyboxVertUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_Downsample4xVertUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_ExtractHightLightFragUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_BlurFragUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_GaussianFragUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_GlareFragUBuffer;

  std::shared_ptr<Ez::GLBuffer> m_ToneMappingFragUBuffer;
  std::unique_ptr<Ez::GLBuffer> m_QuadBuffer;
  std::unique_ptr<Ez::GLBuffer> m_QuadUVBuffer;

  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_PosBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_NormalBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_IndexBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_ColorBuffers;

  std::unique_ptr<Ez::GLRenderTarget> m_MainRT;
  std::unique_ptr<Ez::GLRenderTarget> m_FinalRT;
  std::uint32_t m_NumLevels = 4;
  std::unique_ptr<Ez::GLRenderTarget> m_Downsample4xRT;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_ComposeRTs;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_BlurRTs;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_TempRTs;

  std::unique_ptr<Ez::GLBuffer> m_ModelBuffer;
  std::vector<std::unique_ptr<Ez::GLBuffer>> m_ModelIndexBuffers;
  std::vector<size_t> m_ModelIndicesNums;

  ModelVertUBO m_ModelVertUBO;
  ModelFragUBO m_ModelFragUBO;
  SkyboxVertUBO m_SkyboxVertUBO;
  Downsample4xVertUBO m_Downsample4xVertUBO;
  ExtractHightLightFragUBO m_ExtractHightLightFragUBO;
  BlurFragUBO m_BlurFragUBO;
  GaussianFragUBO m_GaussianFragUBO;
  GlareFragUBO m_GlareFragUBO;
  ToneMappingFragUBO m_ToneMappingFragUBO;
  bool m_EnableHDR = true;

  //3.0f / bloom width
  float m_S[4] = { 0.42f, 0.25f, 0.15f, 0.10f };

  //Vars for transformations
  glm::vec3 m_Eye, m_CenterLook, m_Up, m_RotationAxis1, m_RotationAxis2;
  glm::mat4 m_ModelMatrix, m_ViewMatrix, m_PerspectiveMatrix, m_MVPMatrix;
  
  GLuint m_VAO;
  bool m_IsMouseClicked = false;
  double m_MouseX = 0.0;
  double m_MouseY = 0.0;
  float m_Exposure = 5.f;
};

#endif // TEST_EZRENDERER_MYAPP_H
