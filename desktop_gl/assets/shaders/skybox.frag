in vec3 fragUV;

out vec4 outColor;

uniform samplerCube uSkybox;

void main(){
	outColor = texture(uSkybox, fragUV);
}
