in vec2 fragUV;

uniform sampler2D uSampler;

out vec4 outColor;

void main(){
	outColor = texture(uSampler, fragUV);
}
