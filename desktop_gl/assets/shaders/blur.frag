in vec2 fragUV;
out vec4 outColor;

uniform sampler2D sampler;
layout (std140) uniform UBlurFrag {
    vec2 uDir;
    float uResolution;
    float uRadius;
    float uS;
};

float gaussian(float x, float sd){
	return exp(-(x*sd) * (x*sd));
}

void main(){
	vec4 sum = vec4(0.0);
	float blur = 1 / uResolution;
	float hstep = uDir.x;
	float vstep = uDir.y;
	float gauSum = 0.0;
	for(int i = 0; i < 2 * uRadius + 1; i++){
		gauSum += gaussian(i - uRadius, uS);
	}
    outColor = vec4(sum.rgb, 1.0);
    sum+= texture(sampler, fragUV) * gaussian(0, uS) / gauSum;
    for(int i = 1; i <= uRadius; i++){
    	sum += texture(sampler, vec2(fragUV.x - i*blur*hstep, fragUV.y - i*blur*vstep)) * gaussian(i, uS) / gauSum;
    	sum += texture(sampler, vec2(fragUV.x + i*blur*hstep, fragUV.y + i*blur*vstep)) * gaussian(i, uS) / gauSum;
    }
    outColor = vec4(sum.rgb, 1.0);
}
