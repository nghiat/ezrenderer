layout(location = 0) in vec3 aPos;

layout (std140) uniform USkyboxVert {
    mat4 uViewMatrix;
    mat4 uProjectMatrix;
};

out vec3 fragUV;

void main(){
	fragUV = aPos;
	gl_Position =  uProjectMatrix * uViewMatrix * vec4(aPos, 1.0);
}
