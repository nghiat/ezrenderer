layout(location=0) in vec4 aPoint;
layout(location=1) in vec2 aUV;

out vec2 uv;

void main() {
  gl_Position = aPoint;
  uv = aUV;
}
