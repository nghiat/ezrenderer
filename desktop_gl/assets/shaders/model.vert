layout(location = 0) in vec4 aColor;
layout(location = 1) in vec3 aPos;
layout(location = 2) in vec3 aNormal;

out vec3 fragPos;
out vec3 fragNormal;
out vec4 fragColor;
out vec3 fragReflect;
out vec3 fragRefract;

layout (std140) uniform UModelVert {
    mat4 uMVP;
    mat4 uViewMatrix;
    vec3 uFEye;
    float uEta;
};

void main(){
	fragPos = aPos;
	fragNormal = normalize(aNormal);
	fragColor = aColor;
	fragReflect = reflect(normalize(aPos - uFEye), fragNormal);
	fragRefract = refract(normalize(aPos - uFEye), fragNormal, uEta);

	gl_Position = uMVP * vec4(aPos, 1.0);
}
