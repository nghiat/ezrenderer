uniform samplerCube uSkybox;

layout (std140) uniform UModelFrag {
    vec4 uPhong;
    vec3 uVEye;
    float uFactor;
    vec3 uLightPos;
    int uMode;
    vec3 uLightIntensities; //light color
    bool uEvMapping;
};

in vec3 fragPos;
in vec3 fragNormal;
in vec4 fragColor;
in vec3 fragReflect;
in vec3 fragRefract;
out vec4 outColor;

float my_fresnel(vec3 I, vec3 N, float power,  float scale,  float bias)
{
    return max(0.0, min(1, bias + scale * pow(1.0 + dot(I, N), power)));
}

void main(){
	vec3 reflectColor = texture(uSkybox, fragReflect).rgb;
	vec3 refractColor = texture(uSkybox, fragRefract).rgb;
	vec3 tranformedVector = fragNormal;
	vec3 surfaceToLight = normalize(fragPos - uLightPos);
	vec3 surfaceToEye = normalize(fragPos - uVEye);
	float specular = pow(max(0.0, -dot(reflect(surfaceToLight, tranformedVector), surfaceToEye)), uPhong.w) * uPhong.z;
	float diffuse = max(0.0,dot(surfaceToLight, tranformedVector)) * uPhong.y;
	float ambient = uPhong.x;
	if(!uEvMapping || uMode == 0){
		outColor = vec4(fragColor.rgb * (diffuse + ambient) + specular * uLightIntensities, fragColor.a);
	}
	else{
		switch (uMode){
			case 1: outColor = vec4(mix(fragColor.rgb, reflectColor, uFactor), 1.0); break;
			case 2: outColor = vec4(mix(fragColor.rgb, refractColor, uFactor), 1.0); break;
			case 3: outColor = vec4(mix(refractColor, reflectColor, my_fresnel(surfaceToLight, tranformedVector, 5.0, 1.0, 0.1)), 1.0); break;
		}
	}
}
