in vec2 fragUV;
out vec4 outColor;

uniform sampler2D uSampler;
layout (std140) uniform UGlareFrag {
    float uMixCoeff;
};

void main()
{
	outColor = texture(uSampler, fragUV) * uMixCoeff;
}
