in vec2 fragUV;
out vec4 outColor;

uniform sampler2D uSceneTex;
uniform sampler2D uBlurTex;

layout (std140) uniform UToneMappingFrag {
    float uBlurAmount;
    float uExposure;
    float uGamma;
};

float A = 0.15f;
float B = 0.50f;
float C = 0.10f;
float D = 0.20f;
float E = 0.02f;
float F = 0.30f;
float W = 11.2f;

vec3 filmicTonemapping(vec3 x)
{
  return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E/F;
}

float vignette(vec2 pos, float inner, float outer)
{
  float r = length(pos);
  r = 1.0 - smoothstep(inner, outer, r);
  return r;
}

void main()
{
  vec4 scene = texture(uSceneTex, fragUV);
  vec4 blurred = texture(uBlurTex, fragUV);
  vec3 c = mix(scene.rgb, blurred.rgb, uBlurAmount);
  c = c * uExposure / 0.5;
  c = c * vignette(fragUV * 2.0 - 1.0, 0.55, 1.5);
  float uExposureBias = 1.0;
  c = filmicTonemapping(uExposureBias * c);
  vec3 whiteScale = 1.0 / filmicTonemapping(vec3(W, W, W));
  c = c * whiteScale;
  c.r = pow(c.r, uGamma);
  c.g = pow(c.g, uGamma);
  c.b = pow(c.b, uGamma);
  outColor = vec4(c, 1.0);
}
