#include "MyApp.h"
#include "CubeData.h"
#include <GLAPI/GLBuffer.h>
#include <GLAPI/GLBufferPool.h>
#include <Util/Log.h>
#include <Util/Model.h>
#include <Util/TextureCube.h>
#include <glm/gtc/matrix_transform.hpp>
#include <math.h>

#define PI 3.14159265359f

using namespace Ez;

Ez::Base *base = new MyApp(800, 800, "EzRenderer GL");

void MyApp::Init() {
  Base::Init();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  m_BufferPool = std::make_shared<Ez::GLBufferPool>();
  m_UniformBufferPool = std::make_shared<GLBufferPool>(GL_UNIFORM_BUFFER);
  InitTexture();
  InitShaders();
  InitObjects();
}
void MyApp::Update() {}

void MyApp::Draw() {
  if (m_ModelFragUBO.evMapping) {
    if (m_EnableHDR) {
      m_MainRT->BindFramebuffer();
    } else {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glViewport(0, 0, m_Width, m_Height);
    DrawSkybox();
    DrawModel();

    if (m_EnableHDR) {
      Downsample4x(m_MainRT, m_Downsample4xRT);
      ExtractHL(m_Downsample4xRT, m_ComposeRTs[0]);
      Blur(m_ComposeRTs[0], m_BlurRTs[0], m_TempRTs[0],
           float(int(3.0f / m_S[0])), m_S[0]);

      for (int i = 1; i < m_NumLevels; i++) {
        Downsample(m_ComposeRTs[i - 1], m_ComposeRTs[i]);
        Blur(m_ComposeRTs[i], m_BlurRTs[i], m_TempRTs[i],
             float(int(3.0f / m_S[i])), m_S[i]);
      }

      ComposeEffect();

      ToneMappingPass();
    }
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawModel();
  }
}

void MyApp::InitTexture() {
  auto skybox = std::make_unique<Ez::TextureCube>("textures/altar_cross_mmp_s.hdr");
  m_SkyboxTexture = std::make_unique<Ez::GLTextureCube>(skybox.get());

  m_SkyboxVertexBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sizeof(SkyboxVertices), SkyboxVertices);
  m_SkyboxIndexBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sizeof(SkyboxIndices), SkyboxIndices);
}

void MyApp::MouseButtonCallback(Mouse button, Action action) {
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    m_IsMouseClicked = true;
  }
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
    m_IsMouseClicked = false;
    m_MouseX = m_MouseY = 0.0;
  }
}

void MyApp::PointerPosCallback(double xPos, double yPos) {
  if (m_IsMouseClicked) {
    if (m_MouseX == 0.0 && m_MouseY == 0.0) {
      m_MouseX = xPos;
      m_MouseY = yPos;
    }
    float deltaX = static_cast<float>((xPos - m_MouseX) / m_Width);
    float deltaY = static_cast<float>((yPos - m_MouseY) / m_Height);
    m_MouseX = xPos;
    m_MouseY = yPos;

    m_RotationAxis2 =
        glm::vec3(glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
                  glm::vec4(m_RotationAxis2, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaX * 2 * PI, m_RotationAxis1);
    m_Eye = glm::vec3(glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
                    glm::vec4(m_Eye, 1.0f));

    m_RotationAxis1 =
        glm::vec3(glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
                  glm::vec4(m_RotationAxis1, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaY * 2 * PI, m_RotationAxis2);
    m_Eye = glm::vec3(glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
                    glm::vec4(m_Eye, 1.0f));

    m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;

    m_ModelVertUBO.mvp = m_MVPMatrix;
    m_ModelVertUBO.viewMatrix = m_ViewMatrix;
    m_ModelVertUBO.eye = m_Eye;
    m_ModelVertUBuffer->UpdateData(0, sizeof(m_ModelVertUBO), &m_ModelVertUBO);

    m_ModelFragUBO.eye = m_Eye;
    m_ModelFragUBuffer->UpdateData(0, sizeof(ModelFragUBO), &m_ModelFragUBO);

    m_SkyboxVertUBO.viewMatrix = m_ViewMatrix;
    m_SkyboxVertUBuffer->UpdateData(0, sizeof(glm::mat4), &m_SkyboxVertUBO);
  }
}

void MyApp::ScrollCallback(double xOffset, double yOffset) {
  float zoomFactor = 1;
  if (yOffset < 0.0) {
    zoomFactor = 1.0f / (1.0f - float(yOffset) * 0.05f);
  } else {
    zoomFactor += 0.05f * float(yOffset);
  }
  m_ModelMatrix =
      glm::scale(m_ModelMatrix, glm::vec3(zoomFactor, zoomFactor, zoomFactor));
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
  m_ModelVertUBO.mvp = m_MVPMatrix;
  m_ModelVertUBuffer->UpdateData(0, sizeof(glm::mat4), &m_ModelVertUBO);
}

void MyApp::ResizeCallback(int width, int height) {
  float aspect = float(width * 1.0f / height);
  m_Width = width;
  m_Height = height;
  m_PerspectiveMatrix = glm::perspective(45.0f, aspect, 0.1f, 100.0f);
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
  m_ModelVertUBuffer->UpdateData(0, sizeof(glm::mat4), &m_MVPMatrix);
  m_SkyboxVertUBuffer->UpdateData(sizeof(glm::mat4), sizeof(glm::mat4),
                                  &m_PerspectiveMatrix);
  glViewport(0, 0, GLsizei(m_Width), GLsizei(m_Height));
  m_MainRT->Resize(width, height);
}

void MyApp::InitShaders() {
  m_RotationAxis1 = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ModelMatrix = glm::mat4(1.0f);
  m_Eye = glm::vec3(7.0f);
  m_CenterLook = glm::vec3(0.0f);
  m_Up = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ViewMatrix = glm::lookAt(m_Eye, m_CenterLook, m_Up);
  m_RotationAxis2 = cross(m_CenterLook - m_Eye, m_RotationAxis1);
  m_PerspectiveMatrix =
      glm::perspective(45.0f, float(m_Width * 1.0f / m_Height), 0.1f, 1000.0f);
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;

  m_ModelShaders =
      std::make_unique<GLShader>("shaders/model.vert", "shaders/model.frag");
  m_ModelVertUBO = {m_MVPMatrix, m_ViewMatrix, m_Eye, 1.f};
  m_ModelVertUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(m_ModelVertUBO), &m_ModelVertUBO);
  m_ModelFragUBO = {glm::vec4(0.5f, 0.6f, 0.4f, 16.0f),
                    m_Eye,
                    1.0f,
                    glm::vec3(10.f),
                    1,
                    glm::vec3(1.f),
                    true};
  m_ModelFragUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(m_ModelFragUBO), &m_ModelFragUBO);
  m_ModelShaders->BindBuffer(m_ModelVertUBuffer, "UModelVert");
  m_ModelShaders->BindBuffer(m_ModelFragUBuffer, "UModelFrag");

  m_SkyboxShaders =
      std::make_unique<GLShader>("shaders/skybox.vert", "shaders/skybox.frag");
  m_SkyboxVertUBO = {m_ViewMatrix, m_PerspectiveMatrix};
  m_SkyboxVertUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(SkyboxVertUBO), &m_SkyboxVertUBO);
  m_SkyboxShaders->BindBuffer(m_SkyboxVertUBuffer, "USkyboxVert");

  m_DownsampleShaders = std::make_unique<GLShader>("shaders/downsample.vert",
                                                   "shaders/downsample.frag");

  m_Downsample4xShaders = std::make_unique<GLShader>(
      "shaders/downsample4x.vert", "shaders/downsample4x.frag");
  m_Downsample4xVertUBO.texelSize = glm::vec2(2.f / m_Width, 2.f / m_Height);
  m_Downsample4xVertUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(Downsample4xVertUBO), &m_Downsample4xVertUBO);
  m_Downsample4xShaders->BindBuffer(m_Downsample4xVertUBuffer,
                                    "UDownsample4xVert");

  m_ExtractHLShaders = std::make_unique<GLShader>("shaders/extractHL.vert",
                                                  "shaders/extractHL.frag");
  m_ExtractHightLightFragUBO = {1.f, .3f};
  m_ExtractHightLightFragUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(ExtractHightLightFragUBO),
      &m_ExtractHightLightFragUBO);
  m_ExtractHLShaders->BindBuffer(m_ExtractHightLightFragUBuffer,
                                 "UExtractHLFrag");

  m_BlurShaders =
      std::make_unique<GLShader>("shaders/blur.vert", "shaders/blur.frag");
  m_BlurFragUBuffer = std::make_shared<GLBuffer>(m_UniformBufferPool,
                                                 sizeof(BlurFragUBO), nullptr);
  m_BlurShaders->BindBuffer(m_BlurFragUBuffer, "UBlurFrag");

  m_GaussianComposeShaders = std::make_unique<GLShader>(
      "shaders/gaussianCompose.vert", "shaders/gaussianCompose.frag");
  m_GaussianFragUBO.coeff = glm::vec4(0.3f, 0.3f, 0.25f, 0.20f);
  m_GaussianFragUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(GaussianFragUBO), &m_GaussianFragUBO);
  m_GaussianComposeShaders->BindBuffer(m_GaussianFragUBuffer, "UGaussianFrag");

  m_GlareComposeShaders = std::make_unique<GLShader>(
      "shaders/glareCompose.vert", "shaders/glareCompose.frag");
  m_GlareFragUBO.mixCoeff = 1.2f;
  m_GlareFragUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(GlareFragUBO), &m_GlareFragUBO);
  m_GlareComposeShaders->BindBuffer(m_GlareFragUBuffer, "UGlareFrag");

  m_ToneMappingShaders = std::make_unique<GLShader>("shaders/toneMapping.vert",
                                                    "shaders/toneMapping.frag");
  // adaptive exposure adjustment in log space
  float newExp = (float)(10.0f * log(m_Exposure + 0.0001));
  m_ToneMappingFragUBO = {0.33f, newExp, 1.f / 1.8f};
  m_ToneMappingFragUBuffer = std::make_shared<GLBuffer>(
      m_UniformBufferPool, sizeof(ToneMappingFragUBO), &m_ToneMappingFragUBO);
  m_ToneMappingShaders->BindBuffer(m_ToneMappingFragUBuffer,
                                   "UToneMappingFrag");
}

void MyApp::InitObjects() {
  glGenVertexArrays(1, &m_VAO);
  glBindVertexArray(m_VAO);
  float quad[] = {-1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
                  -1.0f, 1.0f,  0.0f, 1.0f, 1.0f,  0.0f};
  float quadUV[] = {0, 0, 1, 0, 0, 1, 1, 1};

  m_QuadBuffer = std::make_unique<GLBuffer>(m_BufferPool, sizeof(quad), quad);
  m_QuadUVBuffer =
      std::make_unique<GLBuffer>(m_BufferPool, sizeof(quadUV), quadUV);

  auto sculptureModel = std::make_unique<Model>("models/sculpture.obj");
  m_ModelBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sculptureModel->GetVertices().size() * sizeof(Vertex),
      sculptureModel->GetVertices().data());
  auto indicesLists = sculptureModel->GetIndiciesLists();
  m_ModelIndexBuffers.resize(indicesLists.size());
  m_ModelIndicesNums.resize(indicesLists.size());
  for (int i = 0; i < indicesLists.size(); i++) {
    m_ModelIndicesNums[i] = sculptureModel->GetIndiciesLists()[i][2].size();
    if (m_ModelIndicesNums[i] > 0)
      m_ModelIndexBuffers[i] = std::make_unique<GLBuffer>(
          m_BufferPool, m_ModelIndicesNums[i] * sizeof(uint32_t),
          indicesLists[i][2].data());
  }
  int postProcessingWidth = 1024;
  int postProcessingHeight = 1024;
  m_MainRT = std::make_unique<GLRenderTarget>(m_Width, m_Height, 24);

  m_FinalRT = std::make_unique<GLRenderTarget>(postProcessingHeight / 2, postProcessingHeight / 2, 0);
  postProcessingWidth = 256;
  postProcessingHeight = 256;
  m_Downsample4xRT = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
  m_ComposeRTs.resize(m_NumLevels);
  m_BlurRTs.resize(m_NumLevels);
  m_TempRTs.resize(m_NumLevels);
  for (int i = 0; i < m_NumLevels; i++) {
    m_ComposeRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    m_BlurRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    m_TempRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    postProcessingHeight /= 2;
    postProcessingWidth /= 2;
  }
}

void MyApp::DrawSkybox() {
  m_SkyboxShaders->UseProgram();

  glBindTexture(GL_TEXTURE_CUBE_MAP, m_SkyboxTexture->GetTexture());
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, m_SkyboxVertexBuffer->GetBuffer());
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0,
                        (void *)(m_SkyboxVertexBuffer->GetMemoryOffset()));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_SkyboxIndexBuffer->GetBuffer());
  glDrawElements(GL_TRIANGLES, 6 * 6, GL_UNSIGNED_SHORT,
                 (void *)(m_SkyboxIndexBuffer->GetMemoryOffset()));
  m_SkyboxShaders->StopProgram();
}

void MyApp::DrawModel() {
  m_ModelShaders->UseProgram();
  glBindBuffer(GL_ARRAY_BUFFER, m_ModelBuffer->GetBuffer());
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void *)(m_ModelBuffer->GetMemoryOffset()));
  glVertexAttribPointer(
      1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
      (void *)(m_ModelBuffer->GetMemoryOffset() + sizeof(glm::vec4)));
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void *)(m_ModelBuffer->GetMemoryOffset() +
                                 sizeof(glm::vec4) + sizeof(glm::vec3)));
  for (int i = -20; i < 20; i++) {
    for (int j = -20; j < 20; j++) {
      auto tempModelMatrix = 
          glm::translate(m_ModelMatrix, glm::vec3(5.f * i, 0.f, 5.f * j));
      glm::mat4 tempMVPMatrix =
          m_PerspectiveMatrix * m_ViewMatrix * tempModelMatrix;
      m_ModelVertUBuffer->UpdateData(0, sizeof(glm::mat4), &tempMVPMatrix);
      for (int k = 0; k < m_ModelIndicesNums.size(); k++) {
        if (m_ModelIndicesNums[k] > 0) {
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                       m_ModelIndexBuffers[k]->GetBuffer());
          glDrawElements(GL_TRIANGLES, m_ModelIndicesNums[k], GL_UNSIGNED_INT,
                         (void *)(m_ModelIndexBuffers[k]->GetMemoryOffset()));
        }
      }
    }
  }
  m_ModelShaders->StopProgram();
}

void MyApp::Downsample(std::unique_ptr<GLRenderTarget> &src,
                       std::unique_ptr<GLRenderTarget> &des) {
  des->BindFramebuffer();
  m_DownsampleShaders->UseProgram();
  glDisable(GL_CULL_FACE);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::Downsample4x(std::unique_ptr<GLRenderTarget> &src,
                         std::unique_ptr<GLRenderTarget> &des) {
  des->BindFramebuffer();

  m_Downsample4xShaders->UseProgram();
  glDisable(GL_CULL_FACE);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::ExtractHL(std::unique_ptr<GLRenderTarget> &src,
                      std::unique_ptr<GLRenderTarget> &des) {
  des->BindFramebuffer();

  m_ExtractHLShaders->UseProgram();
  glDisable(GL_CULL_FACE);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::Blur(std::unique_ptr<GLRenderTarget> &src,
                 std::unique_ptr<GLRenderTarget> &des,
                 std::unique_ptr<GLRenderTarget> &temp, float radius, float s) {
  temp->BindFramebuffer();
  m_BlurShaders->UseProgram();

  glDisable(GL_CULL_FACE);
  // horizontal blur
  m_BlurFragUBO = {glm::vec2(1.f, 0.f), static_cast<float>(src->GetWidth()),
                   radius, s};
  m_BlurFragUBuffer->UpdateData(0, sizeof(m_BlurFragUBO), &m_BlurFragUBO);
  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
  glUseProgram(0);

  // vertical blur
  m_BlurShaders->UseProgram();
  des->BindFramebuffer();
  m_BlurFragUBO = {glm::vec2(0.f, 1.f), static_cast<float>(src->GetHeight()),
                   radius, s};
  m_BlurFragUBuffer->UpdateData(0, sizeof(m_BlurFragUBO), &m_BlurFragUBO);
  glActiveTexture(GL_TEXTURE0);
  temp->BindColorTexture();
  DrawAxisAlignedQuad();
  temp->UnbindColorTexture();
}

void MyApp::ComposeEffect() {
  ////////compose gaussian blur buffers///////////////
  m_ComposeRTs[0]->BindFramebuffer();
  m_GaussianComposeShaders->UseProgram();
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler0"), 0);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler1"), 1);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler2"), 2);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler3"), 3);
  glActiveTexture(GL_TEXTURE0);
  m_BlurRTs[0]->BindColorTexture();
  glActiveTexture(GL_TEXTURE1);
  m_BlurRTs[1]->BindColorTexture();
  glActiveTexture(GL_TEXTURE2);
  m_BlurRTs[2]->BindColorTexture();
  glActiveTexture(GL_TEXTURE3);
  m_BlurRTs[3]->BindColorTexture();

  DrawAxisAlignedQuad();

  ////////////////final glare composition/////////////
  m_FinalRT->BindFramebuffer();
  m_GlareComposeShaders->UseProgram();

  glActiveTexture(GL_TEXTURE0);
  m_ComposeRTs[0]->BindColorTexture();

  DrawAxisAlignedQuad();
}

void MyApp::ToneMappingPass() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, m_Width, m_Height);

  m_ToneMappingShaders->UseProgram();
  glUniform1i(m_ToneMappingShaders->GetUniform("uSceneTex"), 0);
  glUniform1i(m_ToneMappingShaders->GetUniform("uBlurTex"), 1);
  glActiveTexture(GL_TEXTURE0);
  m_MainRT->BindColorTexture();
  glActiveTexture(GL_TEXTURE1);
  m_FinalRT->BindColorTexture();

  DrawAxisAlignedQuad();
}

void MyApp::DrawAxisAlignedQuad() {
  glDisable(GL_DEPTH_TEST);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, m_QuadBuffer->GetBuffer());
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0,
                        (void *)m_QuadBuffer->GetMemoryOffset());

  glBindBuffer(GL_ARRAY_BUFFER, m_QuadUVBuffer->GetBuffer());
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0,
                        (void *)m_QuadUVBuffer->GetMemoryOffset());

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
}
