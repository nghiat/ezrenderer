#pragma once
#include <Base/Base.h>
#include <GLAPI/GLShader.h>
#include <GLAPI/GLBuffer.h>
#include <GLAPI/GLTextureCube.h>
#include <GLAPI/GLRenderTarget.h>
#include <Util/Model.h>
#include <memory>

class MyApp : public Ez::Base {
public:
  MyApp(int width, int heigth, std::string title)
      : Ez::Base(Ez::Base::Renderer::GL, width, heigth, title){};

  void InitShaders();

  void InitObjects();
  void Init() override;
  void Update() override;
  void Draw() override;
  void InitTexture();

  void PointerPosCallback(double xPos, double yPos) override;

  void ScrollCallback(double xOffset, double yOffset) override;

  void ResizeCallback(int width, int height) override;

  virtual void TouchCallback(Action action) override;

private:
  void DrawSkybox();

  //Draw the model
  void DrawModel();

  void DrawAxisAlignedQuad();
  //Downsample 2x in each dimension
  void Downsample(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Downsample 4x in each dimension
  void Downsample4x(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Extract highlight area
  void ExtractHL(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des);

  //Apply vertical and horizontal gaussian blur
  void Blur(std::unique_ptr<Ez::GLRenderTarget>& src, std::unique_ptr<Ez::GLRenderTarget>& des, std::unique_ptr<Ez::GLRenderTarget>& temp, float radius, float s);

  //Compose all the blur framebuffers
  void ComposeEffect();

  //Map the original scene with the post-processed scene
  void ToneMappingPass();

  std::unique_ptr<Ez::GLTextureCube> m_SkyboxTexture;

  std::shared_ptr<Ez::GLBufferPool> m_BufferPool;

  std::unique_ptr<Ez::GLBuffer> m_SkyboxVertexBuffer;

  std::unique_ptr<Ez::GLBuffer> m_SkyboxIndexBuffer;

  std::unique_ptr<Ez::GLShader> m_ModelShaders;

  std::unique_ptr<Ez::GLShader> m_SkyboxShaders;

  std::unique_ptr<Ez::GLShader> m_DownsampleShaders;

  std::unique_ptr<Ez::GLShader> m_Downsample4xShaders;

  std::unique_ptr<Ez::GLShader> m_ExtractHLShaders;

  std::unique_ptr<Ez::GLShader> m_BlurShaders;

  std::unique_ptr<Ez::GLShader> m_GaussianComposeShaders;

  std::unique_ptr<Ez::GLShader> m_GlareComposeShaders;

  std::unique_ptr<Ez::GLShader> m_ToneMappingShaders;
  std::unique_ptr<Ez::GLBuffer> m_QuadBuffer;
  std::unique_ptr<Ez::GLBuffer> m_QuadUVBuffer;

  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_PosBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_NormalBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_IndexBuffers;
  std::vector<std::vector<std::unique_ptr<Ez::GLBuffer>>> m_ColorBuffers;

  std::unique_ptr<Ez::GLRenderTarget> m_MainRT;
  std::unique_ptr<Ez::GLRenderTarget> m_FinalRT;
  std::uint32_t m_NumLevels = 4;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_ComposeRTs;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_BlurRTs;
  std::vector<std::unique_ptr<Ez::GLRenderTarget>> m_TempRTs;

  std::unique_ptr<Ez::GLBuffer> m_ModelBuffer;
  std::vector<std::unique_ptr<Ez::GLBuffer>> m_ModelIndexBuffers;
  std::vector<size_t> m_ModelIndicesNums;

  GLuint m_ModelMVP;
  GLuint m_ModelViewMatrix;
  GLuint m_ModelEye;
  GLuint m_ModelEta;

  GLuint m_ModelPhong;
  GLuint m_ModelFactor;
  GLuint m_ModelLightPos;
  GLuint m_ModelMode;
  GLuint m_ModelLightIntensities; //light color
  GLuint m_ModelEvMapping;

  GLuint m_SkyboxViewMatrix;
  GLuint m_SkyboxProjectMatrix;

  GLuint m_Downsample4xTexelSize;

  GLuint m_ExtractHightLightThreshold;
  GLuint m_ExtractHightLightScalar;

  GLuint m_BlurDir;
  GLuint m_BlurResolution;
  GLuint m_BlurRadius;
  GLuint m_BlurS;

  GLuint m_GaussianComposeCoeff;

  GLuint m_GlareComposeMixCoeff;

  GLuint m_ToneMappingBlurAmount;
  GLuint m_ToneMappingExposure;
  GLuint m_ToneMappingGamma;

  bool m_EnableHDR = true;

  //3.0f / bloom width
  float m_S[4] = { 0.42f, 0.25f, 0.15f, 0.10f };

  //Vars for transformations
  glm::vec3 m_Eye, m_CenterLook, m_Up, m_RotationAxis1, m_RotationAxis2;
  glm::mat4 m_ModelMatrix, m_ViewMatrix, m_PerspectiveMatrix, m_MVPMatrix;

  GLuint m_VAO;
  bool m_IsMouseClicked = false;
  double m_MouseX = 0.0;
  double m_MouseY = 0.0;
  float m_Exposure = 10.f;
  float m_Eta = 1.f;
  glm::vec4 m_Phong = glm::vec4(.5f, .6f, .4f, 16.f);
  float m_Factor = 1.f;
  glm::vec3 m_LightPos = glm::vec3(10.f);
  int m_Mode = 1;
  glm::vec3 m_LightColor = glm::vec3(1.f);
  bool m_IsEVMappingEnabled = true;
};
