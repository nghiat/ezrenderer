#include <jni.h>
#include <Base/base.h>
#include <Util/JNIHelper.h>
#include <android/asset_manager_jni.h>

extern "C" {
JNIEXPORT void JNICALL Java_nghiat_ezrenderer_Renderer_drawNative(JNIEnv* env, jobject thiz);

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_Renderer_resizeNative(JNIEnv *env,
                                             jobject instance,
                                             jint w,
                                             jint h);
JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_Renderer_initNative(JNIEnv *env, jobject instance);
JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_RendererActivity_SetAssetManager(JNIEnv *env,
                                                        jobject instance,
                                                        jobject mgr);
JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_RendererSurfaceView_nativeOnTouch(JNIEnv *env,
                                                         jobject instance,
                                                         jint type,
                                                         jint x,
                                                         jint y,
                                                         jint id);

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_ScaleGestureListener_scale(JNIEnv *env, jobject instance, jfloat factor);
}

JNIEXPORT void JNICALL Java_nghiat_ezrenderer_Renderer_drawNative(JNIEnv* env, jobject thiz) {
  base->Update();
  base->Draw();
}

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_Renderer_initNative(JNIEnv *env, jobject instance) {
  base->Init();
}

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_Renderer_resizeNative(JNIEnv *env,
                                             jobject instance,
                                             jint w,
                                             jint h){
  base->ResizeCallback(w, h);
}

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_RendererActivity_SetAssetManager(JNIEnv *env,
                                                        jobject instance,
                                                        jobject mgr) {
  JNIHelper::Get()->SetAssetManager(AAssetManager_fromJava(env, mgr));
}


JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_RendererSurfaceView_nativeOnTouch(JNIEnv *env,
                                                         jobject instance,
                                                         jint type,
                                                         jint x,
                                                         jint y,
                                                         jint id) {
  if(type == 2)
    base->PointerPosCallback(static_cast<double>(x),static_cast<double>(y));
  else
    base->TouchCallback(static_cast<Ez::Base::Action>(type));
}

JNIEXPORT void JNICALL
Java_nghiat_ezrenderer_ScaleGestureListener_scale(JNIEnv *env, jobject instance, jfloat factor) {
  base->ScrollCallback(0.f, factor);
}
