#include "App.h"
#include "CubeData.h"
#include "GLAPI/GLBufferPool.h"
#include "Util/TextureCube.h"
#include <glm/gtc/matrix_transform.hpp>
#include <memory>

#define PI 3.14159265359f

using namespace Ez;

Ez::Base *base = new MyApp(800, 800, "EzRenderer GL");

void MyApp::Init() {
  Base::Init();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  m_BufferPool = std::make_shared<Ez::GLBufferPool>();
  InitTexture();
  InitShaders();
  InitObjects();
}
void MyApp::Update() {}

void MyApp::Draw() {
  if (m_IsEVMappingEnabled) {
    if (m_EnableHDR) {
      m_MainRT->BindFramebuffer();
    } else {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glViewport(0, 0, m_Width, m_Height);
    DrawSkybox();
    DrawModel();

    if (m_EnableHDR) {
      Downsample4x(m_MainRT, m_BlurRTs[0]);
      ExtractHL(m_BlurRTs[0], m_ComposeRTs[0]);
      Blur(m_ComposeRTs[0], m_BlurRTs[0], m_TempRTs[0],
           float(int(3.0f / m_S[0])), m_S[0]);

      for (int i = 1; i < m_NumLevels; i++) {
        Downsample(m_ComposeRTs[i - 1], m_ComposeRTs[i]);
        Blur(m_ComposeRTs[i], m_BlurRTs[i], m_TempRTs[i],
             float(int(3.0f / m_S[i])), m_S[i]);
      }

      ComposeEffect();

      ToneMappingPass();
    }
  } else {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawModel();
  }
}

void MyApp::InitTexture() {
  auto skybox = std::make_unique<Ez::TextureCube>("textures/altar_cross_mmp_s.hdr");
  m_SkyboxTexture = std::make_unique<Ez::GLTextureCube>(skybox.get());

  m_SkyboxVertexBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sizeof(SkyboxVertices), SkyboxVertices);
  m_SkyboxIndexBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sizeof(SkyboxIndices), SkyboxIndices);
}

void MyApp::PointerPosCallback(double xPos, double yPos) {
  if (m_IsMouseClicked) {
    if (m_MouseX == 0.0 && m_MouseY == 0.0) {
      m_MouseX = xPos;
      m_MouseY = yPos;
    }
    float deltaX = static_cast<float>((xPos - m_MouseX) / m_Width);
    float deltaY = static_cast<float>((yPos - m_MouseY) / m_Height);
    m_MouseX = xPos;
    m_MouseY = yPos;

    m_RotationAxis2 = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
        glm::vec4(m_RotationAxis2, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaX * 2 * PI, m_RotationAxis1);
    m_Eye = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
        glm::vec4(m_Eye, 1.0f));

    m_RotationAxis1 = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
        glm::vec4(m_RotationAxis1, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaY * 2 * PI, m_RotationAxis2);
    m_Eye = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
        glm::vec4(m_Eye, 1.0f));

    m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
  }
}

void MyApp::ScrollCallback(double xOffset, double yOffset) {
  float zoomFactor = 1;
  if (yOffset < 0.0) {
    zoomFactor = 1.0f / (1.0f - float(yOffset) * 0.05f);
  } else {
    zoomFactor += 0.05f * float(yOffset);
  }
  m_ModelMatrix =
      glm::scale(m_ModelMatrix, glm::vec3(zoomFactor, zoomFactor, zoomFactor));
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
}

void MyApp::ResizeCallback(int width, int height) {
  float aspect = width * 1.f / height;
  m_Width = width;
  m_Height = height;
  m_PerspectiveMatrix = glm::perspective(45.0f, aspect, 0.1f, 100.0f);
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
  glViewport(0, 0, m_Width, m_Height);
  m_MainRT->Resize(width, height);
}

void MyApp::InitShaders() {
  m_RotationAxis1 = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ModelMatrix = glm::mat4(1.0f);
  m_Eye = glm::vec3(7.f);
  m_CenterLook = glm::vec3(0.0f);
  m_Up = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ViewMatrix = glm::lookAt(m_Eye, m_CenterLook, m_Up);
  m_RotationAxis2 = cross(m_CenterLook - m_Eye, m_RotationAxis1);
  m_PerspectiveMatrix =
      glm::perspective(45.0f, float(m_Width * 1.0f / m_Height), 0.1f, 1000.0f);
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;

  m_ModelShaders =
      std::make_unique<GLShader>("shaders/model.vert", "shaders/model.frag");
  m_ModelMVP = m_ModelShaders->GetUniform("uMVP");
  m_ModelViewMatrix = m_ModelShaders->GetUniform("uViewMatrix");
  m_ModelEye = m_ModelShaders->GetUniform("uEye");
  m_ModelEta = m_ModelShaders->GetUniform("uEta");
  m_ModelPhong = m_ModelShaders->GetUniform("uPhong");
  m_ModelFactor = m_ModelShaders->GetUniform("uFactor");
  m_ModelLightPos = m_ModelShaders->GetUniform("uLightPos");
  m_ModelMode = m_ModelShaders->GetUniform("uMode");
  m_ModelLightIntensities =
      m_ModelShaders->GetUniform("uLightIntensities"); // light color
  m_ModelEvMapping = m_ModelShaders->GetUniform("uEvMapping");

  m_SkyboxShaders =
      std::make_unique<GLShader>("shaders/skybox.vert", "shaders/skybox.frag");
  m_SkyboxViewMatrix = m_SkyboxShaders->GetUniform("uViewMatrix");
  m_SkyboxProjectMatrix = m_SkyboxShaders->GetUniform("uProjectMatrix");

  m_DownsampleShaders = std::make_unique<GLShader>("shaders/downsample.vert",
                                                   "shaders/downsample.frag");

  m_Downsample4xShaders = std::make_unique<GLShader>(
      "shaders/downsample4x.vert", "shaders/downsample4x.frag");
  m_Downsample4xTexelSize = m_Downsample4xShaders->GetUniform("uTexelSize");

  m_ExtractHLShaders = std::make_unique<GLShader>("shaders/extractHL.vert",
                                                  "shaders/extractHL.frag");
  m_ExtractHightLightThreshold = m_ExtractHLShaders->GetUniform("uThreshold");
  m_ExtractHightLightScalar = m_ExtractHLShaders->GetUniform("uScalar");

  m_BlurShaders =
      std::make_unique<GLShader>("shaders/blur.vert", "shaders/blur.frag");
  m_BlurDir = m_BlurShaders->GetUniform("uDir");
  m_BlurResolution = m_BlurShaders->GetUniform("uResolution");
  m_BlurRadius = m_BlurShaders->GetUniform("uRadius");
  m_BlurS = m_BlurShaders->GetUniform("uS");

  m_GaussianComposeShaders = std::make_unique<GLShader>(
      "shaders/gaussianCompose.vert", "shaders/gaussianCompose.frag");
  m_GaussianComposeCoeff = m_GaussianComposeShaders->GetUniform("uCoeff");

  m_GlareComposeShaders = std::make_unique<GLShader>(
      "shaders/glareCompose.vert", "shaders/glareCompose.frag");
  m_GlareComposeMixCoeff = m_GlareComposeShaders->GetUniform("uMixCoeff");

  m_ToneMappingShaders = std::make_unique<GLShader>("shaders/toneMapping.vert",
                                                    "shaders/toneMapping.frag");
  m_ToneMappingBlurAmount = m_ToneMappingShaders->GetUniform("uBlurAmount");
  m_ToneMappingExposure = m_ToneMappingShaders->GetUniform("uExposure");
  m_ToneMappingGamma = m_ToneMappingShaders->GetUniform("uGamma");
}

void MyApp::InitObjects() {
  glGenVertexArrays(1, &m_VAO);
  glBindVertexArray(m_VAO);
  float quad[] = {-1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
                  -1.0f, 1.0f,  0.0f, 1.0f, 1.0f,  0.0f};
  float quadUV[] = {0, 0, 1, 0, 0, 1, 1, 1};

  m_QuadBuffer = std::make_unique<GLBuffer>(m_BufferPool, sizeof(quad), quad);
  m_QuadUVBuffer =
      std::make_unique<GLBuffer>(m_BufferPool, sizeof(quadUV), quadUV);

  auto sculptureModel = std::make_unique<Model>("models/sculpture.obj");
  m_ModelBuffer = std::make_unique<GLBuffer>(
      m_BufferPool, sculptureModel->GetVertices().size() * sizeof(Vertex),
      sculptureModel->GetVertices().data());
  auto indicesLists = sculptureModel->GetIndiciesLists();
  m_ModelIndexBuffers.resize(indicesLists.size());
  m_ModelIndicesNums.resize(indicesLists.size());
  for (int i = 0; i < indicesLists.size(); i++) {
    m_ModelIndicesNums[i] = sculptureModel->GetIndiciesLists()[i][2].size();
    if (m_ModelIndicesNums[i] > 0)
      m_ModelIndexBuffers[i] = std::make_unique<GLBuffer>(
          m_BufferPool, m_ModelIndicesNums[i] * sizeof(uint32_t),
          indicesLists[i][2].data());
  }
  int postProcessingWidth = 1024;
  int postProcessingHeight = 1024;
  m_MainRT = std::make_unique<GLRenderTarget>(m_Width, m_Height, 24);

  m_FinalRT = std::make_unique<GLRenderTarget>(postProcessingHeight / 2, postProcessingHeight / 2, 0);
  postProcessingWidth = 256;
  postProcessingHeight = 256;
  m_ComposeRTs.resize(m_NumLevels);
  m_BlurRTs.resize(m_NumLevels);
  m_TempRTs.resize(m_NumLevels);
  for (int i = 0; i < m_NumLevels; i++) {
    m_ComposeRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    m_BlurRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    m_TempRTs[i] = std::make_unique<GLRenderTarget>(postProcessingWidth, postProcessingHeight, 0);
    postProcessingHeight /= 2;
    postProcessingWidth /= 2;
  }
}

void MyApp::DrawSkybox() {
  m_SkyboxShaders->UseProgram();
  glUniformMatrix4fv(m_SkyboxViewMatrix, 1, GL_FALSE, &m_ViewMatrix[0][0]);
  glUniformMatrix4fv(m_SkyboxProjectMatrix, 1, GL_FALSE,
                     &m_PerspectiveMatrix[0][0]);
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_SkyboxTexture->GetTexture());
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, m_SkyboxVertexBuffer->GetBuffer());
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0,
                        (void *)(m_SkyboxVertexBuffer->GetMemoryOffset()));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_SkyboxIndexBuffer->GetBuffer());
  glDrawElements(GL_TRIANGLES, 6 * 6, GL_UNSIGNED_SHORT,
                 (void *)(m_SkyboxIndexBuffer->GetMemoryOffset()));
  m_SkyboxShaders->StopProgram();
}

void MyApp::DrawModel() {
  m_ModelShaders->UseProgram();

  glUniformMatrix4fv(m_ModelViewMatrix, 1, GL_FALSE, &m_ViewMatrix[0][0]);
  glUniform3fv(m_ModelEye, 1, &m_Eye[0]);
  glUniform1f(m_ModelEta, m_Eta);

  glUniform4fv(m_ModelPhong, 1, &m_Phong[0]);
  glUniform1f(m_ModelFactor, m_Factor);
  glUniform3fv(m_ModelLightPos, 1, &m_LightPos[0]);
  glUniform1i(m_ModelMode, m_Mode);
  glUniform3fv(m_ModelLightIntensities, 1, &m_LightColor[0]);
  glUniform1i(m_ModelEvMapping, m_IsEVMappingEnabled);

  glBindBuffer(GL_ARRAY_BUFFER, m_ModelBuffer->GetBuffer());
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void *)(m_ModelBuffer->GetMemoryOffset()));
  glVertexAttribPointer(
      1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
      (void *)(m_ModelBuffer->GetMemoryOffset() + sizeof(glm::vec4)));
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void *)(m_ModelBuffer->GetMemoryOffset() +
                                 sizeof(glm::vec4) + sizeof(glm::vec3)));
  for (int i = -4; i < 4; i++) {
    for (int j = -4; j < 4; j++) {
      glm::mat4 tempModelMatrix =
          glm::translate(m_ModelMatrix, glm::vec3(5.f * i, 0.f, 5.f * j));
      glm::mat4 tempMVPMatrix =
          m_PerspectiveMatrix * m_ViewMatrix * tempModelMatrix;
      glUniformMatrix4fv(m_ModelMVP, 1, GL_FALSE, &tempMVPMatrix[0][0]);
      for (int k = 0; k < m_ModelIndicesNums.size(); k++) {
        if (m_ModelIndicesNums[k] > 0) {
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                       m_ModelIndexBuffers[k]->GetBuffer());
          glDrawElements(GL_TRIANGLES, m_ModelIndicesNums[k], GL_UNSIGNED_INT,
                         (void *)(m_ModelIndexBuffers[k]->GetMemoryOffset()));
        }
      }
    }
  }
  m_ModelShaders->StopProgram();
}

void MyApp::Downsample(std::unique_ptr<GLRenderTarget> &src,
                       std::unique_ptr<GLRenderTarget> &des) {
  des->BindFramebuffer();

  m_DownsampleShaders->UseProgram();
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::Downsample4x(std::unique_ptr<GLRenderTarget> &src,
                         std::unique_ptr<GLRenderTarget> &des) {

  des->BindFramebuffer();
  m_Downsample4xShaders->UseProgram();
  glUniform2f(m_Downsample4xTexelSize, 2.f / m_Width, 2.f / m_Height);
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::ExtractHL(std::unique_ptr<GLRenderTarget> &src,
                      std::unique_ptr<GLRenderTarget> &des) {

  des->BindFramebuffer();

  m_ExtractHLShaders->UseProgram();
  glUniform1f(m_ExtractHightLightThreshold, 1.f);
  glUniform1f(m_ExtractHightLightScalar, .3f);
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
}

void MyApp::Blur(std::unique_ptr<GLRenderTarget> &src,
                 std::unique_ptr<GLRenderTarget> &des,
                 std::unique_ptr<GLRenderTarget> &temp, float radius, float s) {
  temp->BindFramebuffer();
  m_BlurShaders->UseProgram();
  glUniform2f(m_BlurDir, 1.f, 0.f);
  glUniform1f(m_BlurResolution, (float)(src->GetWidth()));
  glUniform1f(m_BlurRadius, radius);
  glUniform1f(m_BlurS, s);
  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);
  // horizontal blur
  glActiveTexture(GL_TEXTURE0);
  src->BindColorTexture();
  DrawAxisAlignedQuad();
  src->UnbindColorTexture();
  glUseProgram(0);

  // vertical blur
  des->BindFramebuffer();
  m_BlurShaders->UseProgram();
  glUniform2f(m_BlurDir, 0.f, 1.f);
  glUniform1f(m_BlurResolution, (float)(src->GetHeight()));
  glUniform1f(m_BlurRadius, radius);
  glUniform1f(m_BlurS, s);
  glActiveTexture(GL_TEXTURE0);
  temp->BindColorTexture();
  DrawAxisAlignedQuad();
  temp->UnbindColorTexture();
}

void MyApp::ComposeEffect() {
  ////////compose gaussian blur buffers///////////////
  m_ComposeRTs[0]->BindFramebuffer();

  m_GaussianComposeShaders->UseProgram();
  glUniform4f(m_GaussianComposeCoeff, .3f, .3f, .25f, .2f);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler0"), 0);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler1"), 1);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler2"), 2);
  glUniform1i(m_GaussianComposeShaders->GetUniform("uSampler3"), 3);
  glActiveTexture(GL_TEXTURE0);
  m_BlurRTs[0]->BindColorTexture();
  glActiveTexture(GL_TEXTURE1);
  m_BlurRTs[1]->BindColorTexture();
  glActiveTexture(GL_TEXTURE2);
  m_BlurRTs[2]->BindColorTexture();
  glActiveTexture(GL_TEXTURE3);
  m_BlurRTs[3]->BindColorTexture();

  DrawAxisAlignedQuad();

  ////////////////final glare composition/////////////
  m_FinalRT->BindFramebuffer();
  m_GlareComposeShaders->UseProgram();
  glUniform1f(m_GlareComposeMixCoeff, 1.2f);
  glActiveTexture(GL_TEXTURE0);
  m_ComposeRTs[0]->BindColorTexture();

  DrawAxisAlignedQuad();
}

void MyApp::ToneMappingPass() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, m_Width, m_Height);

  m_ToneMappingShaders->UseProgram();
  glUniform1f(m_ToneMappingBlurAmount, .33f);
  glUniform1f(m_ToneMappingExposure, (10.0f * logf(m_Exposure + .0001f)));
  glUniform1f(m_ToneMappingGamma, 1.f / 1.8f);
  glUniform1i(m_ToneMappingShaders->GetUniform("uSceneTex"), 0);
  glUniform1i(m_ToneMappingShaders->GetUniform("uBlurTex"), 1);
  glActiveTexture(GL_TEXTURE0);
  m_MainRT->BindColorTexture();
  glActiveTexture(GL_TEXTURE1);
  m_FinalRT->BindColorTexture();

  DrawAxisAlignedQuad();
}

void MyApp::DrawAxisAlignedQuad() {
  glDisable(GL_DEPTH_TEST);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, m_QuadBuffer->GetBuffer());
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0,
                        (void *)m_QuadBuffer->GetMemoryOffset());

  glBindBuffer(GL_ARRAY_BUFFER, m_QuadUVBuffer->GetBuffer());
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0,
                        (void *)m_QuadUVBuffer->GetMemoryOffset());

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
}

void MyApp::TouchCallback(Base::Action action) {
  if (action == DOWN) {
    m_IsMouseClicked = true;
  }
  if (action == UP) {
    m_IsMouseClicked = false;
    m_MouseX = m_MouseY = 0.0;
  }
}
