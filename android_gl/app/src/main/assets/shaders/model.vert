layout(location = 0) in vec4 aColor;
layout(location = 1) in vec3 aPos;
layout(location = 2) in vec3 aNormal;

out vec3 fragPos;
out vec3 fragNormal;
out vec4 fragColor;
out vec3 fragReflect;
out vec3 fragRefract;

uniform mat4 uMVP;
uniform mat4 uViewMatrix;
uniform vec3 uEye;
uniform float uEta;

void main(){
	fragPos = aPos;
	fragNormal = normalize(aNormal);
	fragColor = aColor;
	fragReflect = reflect(normalize(aPos - uEye), fragNormal);
	fragRefract = refract(normalize(aPos - uEye), fragNormal, uEta);

	gl_Position = uMVP * vec4(aPos, 1.0);
}
