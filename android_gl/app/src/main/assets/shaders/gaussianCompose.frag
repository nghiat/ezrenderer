in vec2 fragUV;
out vec4 outColor;

uniform vec4 uCoeff;

uniform sampler2D uSampler0;
uniform sampler2D uSampler1;
uniform sampler2D uSampler2;
uniform sampler2D uSampler3;

void main()
{
	outColor = texture(uSampler0, fragUV) * uCoeff.x
        + texture(uSampler1, fragUV) * uCoeff.y
        + texture(uSampler2, fragUV) * uCoeff.z
        + texture(uSampler3, fragUV) * uCoeff.w;
}
