in vec2 fragUV[4];

out vec4 outColor;

uniform sampler2D uSampler;

void main(){
	outColor = (texture(uSampler, fragUV[0])
            + texture(uSampler, fragUV[1])
            + texture(uSampler, fragUV[2])
            + texture(uSampler, fragUV[3])) * 0.25;
}
