in vec2 fragUV;
out vec4 outColor;

uniform sampler2D uSampler;
uniform float uMixCoeff;

void main()
{
	outColor = texture(uSampler, fragUV) * uMixCoeff;
}
