in vec2 uv;
uniform sampler2D uTex;

out vec4 color;

void main() {
    color = texture(uTex, uv);
}
