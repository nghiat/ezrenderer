in vec2 fragUV;
out vec4 outColor;

uniform float uThreshold;
uniform float uScalar;

uniform sampler2D sampler;

void main(){
	outColor = max((texture(sampler, fragUV) - uThreshold) * uScalar, vec4(0.0, 0.0, 0.0, 0.0));
}
