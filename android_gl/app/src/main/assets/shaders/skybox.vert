layout(location = 0) in vec3 aPos;

uniform mat4 uViewMatrix;
uniform mat4 uProjectMatrix;

out vec3 fragUV;

void main(){
	fragUV = aPos;
	gl_Position =  uProjectMatrix * uViewMatrix * vec4(aPos, 1.0);
}
