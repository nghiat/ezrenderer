in vec2 fragUV;
out vec4 outColor;

uniform sampler2D sampler;
uniform vec2 uDir;
uniform float uResolution;
uniform float uRadius;
uniform float uS;

float gaussian(float x, float sd){
	return exp(-(x*sd) * (x*sd));
}

void main(){
	vec4 sum = vec4(0.0);
	float blur = 1.0 / uResolution;
	float hstep = uDir.x;
	float vstep = uDir.y;
	float gauSum = 0.0;
	for(float i = 0.0; i < 2.0 * uRadius + 1.0; i = i + 1.0){
		gauSum += gaussian(i - uRadius, uS);
	}
    outColor = vec4(sum.rgb, 1.0);
    sum+= texture(sampler, fragUV) * gaussian(0.0, uS) / gauSum;
    for(float i = 1.0; i <= uRadius; i = i + 1.0){
    	sum += texture(sampler, vec2(fragUV.x - i*blur*hstep, fragUV.y - i*blur*vstep)) * gaussian(i, uS) / gauSum;
    	sum += texture(sampler, vec2(fragUV.x + i*blur*hstep, fragUV.y + i*blur*vstep)) * gaussian(i, uS) / gauSum;
    }
    outColor = vec4(sum.rgb, 1.0);
}
