package nghiat.ezrenderer;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;

public class RendererActivity extends Activity {
    static {
        System.loadLibrary("native");
    }
    RendererSurfaceView rendererSurfaceView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AssetManager mgr = getResources().getAssets();
        SetAssetManager(mgr);
        rendererSurfaceView = new RendererSurfaceView(getApplication());
        setContentView(rendererSurfaceView);
    }
    private native void SetAssetManager(AssetManager mgr);
}
