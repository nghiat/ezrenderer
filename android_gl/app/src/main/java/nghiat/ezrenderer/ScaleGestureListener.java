package nghiat.ezrenderer;

import android.view.GestureDetector;
import android.view.ScaleGestureDetector;

public class ScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        scale((detector.getCurrentSpan() - detector.getPreviousSpan()) / 300.f);
        return super.onScale(detector);
    }

    private native void scale(float factor);
}
