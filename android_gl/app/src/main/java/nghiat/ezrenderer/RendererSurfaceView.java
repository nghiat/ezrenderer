package nghiat.ezrenderer;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * Created by Tuan Nghia Tran on 1/1/2017.
 */

public class RendererSurfaceView extends GLSurfaceView {
    private ScaleGestureDetector mScaleDetector;
    private boolean mIsScalingDone = true;
    public RendererSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(3);
        setPreserveEGLContextOnPause(true);
        setRenderer(new nghiat.ezrenderer.Renderer());
        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int type = -1;
        int x = (int) e.getX(0);
        int y = (int) e.getY(0);
        int id = (int) e.getPointerId(0);
        mScaleDetector.onTouchEvent(e);
        if (!mScaleDetector.isInProgress() && e.getAction() == MotionEvent.ACTION_UP) {
            mIsScalingDone = true;
        } else if(mScaleDetector.isInProgress()) {
            mIsScalingDone = false;
        }
        if (mIsScalingDone) {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    type = 1;
                    break;
                case MotionEvent.ACTION_UP:
                    type = 0;
                    break;
                case MotionEvent.ACTION_MOVE:
                    type = 2;
                    break;
                default:
                    break;
            }

            if (type != -1)
                nativeOnTouch(type, x, y, id);
        }
        return true;
    }
    private native void nativeOnTouch( int type, int x, int y, int id);
}
