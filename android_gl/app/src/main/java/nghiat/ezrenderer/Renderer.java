package nghiat.ezrenderer;

import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Renderer implements GLSurfaceView.Renderer {
    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {

        initNative();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int i, int i1) {
        resizeNative(i, i1);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        drawNative();
    }
    private native void drawNative();
    private native void resizeNative(int w, int h);
    private native void initNative();
}
