#include "App.h"
#include "CubeData.h"
#include "Util/TextureCube.h"
#include <Util/Model.h>
#include <glm/gtc/matrix_transform.hpp>

#define PI 3.14159265359f

using namespace Ez;
using namespace std::placeholders;

App::~App() { vkDeviceWaitIdle(m_Device->GetDevice()); }

void App::Init() {
  Base::Init();
  m_Instance = std::make_shared<Ez::VulkanInstance>(false);
  m_Device = std::make_shared<Ez::VulkanDevice>(m_Instance);
  m_Swapchain =
      std::make_shared<Ez::VulkanSwapchain>(m_Instance, m_Device, 2, m_Window);
  m_CommandPool = std::make_shared<VulkanCommandPool>(m_Device);
  m_BufferPool = std::make_shared<VulkanBufferPool>(m_Device, m_CommandPool);
  m_UniformBufferPool = std::make_shared<VulkanBufferPool>(m_Device);
  InitTexture();
  InitShaders();
  InitObjects();
  InitPipelines();
  m_IsInitDone = true;
}

void App::Update() {}

void App::Draw() { m_MainRT->Draw(); }

void App::InitTexture() {
  auto skybox =
      std::make_unique<Ez::TextureCube>("textures/altar_cross_mmp_s.hdr");
  m_SkyboxTexture = std::make_unique<Ez::VulkanTextureCube>(
      m_Device, m_CommandPool, skybox.get());
  m_SkyboxVertexBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(SkyboxVertices), SkyboxVertices);
  m_SkyboxIndexBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(SkyboxIndices), SkyboxIndices);
}

void App::InitShaders() {
  m_RotationAxis1 = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ModelMatrix = glm::mat4(1.0f);
  m_Eye = glm::vec3(7.0f);
  m_CenterLook = glm::vec3(0.0f);
  m_Up = glm::vec3(0.0f, 1.0f, 0.0f);
  m_ViewMatrix = glm::lookAt(m_Eye, m_CenterLook, m_Up);
  m_RotationAxis2 = cross(m_CenterLook - m_Eye, m_RotationAxis1);
  m_PerspectiveMatrix =
      glm::perspective(45.0f, float(m_Width * 1.0f / m_Height), 0.1f, 1000.0f);
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;

  m_ModelShaders.resize(2);
  m_ModelShaders[0] = std::make_shared<Ez::VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/modelv.spv");
  m_ModelShaders[1] = std::make_shared<Ez::VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/modelf.spv");
  m_ModelVertUBO = {m_MVPMatrix, m_ViewMatrix, m_Eye, 1.f};
  m_ModelVertUBuffer = std::make_unique<VulkanBuffer>(
      m_UniformBufferPool, m_NumModelsCol * m_NumModelsRow * 256, nullptr);
  UpdateModelUniforms();
  m_ModelFragUBO = {glm::vec4(0.5f, 0.6f, 0.4f, 16.0f),
                    m_Eye,
                    1.0f,
                    glm::vec3(10.f),
                    1,
                    glm::vec3(1.f),
                    1};
  m_ModelFragUBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(m_ModelFragUBO), &m_ModelFragUBO);

  m_SkyboxShaders.resize(2);
  m_SkyboxShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/skyboxv.spv");
  m_SkyboxShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/skyboxf.spv");
  m_SkyboxVertUBO = {m_ViewMatrix, m_PerspectiveMatrix};
  m_SkyboxVertUBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(SkyboxVertUBO), &m_SkyboxVertUBO);

  m_DownsampleShaders.resize(2);
  m_DownsampleShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/downsamplev.spv");
  m_DownsampleShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/downsamplef.spv");

  m_Downsample4xShaders.resize(2);
  m_Downsample4xShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/downsample4xv.spv");
  m_Downsample4xShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/downsample4xf.spv");
  m_Downsample4xVertUBO.texelSize = glm::vec2(2.f / m_Width, 2.f / m_Height);
  m_Downsample4xVertUBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(Downsample4xVertUBO), &m_Downsample4xVertUBO);

  m_ExtractHLShaders.resize(2);
  m_ExtractHLShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/extractHLv.spv");
  m_ExtractHLShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/extractHLf.spv");
  m_ExtractHightLightFragUBO = {1.f, .3f};
  m_ExtractHightLightFragUBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sizeof(ExtractHightLightFragUBO),
      &m_ExtractHightLightFragUBO);

  m_BlurShaders.resize(2);
  m_BlurShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/blurv.spv");
  m_BlurShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/blurf.spv");

  m_GaussianComposeShaders.resize(2);
  m_GaussianComposeShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/gaussianComposev.spv");
  m_GaussianComposeShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/gaussianComposef.spv");
  m_GaussianFragUBO.coeff = glm::vec4(0.3f, 0.3f, 0.25f, 0.20f);
  m_GaussianFragUBuffer = std::make_unique<VulkanBuffer>(
      m_UniformBufferPool, sizeof(GaussianFragUBO), &m_GaussianFragUBO);

  m_GlareComposeShaders.resize(2);
  m_GlareComposeShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/glareComposev.spv");
  m_GlareComposeShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/glareComposef.spv");
  m_GlareFragUBO.mixCoeff = 1.2f;
  m_GlareFragUBuffer = std::make_unique<VulkanBuffer>(
      m_UniformBufferPool, sizeof(GlareFragUBO), &m_GlareFragUBO);

  m_ToneMappingShaders.resize(2);
  m_ToneMappingShaders[0] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_VERTEX_BIT, "shaders/toneMappingv.spv");
  m_ToneMappingShaders[1] = std::make_shared<VulkanShader>(
      m_Device, VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/toneMappingf.spv");
  // adaptive exposure adjustment in log space
  float newExp = (float)(10.0f * log(m_Exposure + 0.0001));
  m_ToneMappingFragUBO = {0.33f, newExp, 1.f / 1.8f};
  m_ToneMappingFragUBuffer = std::make_unique<VulkanBuffer>(
      m_UniformBufferPool, sizeof(ToneMappingFragUBO), &m_ToneMappingFragUBO);
}

void App::InitObjects() {
  float quad[] = {-1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
                  -1.0f, 1.0f,  0.0f, 1.0f, 1.0f,  0.0f};
  float quadUV[] = {0, 0, 1, 0, 0, 1, 1, 1};

  m_QuadBuffer =
      std::make_unique<VulkanBuffer>(m_BufferPool, sizeof(quad), quad);
  m_QuadUVBuffer =
      std::make_unique<VulkanBuffer>(m_BufferPool, sizeof(quadUV), quadUV);

  auto sculptureModel = std::make_unique<Model>("models/sculpture.obj");
  m_ModelBuffer = std::make_unique<VulkanBuffer>(
      m_BufferPool, sculptureModel->GetVertices().size() * sizeof(Vertex),
      sculptureModel->GetVertices().data());
  auto indicesLists = sculptureModel->GetIndiciesLists();
  m_ModelIndexBuffers.resize(indicesLists.size());
  m_ModelIndicesNums.resize(indicesLists.size());
  for (int i = 0; i < indicesLists.size(); i++) {
    m_ModelIndicesNums[i] = sculptureModel->GetIndiciesLists()[i][2].size();
    if (m_ModelIndicesNums[i] > 0)
      m_ModelIndexBuffers[i] = std::make_unique<VulkanBuffer>(
          m_BufferPool, m_ModelIndicesNums[i] * sizeof(uint32_t),
          indicesLists[i][2].data());
  }

  m_MainRT = std::make_unique<VulkanRenderTarget>(m_Device, m_Swapchain,
                                                  m_CommandPool);
  int postProcessingWidth = 1024;
  int postProcessingHeight = 1024;
  m_SceneRT = std::make_unique<VulkanRenderTarget>(m_Device, m_CommandPool,
                                                   m_Width, m_Height);

  m_FinalRT = std::make_unique<VulkanRenderTarget>(m_Device, m_CommandPool,
                                                   postProcessingWidth / 2,
                                                   postProcessingHeight / 2);
  postProcessingWidth = 256;
  postProcessingHeight = 256;
  m_ComposeRTs.resize(m_NumLevels);
  m_BlurRTs.resize(m_NumLevels);
  m_TempRTs.resize(m_NumLevels);
  m_Downsample4xRT = std::make_unique<VulkanRenderTarget>(
      m_Device, m_CommandPool, postProcessingWidth, postProcessingHeight);
  m_GaussianRT = std::make_unique<VulkanRenderTarget>(
      m_Device, m_CommandPool, postProcessingWidth, postProcessingHeight);
  for (int i = 0; i < m_NumLevels; i++) {
    m_ComposeRTs[i] = std::make_unique<VulkanRenderTarget>(
        m_Device, m_CommandPool, postProcessingWidth, postProcessingHeight);
    m_BlurRTs[i] = std::make_unique<VulkanRenderTarget>(
        m_Device, m_CommandPool, postProcessingWidth, postProcessingHeight);
    m_TempRTs[i] = std::make_unique<VulkanRenderTarget>(
        m_Device, m_CommandPool, postProcessingWidth, postProcessingHeight);
    postProcessingHeight /= 2;
    postProcessingWidth /= 2;
  }
}

void App::InitPipelines() {
  m_SkyboxPipeline = std::make_unique<VulkanGraphicsPipeline>(m_Device);
  std::vector<VkDescriptorSetLayoutBinding> bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT}};
  m_SkyboxDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_SkyboxDescriptorSet->SetBuffer(0, 0, m_SkyboxVertUBuffer);
  m_SkyboxDescriptorSet->SetTexture(1, 0, m_SkyboxTexture.get());
  std::vector<uint32_t> vertexBindings = {0};
  std::vector<uint32_t> strides = {3 * sizeof(float)};
  std::vector<uint32_t> offsets = {0};
  std::vector<VkFormat> formats = {VK_FORMAT_R32G32B32_SFLOAT};
  m_SkyboxPipeline->SetupInputState(strides, vertexBindings, offsets, formats);
  m_SkyboxPipeline->AddShaders(m_SkyboxShaders);
  m_SkyboxPipeline->AddDescriptorSet(m_SkyboxDescriptorSet);
  m_SkyboxPipeline->CreateGraphicsPipeline(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                                           m_SceneRT);

  m_ModelPipeline = std::make_unique<VulkanGraphicsPipeline>(m_Device);
  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1,
       VK_SHADER_STAGE_VERTEX_BIT},
      {1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
      {2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT},
  };
  m_ModelDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_ModelDescriptorSet->SetBuffer(0, 0, m_ModelVertUBuffer, true);
  m_ModelDescriptorSet->SetBuffer(1, 0, m_ModelFragUBuffer);
  m_ModelDescriptorSet->SetTexture(2, 0, m_SkyboxTexture.get());
  m_ModelPipeline->AddDescriptorSet(m_ModelDescriptorSet);
  strides = {sizeof(Vertex)};
  vertexBindings = {0, 0, 0};
  offsets = {0, sizeof(glm::vec4), sizeof(glm::vec4) + sizeof(glm::vec3)};
  formats = {VK_FORMAT_R32G32B32A32_SFLOAT, VK_FORMAT_R32G32B32_SFLOAT,
             VK_FORMAT_R32G32B32_SFLOAT};
  m_ModelPipeline->SetupInputState(strides, vertexBindings, offsets, formats);
  m_ModelPipeline->AddShaders(m_ModelShaders);
  m_ModelPipeline->CreateGraphicsPipeline(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                                          m_SceneRT);

  m_Downsample4xPipeline = std::make_unique<VulkanGraphicsPipeline>(m_Device);
  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT},
  };
  m_Downsample4xDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_Downsample4xDescriptorSet->SetBuffer(0, 0, m_Downsample4xVertUBuffer);
  m_Downsample4xDescriptorSet->SetRenderTarget(1, 0, m_SceneRT);
  m_Downsample4xPipeline->AddDescriptorSet(m_Downsample4xDescriptorSet);
  strides = {3 * sizeof(float), 2 * sizeof(float)};
  vertexBindings = {0, 1};
  offsets = {0, 0};
  formats = {VK_FORMAT_R32G32B32_SFLOAT, VK_FORMAT_R32G32_SFLOAT};
  m_Downsample4xPipeline->SetupInputState(strides, vertexBindings, offsets,
                                          formats);
  m_Downsample4xPipeline->AddShaders(m_Downsample4xShaders);
  m_Downsample4xPipeline->CreateGraphicsPipeline(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_Downsample4xRT);

  m_ExtractHightLightPipeline =
      std::make_unique<VulkanGraphicsPipeline>(m_Device);

  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT}};
  m_ExtractHightLightDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_ExtractHightLightDescriptorSet->SetBuffer(0, 0,
                                              m_ExtractHightLightFragUBuffer);
  m_ExtractHightLightDescriptorSet->SetRenderTarget(1, 0, m_Downsample4xRT);
  m_ExtractHightLightPipeline->AddDescriptorSet(
      m_ExtractHightLightDescriptorSet);
  m_ExtractHightLightPipeline->SetupInputState(strides, vertexBindings, offsets,
                                               formats);
  m_ExtractHightLightPipeline->AddShaders(m_ExtractHLShaders);
  m_ExtractHightLightPipeline->CreateGraphicsPipeline(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_ComposeRTs[0]);

  m_TempBlurPipelines.resize(m_NumLevels);
  m_TempBlurDescriptorSets.resize(m_NumLevels);
  m_BlurPipelines.resize(m_NumLevels);
  m_BlurDescriptorSets.resize(m_NumLevels);
  for (int i = 0; i < m_NumLevels; i++) {
    m_TempBlurPipelines[i] = std::make_unique<VulkanGraphicsPipeline>(m_Device);
    m_TempBlurDescriptorSets[i] =
        std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
    m_TempBlurDescriptorSets[i]->SetRenderTarget(1, 0, m_ComposeRTs[i]);
    m_TempBlurPipelines[i]->AddDescriptorSet(m_TempBlurDescriptorSets[i]);
    m_TempBlurPipelines[i]->SetupInputState(strides, vertexBindings, offsets,
                                            formats);
    m_TempBlurPipelines[i]->AddShaders(m_BlurShaders);
    m_TempBlurPipelines[i]->AddPushConstants(VK_SHADER_STAGE_FRAGMENT_BIT, 0,
                                             sizeof(m_BlurFragUBO));
    m_TempBlurPipelines[i]->CreateGraphicsPipeline(
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_TempRTs[i]);
    m_TempRTs[i]->AddNextRendertarget(m_BlurRTs[i].get());
    m_TempRTs[i]->SetDrawCallback(
        [this, &i](VkCommandBuffer cb) { TempBlur(cb, i); });

    m_BlurPipelines[i] = std::make_unique<VulkanGraphicsPipeline>(m_Device);
    m_BlurDescriptorSets[i] =
        std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
    m_BlurDescriptorSets[i]->SetRenderTarget(1, 0, m_TempRTs[i]);
    m_BlurPipelines[i]->AddDescriptorSet(m_BlurDescriptorSets[i]);
    m_BlurPipelines[i]->SetupInputState(strides, vertexBindings, offsets,
                                        formats);
    m_BlurPipelines[i]->AddShaders(m_BlurShaders);
    m_BlurPipelines[i]->AddPushConstants(VK_SHADER_STAGE_FRAGMENT_BIT, 0,
                                         sizeof(m_BlurFragUBO));
    m_BlurPipelines[i]->CreateGraphicsPipeline(
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_BlurRTs[i]);
    m_BlurRTs[i]->SetDrawCallback(
        [this, &i](VkCommandBuffer cb) { Blur(cb, i); });
  }
  m_BlurRTs[m_NumLevels - 1]->AddNextRendertarget(m_GaussianRT.get());
  m_DownsamplePipelines.resize(m_NumLevels - 1);
  m_DownsampleDescriptorSets.resize(m_NumLevels - 1);
  bindings = {{0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
               VK_SHADER_STAGE_FRAGMENT_BIT}};
  for (int i = 0; i < m_NumLevels - 1; i++) {
    m_DownsamplePipelines[i] =
        std::make_unique<VulkanGraphicsPipeline>(m_Device);
    m_DownsampleDescriptorSets[i] =
        std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
    m_DownsampleDescriptorSets[i]->SetRenderTarget(0, 0, m_ComposeRTs[i]);
    m_DownsamplePipelines[i]->AddDescriptorSet(m_DownsampleDescriptorSets[i]);
    m_DownsamplePipelines[i]->SetupInputState(strides, vertexBindings, offsets,
                                              formats);
    m_DownsamplePipelines[i]->AddShaders(m_DownsampleShaders);
    m_DownsamplePipelines[i]->CreateGraphicsPipeline(
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_ComposeRTs[i + 1]);
    m_ComposeRTs[i + 1]->SetDrawCallback(
        [this, &i](VkCommandBuffer cb) { this->Downsample2x(cb, i); });
    m_ComposeRTs[i]->AddNextRendertarget(m_TempRTs[i].get());
    m_ComposeRTs[i]->AddNextRendertarget(m_ComposeRTs[i + 1].get());
  }
  m_ComposeRTs[m_NumLevels - 1]->AddNextRendertarget(
      m_TempRTs[m_NumLevels - 1].get());

  m_GaussianComposePipeline =
      std::make_unique<VulkanGraphicsPipeline>(m_Device);
  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4,
       VK_SHADER_STAGE_FRAGMENT_BIT}};
  m_GaussianComposeDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_GaussianComposeDescriptorSet->SetBuffer(0, 0, m_GaussianFragUBuffer);
  for (int i = 0; i < 4; i++)
    m_GaussianComposeDescriptorSet->SetRenderTarget(1, i, m_BlurRTs[i]);
  m_GaussianComposePipeline->AddDescriptorSet(m_GaussianComposeDescriptorSet);
  m_GaussianComposePipeline->SetupInputState(strides, vertexBindings, offsets,
                                             formats);
  m_GaussianComposePipeline->AddShaders(m_GaussianComposeShaders);
  m_GaussianComposePipeline->CreateGraphicsPipeline(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_GaussianRT);

  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT}};
  m_GlareComposePipeline = std::make_unique<VulkanGraphicsPipeline>(m_Device);
  m_GlareComposeDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_GlareComposeDescriptorSet->SetBuffer(0, 0, m_GlareFragUBuffer);
  m_GlareComposeDescriptorSet->SetRenderTarget(1, 0, m_GaussianRT);
  m_GlareComposePipeline->AddDescriptorSet(m_GlareComposeDescriptorSet);
  m_GlareComposePipeline->AddShaders(m_GlareComposeShaders);
  m_GlareComposePipeline->SetupInputState(strides, vertexBindings, offsets,
                                          formats);
  m_GlareComposePipeline->CreateGraphicsPipeline(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_FinalRT);

  m_ToneMappingPipeline = std::make_unique<VulkanGraphicsPipeline>(m_Device);
  bindings = {
      {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
      {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT},
      {2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
       VK_SHADER_STAGE_FRAGMENT_BIT}};
  m_ToneMappingDescriptorSet =
      std::make_shared<VulkanDescriptorSet>(m_Device, bindings);
  m_ToneMappingDescriptorSet->SetBuffer(0, 0, m_ToneMappingFragUBuffer);
  m_ToneMappingDescriptorSet->SetRenderTarget(1, 0, m_SceneRT);
  m_ToneMappingDescriptorSet->SetRenderTarget(2, 0, m_FinalRT);
  m_ToneMappingPipeline->AddDescriptorSet(m_ToneMappingDescriptorSet);
  m_ToneMappingPipeline->AddShaders(m_ToneMappingShaders);
  m_ToneMappingPipeline->SetupInputState(strides, vertexBindings, offsets,
                                         formats);
  m_ToneMappingPipeline->CreateGraphicsPipeline(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, m_MainRT);

  m_MainRT->SetDrawCallback(
      [this](VkCommandBuffer cb) { this->ToneMapping(cb); });
  m_MainRT->AddNextRendertarget(m_SceneRT.get());
  m_SceneRT->SetDrawCallback([this](VkCommandBuffer cb) {
    this->DrawSkybox(cb);
    this->DrawModel(cb);
  });
  m_SceneRT->AddNextRendertarget(m_Downsample4xRT.get());
  m_Downsample4xRT->SetDrawCallback(
      [this](VkCommandBuffer cb) { this->DownSample4x(cb); });
  m_Downsample4xRT->AddNextRendertarget(m_ComposeRTs[0].get());
  m_ComposeRTs[0]->SetDrawCallback(
      [this](VkCommandBuffer cb) { this->ExtractHightlight(cb); });
  m_GaussianRT->SetDrawCallback(
      [this](VkCommandBuffer cb) { this->GaussianCompose(cb); });
  m_GaussianRT->AddNextRendertarget(m_FinalRT.get());
  m_FinalRT->SetDrawCallback(
      [this](VkCommandBuffer cb) { this->GlareCompose(cb); });
}

void App::DrawSkybox(VkCommandBuffer commandBuffer) {
  VkViewport viewport = {.0f, .0f, m_Width * 1.f, m_Height * 1.f, .0f, 1.f};
  vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
  m_SkyboxPipeline->BindPipeline(commandBuffer);
  std::vector<VkDescriptorSet> sets = {
      m_SkyboxDescriptorSet->GetDescriptorSet()};
  vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_SkyboxPipeline->GetGraphicsPipelineLayout(), 0, 1,
                          sets.data(), 0, nullptr);
  VkBuffer pBuffers[] = {m_SkyboxVertexBuffer->GetBuffer()};
  VkDeviceSize pSizes[] = {m_SkyboxVertexBuffer->GetMemoryOffset()};
  vkCmdBindVertexBuffers(commandBuffer, 0, 1, pBuffers, pSizes);
  vkCmdBindIndexBuffer(commandBuffer, m_SkyboxIndexBuffer->GetBuffer(),
                       m_SkyboxIndexBuffer->GetMemoryOffset(),
                       VK_INDEX_TYPE_UINT16);
  vkCmdDrawIndexed(commandBuffer, 6 * 6, 1, 0, 0, 0);
}

void App::DrawModel(VkCommandBuffer commandBuffer) {
  VkViewport viewport = {.0f, .0f, m_Width * 1.f, m_Height * 1.f, .0f, 1.f};
  vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
  m_ModelPipeline->BindPipeline(commandBuffer);
  std::vector<VkDescriptorSet> sets = {
      m_ModelDescriptorSet->GetDescriptorSet()};
  VkBuffer pBuffers[] = {m_ModelBuffer->GetBuffer()};
  VkDeviceSize pSizes[] = {m_ModelBuffer->GetMemoryOffset()};
  vkCmdBindVertexBuffers(commandBuffer, 0, 1, pBuffers, pSizes);

  for (int k = 0; k < m_ModelIndicesNums.size(); k++) {
    if (m_ModelIndicesNums[k] > 0) {
      vkCmdBindIndexBuffer(commandBuffer, m_ModelIndexBuffers[k]->GetBuffer(),
                           m_ModelIndexBuffers[k]->GetMemoryOffset(),
                           VK_INDEX_TYPE_UINT32);
      for (int i = -m_NumModelsRow / 2; i < m_NumModelsRow / 2; i++) {
        for (int j = -m_NumModelsCol / 2; j < m_NumModelsCol / 2; j++) {
          uint32_t dynamicOffset = ((i + m_NumModelsRow / 2) * m_NumModelsCol +
                                    j + m_NumModelsCol / 2) *
                                   256;
          vkCmdBindDescriptorSets(commandBuffer,
                                  VK_PIPELINE_BIND_POINT_GRAPHICS,
                                  m_ModelPipeline->GetGraphicsPipelineLayout(),
                                  0, 1, sets.data(), 1, &dynamicOffset);
          vkCmdDrawIndexed(commandBuffer, m_ModelIndicesNums[k], 1, 0, 0, 0);
        }
      }
    }
  }
}

void App::DownSample4x(VkCommandBuffer commandBuffer) {
  VkViewport viewport = {.0f,
                         .0f,
                         m_Downsample4xRT->GetWidth() * 1.f,
                         m_Downsample4xRT->GetHeight() * 1.f,
                         .0f,
                         1.f};
  vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
  m_Downsample4xPipeline->BindPipeline(commandBuffer);
  auto set = m_Downsample4xDescriptorSet->GetDescriptorSet();

  vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_Downsample4xPipeline->GetGraphicsPipelineLayout(),
                          0, 1, &set, 0, nullptr);
  DrawQuad(commandBuffer);
}

void App::ExtractHightlight(VkCommandBuffer commandBuffer) {
  VkViewport viewport = {.0f,
                         .0f,
                         m_ComposeRTs[0]->GetWidth() * 1.f,
                         m_ComposeRTs[0]->GetHeight() * 1.f,
                         .0f,
                         1.f};
  vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
  m_ExtractHightLightPipeline->BindPipeline(commandBuffer);
  auto set = m_ExtractHightLightDescriptorSet->GetDescriptorSet();
  vkCmdBindDescriptorSets(
      commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
      m_ExtractHightLightPipeline->GetGraphicsPipelineLayout(), 0, 1, &set, 0,
      nullptr);
  DrawQuad(commandBuffer);
}

void App::Downsample2x(VkCommandBuffer cb, int level) {
  VkViewport viewport = {.0f,
                         .0f,
                         m_ComposeRTs[level + 1]->GetWidth() * 1.f,
                         m_ComposeRTs[level + 1]->GetHeight() * 1.f,
                         .0f,
                         1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_DownsamplePipelines[level]->BindPipeline(cb);
  auto set = m_DownsampleDescriptorSets[level]->GetDescriptorSet();
  vkCmdBindDescriptorSets(
      cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
      m_DownsamplePipelines[level]->GetGraphicsPipelineLayout(), 0, 1, &set, 0,
      nullptr);
  DrawQuad(cb);
}

void App::TempBlur(VkCommandBuffer cb, int level) {
  VkViewport viewport = {.0f,
                         .0f,
                         (float)m_TempRTs[level]->GetWidth(),
                         (float)m_TempRTs[level]->GetHeight(),
                         .0f,
                         1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_TempBlurPipelines[level]->BindPipeline(cb);
  m_BlurFragUBO = {glm::vec2(1.f, 0.f),
                   static_cast<float>(m_TempRTs[level]->GetWidth()),
                   float(int(3.f / m_S[level])), m_S[level]};
  vkCmdPushConstants(
      cb, m_TempBlurPipelines[level]->GetGraphicsPipelineLayout(),
      VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(m_BlurFragUBO), &m_BlurFragUBO);
  auto set = m_TempBlurDescriptorSets[level]->GetDescriptorSet();
  vkCmdBindDescriptorSets(
      cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
      m_TempBlurPipelines[level]->GetGraphicsPipelineLayout(), 0, 1, &set, 0,
      nullptr);
  DrawQuad(cb);
}

void App::Blur(VkCommandBuffer cb, int level) {
  VkViewport viewport = {.0f,
                         .0f,
                         (float)m_BlurRTs[level]->GetWidth(),
                         (float)m_BlurRTs[level]->GetHeight(),
                         .0f,
                         1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_BlurPipelines[level]->BindPipeline(cb);
  m_BlurFragUBO = {glm::vec2(.0f, 1.f),
                   static_cast<float>(m_BlurRTs[level]->GetHeight()),
                   float(int(3.f / m_S[level])), m_S[level]};
  vkCmdPushConstants(cb, m_BlurPipelines[level]->GetGraphicsPipelineLayout(),
                     VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(m_BlurFragUBO),
                     &m_BlurFragUBO);
  auto set = m_BlurDescriptorSets[level]->GetDescriptorSet();
  vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_BlurPipelines[level]->GetGraphicsPipelineLayout(),
                          0, 1, &set, 0, nullptr);
  DrawQuad(cb);
}

void App::GaussianCompose(VkCommandBuffer cb) {
  VkViewport viewport = {.0f,
                         .0f,
                         (float)m_GaussianRT->GetWidth(),
                         (float)m_GaussianRT->GetHeight(),
                         .0f,
                         1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_GaussianComposePipeline->BindPipeline(cb);
  auto set = m_GaussianComposeDescriptorSet->GetDescriptorSet();
  vkCmdBindDescriptorSets(
      cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
      m_GaussianComposePipeline->GetGraphicsPipelineLayout(), 0, 1, &set, 0,
      nullptr);
  DrawQuad(cb);
}

void App::GlareCompose(VkCommandBuffer cb) {
  VkViewport viewport = {
      .0f, .0f, (float)m_FinalRT->GetWidth(), (float)m_FinalRT->GetHeight(),
      .0f, 1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_GlareComposePipeline->BindPipeline(cb);
  auto set = m_GlareComposeDescriptorSet->GetDescriptorSet();
  vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_GlareComposePipeline->GetGraphicsPipelineLayout(),
                          0, 1, &set, 0, nullptr);
  DrawQuad(cb);
}

void App::ToneMapping(VkCommandBuffer cb) {
  VkViewport viewport = {
      .0f, .0f, (float)m_MainRT->GetWidth(), (float)m_MainRT->GetHeight(),
      .0f, 1.f};
  vkCmdSetViewport(cb, 0, 1, &viewport);
  m_ToneMappingPipeline->BindPipeline(cb);
  auto set = m_ToneMappingDescriptorSet->GetDescriptorSet();
  vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_ToneMappingPipeline->GetGraphicsPipelineLayout(), 0,
                          1, &set, 0, nullptr);
  DrawQuad(cb);
}

void App::UpdateModelUniforms() {
  for (int i = -m_NumModelsRow / 2; i < m_NumModelsRow / 2; i++) {
    for (int j = -m_NumModelsCol / 2; j < m_NumModelsCol / 2; j++) {
      auto tempModelMatrix =
          glm::translate(m_ModelMatrix, glm::vec3(5.f * i, 0.f, 5.f * j));
      m_ModelVertUBO.mvp = m_PerspectiveMatrix * m_ViewMatrix * tempModelMatrix;
      uint64_t offset =
          ((i + m_NumModelsRow / 2) * m_NumModelsCol + j + m_NumModelsCol / 2) *
          256;
      m_ModelVertUBuffer->UpdateData(offset, sizeof(m_ModelVertUBO),
                                     &m_ModelVertUBO);
    }
  }
}

void App::DrawQuad(VkCommandBuffer &commandBuffer) const {
  VkBuffer buffers[2] = {m_QuadBuffer->GetBuffer(),
                         m_QuadUVBuffer->GetBuffer()};
  VkDeviceSize offsets[2] = {m_QuadBuffer->GetMemoryOffset(),
                             m_QuadUVBuffer->GetMemoryOffset()};
  vkCmdBindVertexBuffers(commandBuffer, 0, 2, buffers, offsets);
  vkCmdDraw(commandBuffer, 4, 1, 0, 0);
}

void App::MouseButtonCallback(Mouse button, Action action) {
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    m_IsMouseClicked = true;
  }
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
    m_IsMouseClicked = false;
    m_MouseX = m_MouseY = 0.0;
  }
}

void App::PointerPosCallback(double xPos, double yPos) {
  if (m_IsMouseClicked) {
    if (m_MouseX == 0.0 && m_MouseY == 0.0) {
      m_MouseX = xPos;
      m_MouseY = yPos;
    }
    float deltaX = static_cast<float>((xPos - m_MouseX) / m_Width);
    float deltaY = static_cast<float>((yPos - m_MouseY) / m_Height);
    m_MouseX = xPos;
    m_MouseY = yPos;

    m_RotationAxis2 = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
        glm::vec4(m_RotationAxis2, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaX * 2 * PI, m_RotationAxis1);
    m_Eye = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaX * 2 * PI, m_RotationAxis1) *
        glm::vec4(m_Eye, 1.0f));

    m_RotationAxis1 = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
        glm::vec4(m_RotationAxis1, 1.0f));
    m_ViewMatrix = glm::rotate(m_ViewMatrix, deltaY * 2 * PI, m_RotationAxis2);
    m_Eye = glm::vec3(
        glm::rotate(glm::mat4(1.0f), -deltaY * 2 * PI, m_RotationAxis2) *
        glm::vec4(m_Eye, 1.0f));

    m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;

    m_ModelVertUBO.viewMatrix = m_ViewMatrix;
    m_ModelVertUBO.eye = m_Eye;
    UpdateModelUniforms();
    m_ModelFragUBO.eye = m_Eye;
    m_ModelFragUBuffer->UpdateData(sizeof(glm::vec4), sizeof(glm::vec3),
                                   &m_ModelFragUBO.eye);

    m_SkyboxVertUBO.viewMatrix = m_ViewMatrix;
    m_SkyboxVertUBuffer->UpdateData(0, sizeof(glm::mat4),
                                    &m_SkyboxVertUBO.viewMatrix);
  }
}

void App::ScrollCallback(double xOffset, double yOffset) {
  float zoomFactor = 1;
  if (yOffset < 0.0) {
    zoomFactor = 1.0f / (1.0f - float(yOffset) * 0.05f);
  } else {
    zoomFactor += 0.05f * float(yOffset);
  }
  m_ModelMatrix =
      glm::scale(m_ModelMatrix, glm::vec3(zoomFactor, zoomFactor, zoomFactor));
  m_MVPMatrix = m_PerspectiveMatrix * m_ViewMatrix * m_ModelMatrix;
  m_ModelVertUBO.mvp = m_MVPMatrix;
  UpdateModelUniforms();
}

void App::ResizeCallback(int width, int height) {}

Base *base = new App(800, 800, "desktop_vulkan");
