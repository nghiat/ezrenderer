#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 aPos;

layout (std140, binding = 0) uniform USkyboxVert {
    mat4 uViewMatrix;
    mat4 uProjectMatrix;
} ubo;

layout (location = 0) out vec3 fragUV;

out gl_PerVertex {
    vec4 gl_Position;
};

void main(){
	fragUV = aPos;
	gl_Position =  ubo.uProjectMatrix * ubo.uViewMatrix * vec4(aPos, 1.0);
    gl_Position.y *= -1.0;
}
