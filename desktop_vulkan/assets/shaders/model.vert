#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(location = 0) in vec4 aColor;
layout(location = 1) in vec3 aPos;
layout(location = 2) in vec3 aNormal;

layout (location = 0) out OVert {
    vec3 fragPos;
    vec3 fragNormal;
    vec4 fragColor;
    vec3 fragReflect;
    vec3 fragRefract;
};

layout (std140, binding = 0) uniform UModelVert {
    mat4 uMVP;
    mat4 uViewMatrix;
    vec3 uEye;
    float uEta;
} ubo;

void main(){
	fragPos = aPos;
	fragNormal = normalize(aNormal);
	fragColor = aColor;
	fragReflect = reflect(normalize(aPos - ubo.uEye), fragNormal);
	fragRefract = refract(normalize(aPos - ubo.uEye), fragNormal, ubo.uEta);

	gl_Position = ubo.uMVP * vec4(aPos, 1.0);
    gl_Position.y = -gl_Position.y;
}
