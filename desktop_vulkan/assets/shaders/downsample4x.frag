#version 450
#extension GL_ARB_separate_shader_objects : enable
layout (location = 0) in vec2 fragUV[4];

layout (location = 0) out vec4 outColor;

layout (binding = 1) uniform sampler2D uSampler;

void main(){
	outColor = (texture(uSampler, fragUV[0]) 
            + texture(uSampler, fragUV[1]) 
            + texture(uSampler, fragUV[2]) 
            + texture(uSampler, fragUV[3])) * 0.25;
}
