#version 450
#extension GL_ARB_separate_shader_objects : enable
layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (std140, binding = 0) uniform UGaussianFrag {
    vec4 uCoeff;
} ubo;

layout (binding = 1) uniform sampler2D uSamplers[4];

void main()
{
	outColor = texture(uSamplers[0], fragUV) * ubo.uCoeff.x
        + texture(uSamplers[1], fragUV) * ubo.uCoeff.y
        + texture(uSamplers[2], fragUV) * ubo.uCoeff.z 
        + texture(uSamplers[3], fragUV) * ubo.uCoeff.w;
}
