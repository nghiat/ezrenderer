#version 450
#extension GL_ARB_separate_shader_objects : enable
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;

layout (location = 0) out vec2 fragUV;

void main()
{
	gl_Position = vec4(aPos, 1.0f);
	fragUV = aUV;
}
