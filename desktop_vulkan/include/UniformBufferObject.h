#pragma once

#include <glm/glm.hpp>

/**
 * This header contains the structs for uniform buffer objects
 */

struct ModelVertUBO {
  glm::mat4 mvp;
  glm::mat4 viewMatrix;
  glm::vec3 eye;
  float eta;
};

struct ModelFragUBO {
  glm::vec4 phong;
  glm::vec3 eye;
  float factor;
  glm::vec3 lightPos;
  int mode;
  glm::vec3 lightIntensities;
  int evMapping;
};

struct SkyboxVertUBO {
  glm::mat4 viewMatrix;
  glm::mat4 projectMatrix;
};

struct Downsample4xVertUBO {
  glm::vec2 texelSize;
};

struct ExtractHightLightFragUBO {
  float threshold;
  float scalar;
};

struct BlurFragUBO {
  glm::vec2 dir;
  float resolution;
  float radius;
  float s;
};

struct GaussianFragUBO {
  glm::vec4 coeff;
};

struct GlareFragUBO {
  float mixCoeff;
};

struct ToneMappingFragUBO {
  float blurAmount;
  float exposure;
  float gamma;
};