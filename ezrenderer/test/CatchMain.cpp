//
// Created by nghia on 10/26/16.
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "ShaderToHeader.h"

TEST_CASE("Convert non alphabet or digit to underscores") {
  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore("cube.vs"), "cube_vs"));
  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore("cube."), "cube_"));
  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore("."), "_"));

  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore("cubevs"), "cubevs"));
  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore(""), ""));
  REQUIRE(!strcmp(convertNonAlphaOrNumberToUnderscore("  "), "__"));
}

TEST_CASE("Get file name from file path") {
  REQUIRE(!strcmp(getFilename("a.vert"), "a.vert"));
  REQUIRE(!strcmp(getFilename("./a.vert"), "a.vert"));
  REQUIRE(!strcmp(getFilename("./a b.vert"), "a b.vert"));
  REQUIRE(!strcmp(getFilename("/home/a b.vert"), "a b.vert"));
#ifdef _WIN32
  REQUIRE(!strcmp(getFilename("C:\\shader\\a b.vert"), "a b.vert"));
#endif
}

TEST_CASE("Convert shader to header file") {
    char** args = new char*[2];
    args[0] = "rectangle.vert";
    args[1] = "rectangle.vert";
    convertFilesToHeader(2, args);
}
