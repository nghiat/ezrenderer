#ifndef ANDROID
#include <Base/Base.h>

extern Ez::Base *base;


int main() {
  base->Init();
  base->Loop();
  delete base;
}
#endif
