#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanCommandPool.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
using namespace Ez;

VulkanCommandBuffer::VulkanCommandBuffer(
    std::shared_ptr<VulkanDevice> device,
    std::shared_ptr<VulkanCommandPool> &commandPool, VkCommandBufferLevel level)
    : m_Device(std::move(device)), m_CommandPool(commandPool) {
  auto wCommandPool = m_CommandPool.lock();
  VkCommandBufferAllocateInfo commandBufferAllocateInfo = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, // sType
      nullptr,                                        // pNext
      wCommandPool->GetCommandPool(),                 // commandPool
      level,                                          // level
      1                                               // commandBufferCount
  };
  VK_CHECK(vkAllocateCommandBuffers(
      m_Device->GetDevice(), &commandBufferAllocateInfo, &m_CommandBuffer));
}

VulkanCommandBuffer::~VulkanCommandBuffer() {
  auto wCommandPool = m_CommandPool.lock();
  vkFreeCommandBuffers(m_Device->GetDevice(), wCommandPool->GetCommandPool(), 1,
                       &m_CommandBuffer);
}
void VulkanCommandBuffer::Begin(
    VkCommandBufferUsageFlags flags,
    VkCommandBufferInheritanceInfo *inheritanceInfo) {
  VkCommandBufferBeginInfo beginInfo = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, // sType
      nullptr,                                     // pNext
      flags,                                       // flags
      nullptr                                      // pInheritanceInfo
  };
  vkBeginCommandBuffer(m_CommandBuffer, &beginInfo);
}
VkCommandBuffer VulkanCommandBuffer::GetCommandBuffer() {
  return m_CommandBuffer;
}
void VulkanCommandBuffer::End() { vkEndCommandBuffer(m_CommandBuffer); }

void VulkanCommandBuffer::Reset() { vkResetCommandBuffer(m_CommandBuffer, 0); }
