#include "VulkanAPI/VulkanDescriptorSet.h"
#include "VulkanAPI/VulkanBuffer.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanRenderTarget.h"
#include "VulkanAPI/VulkanTexture.h"

#define MAX_SETS 1

using namespace Ez;
VulkanDescriptorSet::VulkanDescriptorSet(
    std::shared_ptr<VulkanDevice> device,
    std::vector<VkDescriptorSetLayoutBinding> &layoutBindings)
    : m_Device(std::move(device)) {
  for (auto &layoutBinding : layoutBindings) {
    if (m_CountForTypes.find(layoutBinding.descriptorType) ==
        m_CountForTypes.end())
      m_CountForTypes[layoutBinding.descriptorType] = 0;
    m_CountForTypes[layoutBinding.descriptorType] +=
        layoutBinding.descriptorCount;
  }

  CreateDescriptorSetLayout(layoutBindings);
  CreateDescriptorPool();
  CreateDescriptorSet();
}
void VulkanDescriptorSet::CreateDescriptorSetLayout(
    std::vector<VkDescriptorSetLayoutBinding> &layoutBindings) {
  VkDescriptorSetLayoutCreateInfo setLayoutCreateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, // sType
      nullptr,                                             // pNext
      0,                                                   // flags
      static_cast<uint32_t>(layoutBindings.size()),        // bindingCount
      layoutBindings.data(),                               // pBindings
  };
  VK_CHECK(vkCreateDescriptorSetLayout(m_Device->GetDevice(),
                                       &setLayoutCreateInfo, nullptr,
                                       &m_DescriptorSetLayout));
}

VulkanDescriptorSet::~VulkanDescriptorSet() {
  vkDestroyDescriptorPool(m_Device->GetDevice(), m_DescriptorPool, nullptr);
  vkDestroyDescriptorSetLayout(m_Device->GetDevice(), m_DescriptorSetLayout,
                               nullptr);
}

void VulkanDescriptorSet::CreateDescriptorPool() {
  std::vector<VkDescriptorPoolSize> poolSizes;
  for (auto &type : m_CountForTypes) {
    VkDescriptorPoolSize poolSize = {
        static_cast<VkDescriptorType>(type.first), // type
        type.second,                               // descriptorCount
    };
    poolSizes.push_back(poolSize);
  }
  VkDescriptorPoolCreateInfo poolCreateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, // sType
      nullptr,                                       // pNext
      0,                                             // flags
      MAX_SETS,                                      // maxSets
      static_cast<uint32_t>(poolSizes.size()),       // poolSizeCount
      poolSizes.data()                               // pPoolSizes
  };
  VK_CHECK(vkCreateDescriptorPool(m_Device->GetDevice(), &poolCreateInfo,
                                  nullptr, &m_DescriptorPool));
}

void VulkanDescriptorSet::CreateDescriptorSet() {
  VkDescriptorSetAllocateInfo setAllocateInfo = {
      VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO, // sType
      nullptr,                                        // pNext
      m_DescriptorPool,                               // descriptorPool
      1,                                              // descriptorSetCount
      &m_DescriptorSetLayout                          // pSetLayouts
  };
  VK_CHECK(vkAllocateDescriptorSets(m_Device->GetDevice(), &setAllocateInfo,
                                    &m_DescriptorSet));
}

VkDescriptorSetLayout VulkanDescriptorSet::GetDescriptorSetLayout() {
  return m_DescriptorSetLayout;
}

void VulkanDescriptorSet::SetBuffer(uint32_t binding, uint32_t arrayIndex,
                                    std::unique_ptr<VulkanBuffer> &buffer,
                                    bool isDynamicUniform) {
  VkDescriptorBufferInfo descriptorBufferInfo = {
      buffer->GetBuffer(), buffer->GetMemoryOffset(), buffer->GetMemorySize()};
  VkWriteDescriptorSet writeDescriptorSet = {
      VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, // sType
      nullptr,                                // pNext
      m_DescriptorSet,                        // dstSet
      binding,                                // dstBingding
      arrayIndex,                             // dstArrayElement
      1,                                      // descriptorCount
      isDynamicUniform ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
                       : VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // descriptorType
      nullptr,                                              // pImageInfo
      &descriptorBufferInfo,                                // pBufferInfo
      nullptr                                               // pTexelBufferView
  };
  vkUpdateDescriptorSets(m_Device->GetDevice(), 1, &writeDescriptorSet, 0,
                         nullptr);
}

void VulkanDescriptorSet::SetTexture(uint32_t binding, uint32_t arrayIndex,
                                     VulkanTexture *texture) {
  VkDescriptorImageInfo imageInfo = {
      texture->GetSampler(),                   // sampler
      texture->GetImageView(),                 // imageView
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL // imageLayout
  };
  VkWriteDescriptorSet writeDescriptorSet = {
      VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,    // sType
      nullptr,                                   // pNext
      m_DescriptorSet,                           // dstSet
      binding,                                   // dstBingding
      arrayIndex,                                // dstArrayElement
      1,                                         // descriptorCount
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // descriptorType
      &imageInfo,                                // pImageInfo
      nullptr,                                   // pBufferInfo
      nullptr                                    // pTexelBufferView
  };
  vkUpdateDescriptorSets(m_Device->GetDevice(), 1, &writeDescriptorSet, 0,
                         nullptr);
}

void VulkanDescriptorSet::SetRenderTarget(
    uint32_t binding, uint32_t arrayIndex,
    std::unique_ptr<Ez::VulkanRenderTarget> &rt) {
  VkDescriptorImageInfo imageInfo = {
      rt->GetSampler(),                         // sampler
      rt->GetImageView(),                       // imageView
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, // imageLayout
  };
  VkWriteDescriptorSet writeDescriptorSet = {
      VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,    // sType
      nullptr,                                   // pNext
      m_DescriptorSet,                           // dstSet
      binding,                                   // dstBingding
      arrayIndex,                                // dstArrayElement
      1,                                         // descriptorCount
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // descriptorType
      &imageInfo,                                // pImageInfo
      nullptr,                                   // pBufferInfo
      nullptr                                    // pTexelBufferView
  };
  vkUpdateDescriptorSets(m_Device->GetDevice(), 1, &writeDescriptorSet, 0,
                         nullptr);
}

VkDescriptorSet VulkanDescriptorSet::GetDescriptorSet() {
  return m_DescriptorSet;
}
