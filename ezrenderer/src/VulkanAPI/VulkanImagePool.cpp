#include "VulkanAPI/VulkanImagePool.h"
#include "Util/PoolTracker2D.h"
#include "VulkanAPI/VulkanBufferPool.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanCommandPool.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanSynchronizationPrimitives.h"
#include <cstring>

using namespace Ez;

VulkanImagePool::VulkanImagePool(std::shared_ptr<VulkanDevice> device,
                                 std::shared_ptr<VulkanCommandPool> commandPool)
    : m_Device(std::move(device)), m_CommandPool(std::move(commandPool)) {
  m_CommandBuffer =
      std::make_unique<VulkanCommandBuffer>(m_Device, m_CommandPool);
  m_Fence = std::make_unique<VulkanFence>(m_Device);
  uint64_t dummyAlignment;
  VulkanBufferPool::CreateBuffer(
      m_Device, DEFAULT_IMAGE_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
          VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
      m_StagingBuffer, m_StagingMemory, dummyAlignment);
}

VulkanImagePool::~VulkanImagePool() {
  for (auto &bigImages : m_BigImagesMap) {
    for (auto &image : bigImages.second.images)
      vkDestroyImage(m_Device->GetDevice(), image, nullptr);
    for (auto &memory : bigImages.second.memories)
      vkFreeMemory(m_Device->GetDevice(), memory, nullptr);
  }

  vkDestroyBuffer(m_Device->GetDevice(), m_StagingBuffer, nullptr);
  vkFreeMemory(m_Device->GetDevice(), m_StagingMemory, nullptr);
}

VkImage VulkanImagePool::GetImage(VkFormat format, uint64_t index) {
  return m_BigImagesMap[format].images[index];
}

uint32_t VulkanImagePool::GetImageWidth(VkFormat format, uint64_t index) {
  return m_BigImagesMap[format].dimensions[index].first;
}

uint32_t VulkanImagePool::GetImageHeight(VkFormat format, uint64_t index) {
  return m_BigImagesMap[format].dimensions[index].second;
}

void VulkanImagePool::CreateImage2D(const std::shared_ptr<VulkanDevice> &device,
                                    VkFormat format, uint32_t width,
                                    uint32_t height, VkImage &image,
                                    VkDeviceMemory &memory, uint64_t &alignment,
                                    VkImageCreateFlags flags) {
  uint32_t queueFamilyIndex = 0;
  VkImageCreateInfo imageCreateInfo = {
      VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, // sType
      nullptr,                             // pNext
      flags,                               // flags
      VK_IMAGE_TYPE_2D,                    // imageType
      format,                              // format
      {
          // extent
          width,  // width
          height, // height
          1       // depth
      },
      1,                                      // mipLevels
      flags == 0 ? (uint32_t)1 : (uint32_t)6, // arrayLayers
      VK_SAMPLE_COUNT_1_BIT,                  // samples
      VK_IMAGE_TILING_OPTIMAL,                // titling
      VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT |
          VK_IMAGE_USAGE_TRANSFER_SRC_BIT, // usage
      VK_SHARING_MODE_EXCLUSIVE,           // sharingMode
      1,                                   // queueFamilyIndexCount
      &queueFamilyIndex,                   // pQueueFamilyIndices
      VK_IMAGE_LAYOUT_UNDEFINED            // initialLayout
  };
  VK_CHECK(
      vkCreateImage(device->GetDevice(), &imageCreateInfo, nullptr, &image));

  VkMemoryRequirements req;
  vkGetImageMemoryRequirements(device->GetDevice(), image, &req);
  VkMemoryAllocateInfo allocateInfo = {
      VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // sType
      nullptr,                                // pNext
      req.size,                               // allocationSize
      device->GetMemoryTypeIndex(
          req.memoryTypeBits,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) // memoryTypeIndex
  };
  VK_CHECK(
      vkAllocateMemory(device->GetDevice(), &allocateInfo, nullptr, &memory));
  VK_CHECK(vkBindImageMemory(device->GetDevice(), image, memory, 0));
  alignment = req.alignment;
}

void VulkanImagePool::AllocateSubImage(VkFormat format, uint32_t width,
                                       uint32_t height, uint8_t channels,
                                       void *data, uint64_t &imageIndex,
                                       uint32_t &offsetX, uint32_t &offsetY) {
  // We add 1 more row and column to the subtexture to prevent some wired
  // artifacts
  FindSubImageRegion(format, width + 1, height + 1, imageIndex, offsetX,
                     offsetY);
  if (data)
    CopyBufferToImage(format, imageIndex, offsetX, offsetY, width, height,
                      channels, data);
}

void VulkanImagePool::Deallocate(VkFormat format, uint64_t imageIndex,
                                 uint32_t offsetX, uint32_t offsetY,
                                 uint32_t width, uint32_t height) {
  m_BigImagesMap[format].poolTracker->Deallocate(imageIndex, offsetX, offsetY,
                                                 width, height);
}

void VulkanImagePool::FindSubImageRegion(VkFormat format, uint32_t width,
                                         uint32_t height, uint64_t &imageIndex,
                                         uint32_t &offsetX, uint32_t &offsetY) {
  uint64_t size = width * height;
  if (m_BigImagesMap.find(static_cast<uint32_t>(format)) ==
      m_BigImagesMap.end()) {
    m_BigImagesMap.insert(std::make_pair(
        static_cast<uint32_t>(format),
        BigImages{std::vector<VkImage>(0), std::vector<VkDeviceMemory>(0),
                  std::vector<bool>(0),
                  std::vector<std::pair<uint32_t, uint32_t>>(0),
                  std::make_unique<PoolTracker2D>(DEFAULT_IMAGE_WIDTH,
                                                  DEFAULT_IMAGE_HEIGHT)}));
  }
  auto result = m_BigImagesMap[format].poolTracker->AllocateSubRegion(
      imageIndex, offsetX, offsetY, width, height);
  if (!result) {
    VkImage newImage;
    VkDeviceMemory newMemory;
    uint64_t alignment;
    CreateImage2D(m_Device, format, width, height, newImage, newMemory,
                  alignment);
    m_BigImagesMap[format].poolTracker->SetLastMetadataAlignment(alignment);
    m_BigImagesMap[format].images.push_back(newImage);
    m_BigImagesMap[format].memories.push_back(newMemory);
    m_BigImagesMap[format].dimensions.push_back(std::make_pair(width, height));
    m_BigImagesMap[format].isInit.push_back(false);
  }
}

void VulkanImagePool::CopyBufferToImage(VkFormat format, uint64_t imageIndex,
                                        uint32_t offsetX, uint32_t offsetY,
                                        uint32_t width, uint32_t height,
                                        uint8_t channels, void *data) {
  uint64_t size = width * height * channels;
  for (uint64_t start = 0; start < size; start += DEFAULT_IMAGE_SIZE) {
    void *mappedData;
    uint64_t copySize =
        size - start < DEFAULT_IMAGE_SIZE ? size - start : DEFAULT_IMAGE_SIZE;
    vkMapMemory(m_Device->GetDevice(), m_StagingMemory, start, copySize, 0,
                &mappedData);
    std::memcpy(mappedData, data, copySize);
    vkUnmapMemory(m_Device->GetDevice(), m_StagingMemory);

    m_CommandBuffer->Begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    VkImageSubresourceRange subresourceRange = {
        VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
        0,                         // baseMipLevel
        1,                         // levelCount
        0,                         // baseArrayLayer
        1                          // layerCount
    };
    if (!m_BigImagesMap[format].isInit[imageIndex]) {
      TransitionImageLayout(
          m_CommandBuffer, m_BigImagesMap[format].images[imageIndex],
          VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          subresourceRange);
      m_BigImagesMap[format].isInit[imageIndex] = true;
    } else {
      TransitionImageLayout(
          m_CommandBuffer, m_BigImagesMap[format].images[imageIndex],
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);
    }
    VkBufferImageCopy bufferImageCopy = {
        0, // bufferOffset
        0, // bufferRowLength
        0, // bufferImageHeight
        {
            // imageSubresource
            VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
            0,                         // mipLevel
            0,                         // baseArrayLayer
            1,                         // layerCount
        },
        {
            // imageOffset
            static_cast<int32_t>(offsetX + start % width), // x
            static_cast<int32_t>(offsetY + start / width), // y
            0                                              // z
        },
        {
            // imageExtent
            width,  // x
            height, // y
            1       // z
        }};
    vkCmdCopyBufferToImage(m_CommandBuffer->GetCommandBuffer(), m_StagingBuffer,
                           m_BigImagesMap[format].images[imageIndex],
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
                           &bufferImageCopy);
    TransitionImageLayout(
        m_CommandBuffer, m_BigImagesMap[format].images[imageIndex],
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
    m_CommandBuffer->End();
    // TODO reuse
    VkCommandBuffer pCommandBuffers[] = {m_CommandBuffer->GetCommandBuffer()};
    VkSubmitInfo submitInfo = {
        VK_STRUCTURE_TYPE_SUBMIT_INFO, // sType
        nullptr,                       // pNext
        0,                             // waitSemaphoreCount
        nullptr,                       // pWaitSemaphores
        nullptr,                       // pWaitSemaphores
        1,                             // commandBufferCount
        pCommandBuffers,               // pCommandBuffers
        0,                             // signalSemaphoreCount
        nullptr                        // pSignalSemaphores
    };
    VK_CHECK(vkQueueSubmit(m_Device->GetDeviceQueue(0, 0), 1, &submitInfo,
                           m_Fence->GetFence()));
    m_Fence->Wait();
    m_Fence->Reset();
    m_CommandBuffer->Reset();
  }
}

void VulkanImagePool::TransitionImageLayout(
    std::unique_ptr<VulkanCommandBuffer> &commandBuffer, VkImage &image,
    VkImageLayout oldLayout, VkImageLayout newLayout,
    VkImageSubresourceRange &subresourceRange) {
  VkImageMemoryBarrier imageMemoryBarrier = {
      VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, // sType
      nullptr,                                // pNext
      0,                                      // srcAccessMask
      0,                                      // dstAccessMask
      oldLayout,                              // oldLayout
      newLayout,                              // newLayout
      0,                                      // srcQueueFamilyIndex
      0,                                      // dstQueueFamilyIndex
      image,                                  // image
      subresourceRange                        // subresourceRange
  };

  switch (oldLayout) {
  case VK_IMAGE_LAYOUT_UNDEFINED:
    // Only valid as initial layout, memory contents are not preserved
    // Can be accessed directly, no source dependency required
    imageMemoryBarrier.srcAccessMask = 0;
    break;
  case VK_IMAGE_LAYOUT_PREINITIALIZED:
    // Only valid as initial layout for linear images, preserves memory contents
    // Make sure host writes to the image have been finished
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
    break;
  case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
    // Old layout is transfer destination
    // Make sure any writes to the image have been finished
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    break;
  case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
  default:
    break;
  }

  // Target layouts (new)
  switch (newLayout) {
  case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
    // Transfer source (copy, blit)
    // Make sure any reads from the image have been finished
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    break;
  case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
    // Transfer destination (copy, blit)
    // Make sure any writes to the image have been finished
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    break;
  case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
    // Shader read (sampler, input attachment)
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    break;
  default:
    break;
  }

  // Put barrier on top of pipeline
  VkPipelineStageFlags srcStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
  VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

  // Put barrier inside setup command buffer
  vkCmdPipelineBarrier(commandBuffer->GetCommandBuffer(), srcStageFlags,
                       destStageFlags, 0, 0, nullptr, 0, nullptr, 1,
                       &imageMemoryBarrier);
}
