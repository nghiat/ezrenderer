#include "VulkanAPI/VulkanTextureCube.h"
#include "VulkanAPI/VulkanBufferPool.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanCommandPool.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanImagePool.h"
#include "VulkanAPI/VulkanSynchronizationPrimitives.h"
#include <Util/TextureCube.h>
#include <cstring>

using namespace Ez;

VulkanTextureCube::VulkanTextureCube(
    std::shared_ptr<VulkanDevice> device,
    std::shared_ptr<VulkanCommandPool> commandPool, TextureCube *texture)
    : VulkanTexture() {
  m_Device = std::move(device);
  m_Width = texture->GetX();
  m_Height = texture->GetY();
  m_Format = VK_FORMAT_R16G16B16A16_SFLOAT;
  m_OffsetX = m_OffsetY = 0;
  m_Channels = texture->GetChannel();
  bool isInit = false;
  auto commandBuffer =
      std::make_unique<VulkanCommandBuffer>(m_Device, commandPool);
  uint64_t dummyAlignment;
  VulkanImagePool::CreateImage2D(
      m_Device, m_Format, texture->GetX(), texture->GetY(), m_Image,
      m_ImageMemory, dummyAlignment, VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);
  CopyDataToImage(commandBuffer, texture);
  // Format used for image view
  CreateSampler();
  CreateImageView(VK_IMAGE_VIEW_TYPE_CUBE);
}

VulkanTextureCube::~VulkanTextureCube() {
  vkDestroyImage(m_Device->GetDevice(), m_Image, nullptr);
  vkFreeMemory(m_Device->GetDevice(), m_ImageMemory, nullptr);
}

void VulkanTextureCube::CopyDataToImage(
    std::unique_ptr<VulkanCommandBuffer> &commandBuffer, TextureCube *texture) {
  auto imageFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
  uint64_t dummyAlignment;
  VkImage tempImage;
  VkDeviceMemory tempMemory;
  VulkanImagePool::CreateImage2D(
      m_Device, imageFormat, texture->GetX(), texture->GetY(), tempImage,
      tempMemory, dummyAlignment, VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);
  VkBuffer stagingBuffer;
  VkDeviceMemory stagingMemory;
  VulkanBufferPool::CreateBuffer(m_Device, DEFAULT_IMAGE_SIZE,
                                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                     VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                 stagingBuffer, stagingMemory, dummyAlignment);
  auto data = texture->GetFacesData();
  float *mappedData;
  uint64_t faceSize = m_Width * m_Height * m_Channels * sizeof(float);
  vkMapMemory(m_Device->GetDevice(), stagingMemory, 0, 6 * faceSize, 0,
              (void **)(&mappedData));
  for (int i = 0; i < 6; i++) {
    std::memcpy(mappedData + i * faceSize / sizeof(float), data[i], faceSize);
  }
  vkUnmapMemory(m_Device->GetDevice(), stagingMemory);

  commandBuffer->Begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
  VkImageSubresourceRange subresourceRange = {
      VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
      0,                         // baseMipLevel
      1,                         // levelCount
      0,                         // baseArrayLayer
      6                          // layerCount
  };
  VulkanImagePool::TransitionImageLayout(
      commandBuffer, tempImage, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);
  std::vector<VkBufferImageCopy> imageCopies(6);
  for (int i = 0; i < 6; i++) {
    imageCopies[i] = {faceSize * i, // bufferOffset
                      0,            // bufferRowLength
                      0,            // bufferImageHeight
                      {
                          // imageSubresource
                          VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
                          0,                         // mipLevel
                          (uint32_t)i,               // baseArrayLayer
                          1,                         // layerCount
                      },
                      {
                          // imageOffset
                          0, // x
                          0, // y
                          0  // z
                      },
                      {
                          // imageExtent
                          m_Width,  // width
                          m_Height, // height
                          1         // depth
                      }};
  }
  vkCmdCopyBufferToImage(commandBuffer->GetCommandBuffer(), stagingBuffer,
                         tempImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                         imageCopies.size(), imageCopies.data());
  VulkanImagePool::TransitionImageLayout(
      commandBuffer, tempImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subresourceRange);
  VulkanImagePool::TransitionImageLayout(
      commandBuffer, m_Image, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);
  VkImageSubresourceLayers subresourceLayers = {
      VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
      0,                         // mipLevel
      0,                         // baseArrayLayer
      6,                         // layerCount
  };
  VkImageBlit region = {
      subresourceLayers,                                  // srcSubresource;
      {{0, 0, 0}, {texture->GetX(), texture->GetY(), 1}}, //  srcOffsets[2];
      subresourceLayers,                                  // dstSubresource;
      {{0, 0, 0}, {texture->GetX(), texture->GetY(), 6}}  // dstOffsets[2];
  };
  vkCmdBlitImage(commandBuffer->GetCommandBuffer(), tempImage,
                 VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, m_Image,
                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region,
                 VK_FILTER_LINEAR);
  VulkanImagePool::TransitionImageLayout(
      commandBuffer, m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
  commandBuffer->End();
  // TODO reuse
  VkCommandBuffer pCommandBuffers[] = {commandBuffer->GetCommandBuffer()};
  VkSubmitInfo submitInfo = {
      VK_STRUCTURE_TYPE_SUBMIT_INFO, // sType
      nullptr,                       // pNext
      0,                             // waitSemaphoreCount
      nullptr,                       // pWaitSemaphores
      nullptr,                       // pWaitSemaphores
      1,                             // commandBufferCount
      pCommandBuffers,               // pCommandBuffers
      0,                             // signalSemaphoreCount
      nullptr                        // pSignalSemaphores
  };
  auto fence = std::make_unique<VulkanFence>(m_Device);
  VK_CHECK(vkQueueSubmit(m_Device->GetDeviceQueue(0, 0), 1, &submitInfo,
                         fence->GetFence()));
  fence->Wait();
  fence->Reset();
  commandBuffer->Reset();
  vkDestroyImage(m_Device->GetDevice(), tempImage, nullptr);
  vkFreeMemory(m_Device->GetDevice(), tempMemory, nullptr);
  vkDestroyBuffer(m_Device->GetDevice(), stagingBuffer, nullptr);
  vkFreeMemory(m_Device->GetDevice(), stagingMemory, nullptr);
}
