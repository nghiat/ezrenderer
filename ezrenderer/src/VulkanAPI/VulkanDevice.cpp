#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanInstance.h"
#include <Util/Log.h>
#include <vector>

using namespace Ez;

VulkanDevice::VulkanDevice(std::shared_ptr<VulkanInstance> instance,
                           uint32_t mainPhysicalDeviceIndex)
    : m_Instance(std::move(instance)) {
  SetMainPhysicalDevice(mainPhysicalDeviceIndex);
  CreateLogicalDevice();
}

VulkanDevice::~VulkanDevice() { vkDestroyDevice(m_Device, nullptr); }

void VulkanDevice::SetMainPhysicalDevice(uint32_t index) {
  uint32_t devicesCount;
  vkEnumeratePhysicalDevices(m_Instance->GetInstance(), &devicesCount, nullptr);
  std::vector<VkPhysicalDevice> physicalDevices(devicesCount);
  vkEnumeratePhysicalDevices(m_Instance->GetInstance(), &devicesCount,
                             physicalDevices.data());
  if (index < devicesCount)
    m_MainPhysicalDevice = physicalDevices[index];
  else {
    m_MainPhysicalDevice = physicalDevices[0];
    LOGW("VulkanDevice",
         "Can't find physical device with index %d, use the first "
         "physical device as the main physcail device",
         index);
  }
  vkGetPhysicalDeviceMemoryProperties(m_MainPhysicalDevice,
                                      &m_MainPhysicalDeviceMemoryProperties);
  vkGetPhysicalDeviceFeatures(m_MainPhysicalDevice,
                              &m_MainPhysicalDeviceFeatures);
  vkGetPhysicalDeviceProperties(m_MainPhysicalDevice,
                                &m_MainPhysicalDeviceProperties);
}

void VulkanDevice::ChooseDepthFormat() {
  std::vector<VkFormat> depthFormats = {
      VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D32_SFLOAT,
      VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT,
      VK_FORMAT_D16_UNORM};

  for (auto &format : depthFormats) {
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(m_MainPhysicalDevice, format,
                                        &formatProperties);
    // Format must support depth stencil attachment for optimal tiling
    if (formatProperties.optimalTilingFeatures &
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
      m_DepthFormat = format;
      return;
    }
  }
  LOGE("VulkanDevice", "Couldn't pick any depth format");
  throw "Couldn't pick any depth format";
}

void VulkanDevice::CreateLogicalDevice() {
  float priorities[] = {1.f};
  VkDeviceQueueCreateInfo deviceQueueCreateInfo = {
      VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, // sType
      nullptr,                                    // pNext
      0,                                          // flags
      0,                                          // queueFamilyIndex
      1,                                          // queueCount
      priorities,                                 // pQueuePriorities
  };
  VkPhysicalDeviceFeatures physicalDeviceFeatures;
  vkGetPhysicalDeviceFeatures(m_MainPhysicalDevice, &physicalDeviceFeatures);

  std::vector<const char *> enabledDeviceExtensionNames = {"VK_KHR_swapchain"};
  VkDeviceCreateInfo deviceCreateInfo = {
      VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO, // sType
      nullptr,                              // pNext
      0,                                    // flags
      1,                                    // queueCreateInfoCount
      &deviceQueueCreateInfo,               // pQueueCreateInfos
      0,                                    // enabledLayerCount
      nullptr,                              // ppEnabledLayerNames
      static_cast<uint32_t>(
          enabledDeviceExtensionNames.size()), // enabledExtensionCount
      enabledDeviceExtensionNames.data(),      // ppEnabledExtensionNames
      &physicalDeviceFeatures                  // pEnabledFeatures
  };
  uint32_t count;
  vkGetPhysicalDeviceQueueFamilyProperties(m_MainPhysicalDevice, &count,
                                           nullptr);
  std::vector<VkQueueFamilyProperties> properties(count);
  vkGetPhysicalDeviceQueueFamilyProperties(m_MainPhysicalDevice, &count,
                                           properties.data());
  VK_CHECK(vkCreateDevice(m_MainPhysicalDevice, &deviceCreateInfo, nullptr,
                          &m_Device));
}

VkFormat VulkanDevice::GetDepthFormat() {
  if (m_DepthFormat == VK_FORMAT_UNDEFINED)
    ChooseDepthFormat();
  return m_DepthFormat;
}

VkQueue VulkanDevice::GetDeviceQueue(uint32_t queueFamilyIndex,
                                     uint32_t queueIndex) {
  VkQueue queue;
  vkGetDeviceQueue(m_Device, queueFamilyIndex, queueIndex, &queue);
  return queue;
}

uint32_t
VulkanDevice::GetMemoryTypeIndex(uint32_t typeBits,
                                 VkMemoryPropertyFlags propertyFlagBits) {
  for (uint32_t i = 0; i < m_MainPhysicalDeviceMemoryProperties.memoryTypeCount;
       i++) {
    if ((typeBits & 1) == 1) {
      if ((m_MainPhysicalDeviceMemoryProperties.memoryTypes[i].propertyFlags &
           propertyFlagBits) == propertyFlagBits)
        return i;
    }
    typeBits >>= 1;
  }
  LOGE("VulkanDevice", "Can't find an appropriate memory type index");
  throw "Can't find an appropriate memory type index";
}

VkPhysicalDeviceFeatures VulkanDevice::GetFeatures() const {
  return m_MainPhysicalDeviceFeatures;
}

VkPhysicalDeviceProperties VulkanDevice::GetProperties() const {
  return m_MainPhysicalDeviceProperties;
}
