#include "VulkanAPI/VulkanRenderTarget.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanSwapchain.h"
#include "VulkanAPI/VulkanSynchronizationPrimitives.h"
#include <limits>
#include <Util/Log.h>

using namespace Ez;

VulkanRenderTarget::VulkanRenderTarget(
    std::shared_ptr<VulkanDevice> device,
    std::shared_ptr<VulkanSwapchain> swapchain,
    std::shared_ptr<VulkanCommandPool> &commandPool)
    : m_Device(std::move(device)), m_Swapchain(std::move(swapchain)),
      m_UseSwapchain(true) {
  m_Width = m_Swapchain->GetWidth();
  m_Height = m_Swapchain->GetHeight();
  CreateDepthStencilImageAndView(m_Device->GetDepthFormat(), m_Width, m_Height);
  CreateRenderpass(m_Swapchain->GetSurfaceFormat());
  m_Framebuffers.resize(m_Swapchain->GetImageCount());

  for (uint32_t i = 0; i < m_Framebuffers.size(); i++) {
    std::vector<VkImageView> attachments = {
        m_Swapchain->GetImageView(i),
        m_DepthStencilImageView // same for all framebuffers
    };

    VkFramebufferCreateInfo framebufferInfo = {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, // sType
        nullptr,                                   // pNext
        0,                                         // flags
        m_RenderPass,                              // renderPass
        2,                                         // attachmentCount
        attachments.data(),                        // pAttachments
        m_Swapchain->GetSurfaceCapabilities().currentExtent.width,  // width
        m_Swapchain->GetSurfaceCapabilities().currentExtent.height, // height
        1,                                                          // layers
    };
    VK_CHECK(vkCreateFramebuffer(m_Device->GetDevice(), &framebufferInfo,
                                 nullptr, &m_Framebuffers[i]));
    m_CommandBuffers.push_back(
        std::make_unique<VulkanCommandBuffer>(m_Device, commandPool));
  }
  m_Queue = m_Device->GetDeviceQueue(0, 0);
  m_ImageAvailableSemaphore = std::make_unique<VulkanSemaphore>(m_Device);
  m_RenderFinishedSemaphore = std::make_unique<VulkanSemaphore>(m_Device);
}

VulkanRenderTarget::VulkanRenderTarget(
    std::shared_ptr<VulkanDevice> device,
    std::shared_ptr<VulkanCommandPool> &commandPool, uint32_t width,
    uint32_t height)
    : m_Device(std::move(device)), m_Width(width), m_Height(height) {
  uint32_t pQueueFamilyIndices[] = {0};
  VkImageCreateInfo imageCI = {
      VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, // sType
      nullptr,                             // pNext
      0,                                   // flags
      VK_IMAGE_TYPE_2D,                    // imageType
      VK_FORMAT_R16G16B16A16_SFLOAT,       // format
      {
          // extent
          width,  // width
          height, // height
          1       // depth
      },
      1,                       // mipLevels
      1,                       // arrayLayers
      VK_SAMPLE_COUNT_1_BIT,   // samples
      VK_IMAGE_TILING_OPTIMAL, // titling
      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, // usage
      VK_SHARING_MODE_EXCLUSIVE, // sharingMode
      1,                         // queueFamilyIndexCount
      pQueueFamilyIndices,       // pQueueFamilyIndices
      VK_IMAGE_LAYOUT_UNDEFINED  // initialLayout
  };
  VK_CHECK(
      vkCreateImage(m_Device->GetDevice(), &imageCI, nullptr, &m_ColorImage));

  VkMemoryRequirements req;
  vkGetImageMemoryRequirements(m_Device->GetDevice(), m_ColorImage, &req);
  VkMemoryAllocateInfo memoryAllocateInfo = {
      VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // sType
      nullptr,                                // pNext
      req.size,                               // allocationSize
      m_Device->GetMemoryTypeIndex(
          req.memoryTypeBits,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) // memoryTypeIndex
  };
  VK_CHECK(vkAllocateMemory(m_Device->GetDevice(), &memoryAllocateInfo, nullptr,
                            &m_ColorImageMemory));
  VK_CHECK(vkBindImageMemory(m_Device->GetDevice(), m_ColorImage,
                             m_ColorImageMemory, 0));

  VkImageViewCreateInfo imageViewCI = {
      VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, // sType
      nullptr,                                  // pNext
      0,                                        // flags
      m_ColorImage,                             // image
      VK_IMAGE_VIEW_TYPE_2D,                    // viewType
      VK_FORMAT_R16G16B16A16_SFLOAT,            // format
      {
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.r
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.g
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.b
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.a
      },
      {
          VK_IMAGE_ASPECT_COLOR_BIT, // subresourceRange.aspectMask
          0,                         // subresourceRange.baseMipLevel
          1,                         // subresourceRange.levelCount
          0,                         // subresourceRange.baseArrayLayer
          1,                         // subresourceRange.layerCount
      }};
  VK_CHECK(vkCreateImageView(m_Device->GetDevice(), &imageViewCI, nullptr,
                             &m_ColorImageView));

  CreateDepthStencilImageAndView(m_Device->GetDepthFormat(), width, height);
  CreateRenderpass(VK_FORMAT_R16G16B16A16_SFLOAT);
  VkImageView attachments[2] = {m_ColorImageView, m_DepthStencilImageView};
  VkFramebufferCreateInfo framebufferCI = {
      VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, // sType
      nullptr,                                   // pNext
      0,                                         // flags
      m_RenderPass,                              // renderpass
      2,                                         // attachmentCount
      attachments,                               // pAttachments
      width,                                     // width
      height,                                    // height
      1,                                         // layers
  };
  m_Framebuffers.resize(1);
  VK_CHECK(vkCreateFramebuffer(m_Device->GetDevice(), &framebufferCI, nullptr,
                               &m_Framebuffers[0]));
  VkSamplerCreateInfo samplerCreateInfo = {
      VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO, // sType
      nullptr,                               // pNext
      0,                                     // flags
      VK_FILTER_LINEAR,                      // magFilter
      VK_FILTER_LINEAR,                      // minFilter
      VK_SAMPLER_MIPMAP_MODE_LINEAR,         // mipmapMode
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeU
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeV
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeW
      0.f,                                   // mipLodBias
      VK_FALSE,                              // anisotropyEnable
      0.f,                                   // maxAnisotropy
      VK_FALSE,                              // compareEnable
      VK_COMPARE_OP_ALWAYS,                  // compareOp
      0.f,                                   // minLod
      0.f,                                   // maxLod
      VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,    // borderColor
      VK_FALSE                               // unnomalizedCoordinates
  };
  VK_CHECK(vkCreateSampler(m_Device->GetDevice(), &samplerCreateInfo, nullptr,
                           &m_Sampler));
  m_CommandBuffers.push_back(
      std::make_unique<VulkanCommandBuffer>(m_Device, commandPool));
  m_Queue = m_Device->GetDeviceQueue(0, 0);
  m_SeparateSemaphore = std::make_unique<VulkanSemaphore>(m_Device);
}

VulkanRenderTarget::~VulkanRenderTarget() {
  if (!m_UseSwapchain) {
    vkDestroyImageView(m_Device->GetDevice(), m_ColorImageView, nullptr);
    vkDestroyImage(m_Device->GetDevice(), m_ColorImage, nullptr);
    vkFreeMemory(m_Device->GetDevice(), m_ColorImageMemory, nullptr);
  }
  vkDestroyImageView(m_Device->GetDevice(), m_DepthStencilImageView, nullptr);
  vkDestroyImage(m_Device->GetDevice(), m_DepthStencilImage, nullptr);
  vkFreeMemory(m_Device->GetDevice(), m_DepthStencilImageMemory, nullptr);
  for (auto &framebuffer : m_Framebuffers)
    vkDestroyFramebuffer(m_Device->GetDevice(), framebuffer, nullptr);
  if (m_Sampler)
    vkDestroySampler(m_Device->GetDevice(), m_Sampler, nullptr);
  vkDestroyRenderPass(m_Device->GetDevice(), m_RenderPass, nullptr);
}

void VulkanRenderTarget::SetDrawCallback(
    std::function<void(VkCommandBuffer commandBuffer)> f) {
  m_Drawcallback = f;
  VkClearValue clearValues[2];
  clearValues[0].color = {1.f, 1.f, 1.f, 1.f};
  clearValues[1].depthStencil = {1.0f, 0};
  VkRenderPassBeginInfo renderPassBeginInfo = {
      VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO, // sType
      nullptr,                                  // pNext
      m_RenderPass,                             // renderPass
      VK_NULL_HANDLE,                           // framebuffer
      {                                         // renderArea
       {
           // offset
           0, // x
           0  // y
       },
       {
           // extent
           m_Width,  // width
           m_Height, // height
       }},
      2,           // clearValueCount
      clearValues, // pClearValue
  };
  for (uint32_t i = 0; i < m_Framebuffers.size(); i++) {
    m_CommandBuffers[i]->Begin(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
    renderPassBeginInfo.framebuffer = m_Framebuffers[i];
    vkCmdBeginRenderPass(m_CommandBuffers[i]->GetCommandBuffer(),
                         &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
    m_Drawcallback(m_CommandBuffers[i]->GetCommandBuffer());
    vkCmdEndRenderPass(m_CommandBuffers[i]->GetCommandBuffer());
    m_CommandBuffers[i]->End();
  }
}

VkRenderPass VulkanRenderTarget::GetRenderPass() const { return m_RenderPass; }

void VulkanRenderTarget::Draw() {
  vkAcquireNextImageKHR(m_Device->GetDevice(), m_Swapchain->GetSwapchain(),
                        std::numeric_limits<uint64_t>::max(),
                        m_ImageAvailableSemaphore->GetSemaphore(),
                        VK_NULL_HANDLE, &m_CurrentImageIndex);
  std::vector<VkPipelineStageFlags> waitStages = {
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  VkSemaphore waitSemaphore = m_ImageAvailableSemaphore->GetSemaphore();
  std::vector<VkSemaphore> finalWaitSemaphores;
  VkSubmitInfo submitInfo = {};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitDstStageMask = waitStages.data();
  submitInfo.pWaitSemaphores = &waitSemaphore;
  if (m_NextRendertargets.size()) {
    std::vector<VkSemaphore> signalSemaphores;
    std::vector<VkCommandBuffer> cbs;
    for (auto &rt : m_NextRendertargets) {
      signalSemaphores.push_back(rt->m_SeparateSemaphore->GetSemaphore());
      cbs.push_back(rt->m_CommandBuffers[0]->GetCommandBuffer());
    }
    submitInfo.signalSemaphoreCount = signalSemaphores.size();
    submitInfo.pSignalSemaphores = signalSemaphores.data();
    submitInfo.commandBufferCount = cbs.size();
    submitInfo.pCommandBuffers = cbs.data();
    VK_CHECK(vkQueueSubmit(m_Queue, 1, &submitInfo, VK_NULL_HANDLE));
    finalWaitSemaphores = CreateSubmitsChain(m_NextRendertargets);
    submitInfo.waitSemaphoreCount = finalWaitSemaphores.size();
    submitInfo.pWaitSemaphores = finalWaitSemaphores.data();
  }
  waitStages.resize(finalWaitSemaphores.size());
  for (auto &waitStage : waitStages)
    waitStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  submitInfo.pWaitDstStageMask = waitStages.data();
  submitInfo.signalSemaphoreCount = 1;
  auto finalSignalSemaphore = m_RenderFinishedSemaphore->GetSemaphore();
  submitInfo.pSignalSemaphores = &finalSignalSemaphore;
  auto commandBuffer =
      m_CommandBuffers[m_CurrentImageIndex]->GetCommandBuffer();
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;
  VK_CHECK(vkQueueSubmit(m_Queue, 1, &submitInfo, VK_NULL_HANDLE));
  VkSwapchainKHR swapChains[] = {m_Swapchain->GetSwapchain()};
  VkPresentInfoKHR presentInfoKHR = {
      VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, // sType
      nullptr,                            // pNext
      1,                                  // waitSemaphoreCount
      &finalSignalSemaphore,              // pWaitSemaphores
      1,                                  // swapchainCount
      swapChains,                         // pSwapchains
      &m_CurrentImageIndex,               // pImageIndices
      nullptr,                            // pResults
  };
  vkQueuePresentKHR(m_Queue, &presentInfoKHR);
}

VkImage VulkanRenderTarget::GetImage() { return m_ColorImage; }

VkImageView VulkanRenderTarget::GetImageView() { return m_ColorImageView; }

VkSampler VulkanRenderTarget::GetSampler() { return m_Sampler; }

uint32_t VulkanRenderTarget::GetWidth() { return m_Width; }

uint32_t VulkanRenderTarget::GetHeight() { return m_Height; }

void VulkanRenderTarget::AddNextRendertarget(VulkanRenderTarget *rt) {
  m_NextRendertargets.push_back(rt);
}

void VulkanRenderTarget::CreateRenderpass(VkFormat format) {
  VkAttachmentDescription attachments[2];
  attachments[0] = {
      0,                                // flags
      format,                           // format
      VK_SAMPLE_COUNT_1_BIT,            // samples
      VK_ATTACHMENT_LOAD_OP_CLEAR,      // loadOp
      VK_ATTACHMENT_STORE_OP_STORE,     // storeOp
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,  // stencilLoadOp
      VK_ATTACHMENT_STORE_OP_DONT_CARE, // stencilStoreOp
      VK_IMAGE_LAYOUT_UNDEFINED,        // initialLayout
      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,  // finalLayout
  };

  attachments[1] = {
      0,                                               // flags
      m_Device->GetDepthFormat(),                      // format
      VK_SAMPLE_COUNT_1_BIT,                           // samples
      VK_ATTACHMENT_LOAD_OP_CLEAR,                     // loadOp
      VK_ATTACHMENT_STORE_OP_STORE,                    // storeOp
      VK_ATTACHMENT_LOAD_OP_DONT_CARE,                 // stencilLoadOp
      VK_ATTACHMENT_STORE_OP_DONT_CARE,                // stencilStoreOp
      VK_IMAGE_LAYOUT_UNDEFINED,                       // initialLayout
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // finalLayout
  };

  VkAttachmentReference colorAttachmentRef = {
      0,                                        // attachment
      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, // layout
  };
  VkAttachmentReference depthAttachmentRef = {
      1,                                               // attachment
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL // layout
  };
  VkSubpassDescription subpass = {
      0,                               // flags
      VK_PIPELINE_BIND_POINT_GRAPHICS, // pipelineBindPoint
      0,                               // inputAttachmentCount
      nullptr,                         // pInputAttachments
      1,                               // colorAttachmentCount
      &colorAttachmentRef,             // pColorAttachments
      nullptr,                         // pResolveAttachments
      &depthAttachmentRef,             // pDepthStencilAttachment
      0,                               // preserveAttachmentCount
      nullptr,                         // pPreserveAttachments
  };

  //  VkSubpassDependency dependencies[2];
  //  dependencies[0] = {
  //      VK_SUBPASS_EXTERNAL,                           // srcSubpass
  //      0,                                             // dstSubpass
  //      VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,          // srcStageMask
  //      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // dstStageMask
  //      VK_ACCESS_MEMORY_READ_BIT,                     // srcAccessMask
  //      VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |          // dstAccessMask
  //          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
  //      VK_DEPENDENCY_BY_REGION_BIT, // dependencyFlags
  //  };
  //
  //  dependencies[1] = {
  //      VK_SUBPASS_EXTERNAL,                           // srcSubpass
  //      0,                                             // dstSubpass
  //      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // srcStageMask
  //      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // dstStageMask
  //      VK_ACCESS_MEMORY_READ_BIT |
  //          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, // srcAccessMask
  //      VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,      // dstAccessMask
  //      VK_DEPENDENCY_BY_REGION_BIT,              // dependencyFlags
  //  };
  VkRenderPassCreateInfo renderPassCreateInfo = {
      VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO, // sType
      nullptr,                                   // pNext
      0,                                         // flags
      2,                                         // attachmentCount
      attachments,                               // pAttachments
      1,                                         // subpassCount
      &subpass,                                  // pSubpasses
      0,                                         // dependencyCount
      nullptr,                                   // pDependencies
  };
  VK_CHECK(vkCreateRenderPass(m_Device->GetDevice(), &renderPassCreateInfo,
                              nullptr, &m_RenderPass));
}

void VulkanRenderTarget::CreateDepthStencilImageAndView(VkFormat depthFormat,
                                                        uint32_t width,
                                                        uint32_t height) {
  uint32_t pQueueFamilyIndices[] = {0};
  VkImageCreateInfo imageCI = {
      VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, // sType
      nullptr,                             // pNext
      0,                                   // flags
      VK_IMAGE_TYPE_2D,                    // imageType
      depthFormat,                         // format
      {
          // extent
          width,  // width
          height, // height
          1       // depth
      },
      1,                                           // mipLevels
      1,                                           // arrayLayers
      VK_SAMPLE_COUNT_1_BIT,                       // samples
      VK_IMAGE_TILING_OPTIMAL,                     // titling
      VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, // usage
      VK_SHARING_MODE_EXCLUSIVE,                   // sharingMode
      1,                                           // queueFamilyIndexCount
      pQueueFamilyIndices,                         // pQueueFamilyIndices
      VK_IMAGE_LAYOUT_UNDEFINED                    // initialLayout
  };
  VK_CHECK(vkCreateImage(m_Device->GetDevice(), &imageCI, nullptr,
                         &m_DepthStencilImage));
  VkMemoryRequirements req;
  vkGetImageMemoryRequirements(m_Device->GetDevice(), m_DepthStencilImage,
                               &req);
  VkMemoryAllocateInfo memoryAllocateInfo = {
      VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // sType
      nullptr,                                // pNext
      req.size,                               // allocationSize
      m_Device->GetMemoryTypeIndex(
          req.memoryTypeBits,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) // memoryTypeIndex
  };
  VK_CHECK(vkAllocateMemory(m_Device->GetDevice(), &memoryAllocateInfo, nullptr,
                            &m_DepthStencilImageMemory));
  VK_CHECK(vkBindImageMemory(m_Device->GetDevice(), m_DepthStencilImage,
                             m_DepthStencilImageMemory, 0));
  VkImageViewCreateInfo imageViewCI = {
      VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, // sType
      nullptr,                                  // pNext
      0,                                        // flags
      m_DepthStencilImage,                      // image
      VK_IMAGE_VIEW_TYPE_2D,                    // viewType
      depthFormat,                              // format
      {
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.r
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.g
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.b
          VK_COMPONENT_SWIZZLE_IDENTITY, // components.a
      },
      {
          VK_IMAGE_ASPECT_DEPTH_BIT |
              VK_IMAGE_ASPECT_STENCIL_BIT, // subresourceRange.aspectMask
          0,                               // subresourceRange.baseMipLevel
          1,                               // subresourceRange.levelCount
          0,                               // subresourceRange.baseArrayLayer
          1,                               // subresourceRange.layerCount
      }};
  VK_CHECK(vkCreateImageView(m_Device->GetDevice(), &imageViewCI, nullptr,
                             &m_DepthStencilImageView));
}

std::vector<VkSemaphore>
VulkanRenderTarget::CreateSubmitsChain(std::vector<VulkanRenderTarget *> &rts) {
  VkPipelineStageFlags waitStages[] = {
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  std::vector<VkSemaphore> waitSemaphores;
  for (auto &rt : rts) {
    if (rt->m_NextRendertargets.size()) {
      VkSemaphore waitSemaphore = rt->m_SeparateSemaphore->GetSemaphore();
      std::vector<VkSemaphore> signalSemaphores;
      std::vector<VkCommandBuffer> cbs;
      for (auto &nextRT : rt->m_NextRendertargets) {
        signalSemaphores.push_back(nextRT->m_SeparateSemaphore->GetSemaphore());
        cbs.push_back(nextRT->m_CommandBuffers[0]->GetCommandBuffer());
      }
      VkSubmitInfo submitInfo = {};
      submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
      submitInfo.pNext = nullptr;
      submitInfo.waitSemaphoreCount = 1;
      submitInfo.pWaitDstStageMask = waitStages;
      submitInfo.pWaitSemaphores = &waitSemaphore;
      submitInfo.signalSemaphoreCount = signalSemaphores.size();
      submitInfo.pSignalSemaphores = signalSemaphores.data();
      submitInfo.commandBufferCount = cbs.size();
      submitInfo.pCommandBuffers = cbs.data();
      VK_CHECK(vkQueueSubmit(rt->m_Queue, 1, &submitInfo, VK_NULL_HANDLE));
      auto returnSemaphores = CreateSubmitsChain(rt->m_NextRendertargets);
      if (returnSemaphores.size())
        for (auto &semaphore : returnSemaphores)
          waitSemaphores.push_back(semaphore);
    } else
      waitSemaphores.push_back(rt->m_SeparateSemaphore->GetSemaphore());
  }
  return waitSemaphores;
}
