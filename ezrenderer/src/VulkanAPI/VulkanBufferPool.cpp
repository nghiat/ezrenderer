//===-- VulkanAPI/VulkanBufferPool.cpp ------------------------------------===//
//===----------------------------------------------------------------------===//
//
// This file implements the VulkanBufferPool class
//
//===----------------------------------------------------------------------===//

#include "VulkanAPI/VulkanBufferPool.h"
#include "Util/PoolTracker1D.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanCommandPool.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanSynchronizationPrimitives.h"
#include <cstring>

using namespace Ez;

VulkanBufferPool::VulkanBufferPool(
    std::shared_ptr<VulkanDevice> device,
    std::shared_ptr<VulkanCommandPool> commandPool)
    : m_Device(std::move(device)), m_CommandPool(commandPool),
      m_UseDeviceMemory(true) {
  // dummy value
  uint64_t stagingAlignemt;
  CreateBuffer(m_Device, DEFAULT_STAGING_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                   VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
               m_StagingBuffer, m_StagingMemory, stagingAlignemt);
  m_CommandBuffer =
      std::make_unique<VulkanCommandBuffer>(m_Device, m_CommandPool);
  m_PoolTracker = std::make_unique<PoolTracker1D>(DEFAULT_BUFFER_SIZE);
  m_Fence = std::make_unique<VulkanFence>(m_Device);
}

VulkanBufferPool::VulkanBufferPool(std::shared_ptr<VulkanDevice> device)
    : m_Device(device) {
  uint64_t stagingAlignemt;
  m_PoolTracker = std::make_unique<PoolTracker1D>(SMALL_BUFFER_SIZE);
}

VulkanBufferPool::~VulkanBufferPool() {
  for (auto &bigBuffer : m_BigBuffers) {
    vkDestroyBuffer(m_Device->GetDevice(), bigBuffer, nullptr);
  }
  for (auto &bigMemory : m_BigMemories) {
    vkFreeMemory(m_Device->GetDevice(), bigMemory, nullptr);
  }
  if (m_StagingBuffer)
    vkDestroyBuffer(m_Device->GetDevice(), m_StagingBuffer, nullptr);
  if (m_StagingMemory)
    vkFreeMemory(m_Device->GetDevice(), m_StagingMemory, nullptr);
}

void VulkanBufferPool::CreateBuffer(const std::shared_ptr<VulkanDevice> &device,
                                    uint64_t size, VkBufferUsageFlags usage,
                                    VkMemoryPropertyFlags properties,
                                    VkBuffer &buffer,
                                    VkDeviceMemory &deviceMemory,
                                    uint64_t &alignment) {

  uint32_t pQueueFamilyIndices[] = {0};
  VkBufferCreateInfo bufferCreateInfo = {
      VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, // sType
      nullptr,                              // pNext
      0,                                    // flags
      size,                                 // size
      usage,                                // usage
      VK_SHARING_MODE_EXCLUSIVE,            // sharingMode
      1,                                    // queueFamilyIndexCount
      pQueueFamilyIndices                   // pQueueFamilyIndices
  };
  VK_CHECK(
      vkCreateBuffer(device->GetDevice(), &bufferCreateInfo, nullptr, &buffer));
  VkMemoryRequirements memoryRequirements;
  vkGetBufferMemoryRequirements(device->GetDevice(), buffer,
                                &memoryRequirements);
  alignment = memoryRequirements.alignment;
  VkMemoryAllocateInfo memoryAllocateInfo = {
      VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, // sType
      nullptr,                                // pNext
      memoryRequirements.size,                // allocationSize
      device->GetMemoryTypeIndex(memoryRequirements.memoryTypeBits,
                                 properties) // memoryTypeIndex
  };
  VK_CHECK(vkAllocateMemory(device->GetDevice(), &memoryAllocateInfo, nullptr,
                            &deviceMemory));
  vkBindBufferMemory(device->GetDevice(), buffer, deviceMemory, 0);
}

void VulkanBufferPool::AllocateSubBuffer(uint64_t size, void *data,
                                         uint64_t &bufferIndex,
                                         uint64_t &subBufferOffset) {
  FindBufferSubRegion(size, bufferIndex, subBufferOffset);
  if (data)
    CopyToDeviceMemory(bufferIndex, subBufferOffset, size, data);
}

void VulkanBufferPool::FindBufferSubRegion(uint64_t size, uint64_t &bufferIndex,
                                           uint64_t &subBufferOffset) {
  auto result =
      m_PoolTracker->AllocateSubRegion(size, bufferIndex, subBufferOffset);
  if (!result) {
    VkBuffer newBuffer;
    VkDeviceMemory newMemory;
    uint64_t alignment;
    if (m_UseDeviceMemory)
      CreateBuffer(
          m_Device, size,
          VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
              VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT |
              VK_BUFFER_USAGE_TRANSFER_DST_BIT,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, newBuffer, newMemory, alignment);
    else
      CreateBuffer(m_Device, SMALL_BUFFER_SIZE,
                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                       VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                   newBuffer, newMemory, alignment);
    m_PoolTracker->SetLastMetadataAlignment(alignment);
    m_BigBuffers.push_back(newBuffer);
    m_BigMemories.push_back(newMemory);
  }
}

void VulkanBufferPool::Deallocate(uint64_t bufferIndex,
                                  uint64_t subBufferOffset,
                                  uint64_t subBufferSize) {
  m_PoolTracker->Deallocate(bufferIndex, subBufferOffset, subBufferSize);
}

void VulkanBufferPool::CopyToDeviceMemory(uint64_t bufferIndex,
                                          uint64_t subBufferOffset,
                                          uint64_t dataSize, void *data) {
  if (m_UseDeviceMemory)
    for (uint64_t start = 0; start < dataSize; start += DEFAULT_STAGING_SIZE) {
      void *mappedData;
      uint64_t copySize = dataSize - start < DEFAULT_STAGING_SIZE
                              ? dataSize - start
                              : DEFAULT_STAGING_SIZE;
      vkMapMemory(m_Device->GetDevice(), m_StagingMemory, start, copySize, 0,
                  &mappedData);
      std::memcpy(mappedData, data, copySize);
      vkUnmapMemory(m_Device->GetDevice(), m_StagingMemory);

      m_CommandBuffer->Begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
      VkBufferCopy bufferCopy = {
          0,                       // srcOffset
          subBufferOffset + start, // dstOffset
          copySize                 // size
      };
      vkCmdCopyBuffer(m_CommandBuffer->GetCommandBuffer(), m_StagingBuffer,
                      m_BigBuffers[bufferIndex], 1, &bufferCopy);
      m_CommandBuffer->End();
      // TODO reuse
      VkCommandBuffer pCommandBuffers[] = {m_CommandBuffer->GetCommandBuffer()};
      VkSubmitInfo submitInfo = {
          VK_STRUCTURE_TYPE_SUBMIT_INFO, // sType
          nullptr,                       // pNext
          0,                             // waitSemaphoreCount
          nullptr,                       // pWaitSemaphores
          nullptr,                       // pWaitSemaphores
          1,                             // commandBufferCount
          pCommandBuffers,               // pCommandBuffers
          0,                             // signalSemaphoreCount
          nullptr                        // pSignalSemaphores
      };
      VK_CHECK(vkQueueSubmit(m_Device->GetDeviceQueue(0, 0), 1, &submitInfo,
                             m_Fence->GetFence()));
      m_Fence->Wait();
      m_Fence->Reset();
      m_CommandBuffer->Reset();
    }
  else {
    void *mappedData;
    vkMapMemory(m_Device->GetDevice(), m_BigMemories[bufferIndex],
                subBufferOffset, dataSize, 0, &mappedData);
    std::memcpy(mappedData, data, dataSize);
    vkUnmapMemory(m_Device->GetDevice(), m_BigMemories[bufferIndex]);
  }
}

VkBuffer VulkanBufferPool::GetBuffer(uint64_t bufferIndex) {
  return m_BigBuffers[bufferIndex];
}
