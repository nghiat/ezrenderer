#include "VulkanAPI/VulkanGraphicsPipeline.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanDescriptorSet.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanRenderTarget.h"
#include "VulkanAPI/VulkanShader.h"
#include <cassert>

using namespace Ez;

VulkanGraphicsPipeline::VulkanGraphicsPipeline(
    std::shared_ptr<VulkanDevice> device)
    : m_Device(std::move(device)) {}

VulkanGraphicsPipeline::~VulkanGraphicsPipeline() {
  vkDeviceWaitIdle(m_Device->GetDevice());
  if (m_PipelineLayout)
    vkDestroyPipelineLayout(m_Device->GetDevice(), m_PipelineLayout, nullptr);
  if (m_PipelineCache)
    vkDestroyPipelineCache(m_Device->GetDevice(), m_PipelineCache, nullptr);
  if (m_GraphicsPipeline)
    vkDestroyPipeline(m_Device->GetDevice(), m_GraphicsPipeline, nullptr);
  for (auto &framebuffer : m_Framebuffers)
    vkDestroyFramebuffer(m_Device->GetDevice(), framebuffer, nullptr);
}

// We merge all the functions into one big function to prevent memory problems.
// Problem 1: all create info structures will be deleted
//           after exiting the function scope
// Problem 2: some create info structures need pointers to other local
// structures that will disappear after exiting their functions
void VulkanGraphicsPipeline::CreateGraphicsPipeline(
    VkPrimitiveTopology topology,
    std::unique_ptr<VulkanRenderTarget> &renderTarget, bool enableBlend) {
  CreatePipelineCache();
  CreatePipelineLayout();
  VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo =
      {
          VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, // sType
          nullptr,                                                     // pNext
          0,                                                           // flags,
          topology, // topology
          VK_FALSE, // primitiveRestartEnable
      };

  std::vector<VkPipelineShaderStageCreateInfo> stages;
  for (auto &shader : m_Shaders) {
    stages.push_back(shader->GetPipelineShaderStateCreateInfo());
  }
  assert(m_Offsets.size() == m_Formats.size());
  auto count = static_cast<uint32_t>(m_Offsets.size());
  std::vector<VkVertexInputBindingDescription> vertexInputBindingDescriptions(
      m_Strides.size());
  for (uint32_t i = 0; i < m_Strides.size(); i++)
    vertexInputBindingDescriptions[i] = {
        i,                          // binding
        m_Strides[i],               // stride
        VK_VERTEX_INPUT_RATE_VERTEX // inputRate
    };
  std::vector<VkVertexInputAttributeDescription>
      vertexInputAttributeDescriptions(count);
  for (uint32_t i = 0; i < count; i++) {
    vertexInputAttributeDescriptions[i] = {
        i,             // location, default locations are from 0 to count - 1
        m_Bindings[i], // binding, default binding is 0
        m_Formats[i], m_Offsets[i]};
  }
  VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, // sType
      nullptr,                                                   // pNext
      0,                                                         // flags
      (uint32_t)vertexInputBindingDescriptions
          .size(),                           // vertexBindingDescriptionCount
      vertexInputBindingDescriptions.data(), // pVertexBindingDescriptions
      count,                                 // vertexAttributeDescriptionCount
      vertexInputAttributeDescriptions.data(), // pVertexAttributeDescriptions
  };
  VkViewport viewport = {
      0.f,                                           // x
      0.f,                                           // y
      static_cast<float>(renderTarget->GetWidth()),  // width
      static_cast<float>(renderTarget->GetHeight()), // height
      0.f,                                           // minDepth
      1.f,                                           // maxDepth
  };

  VkRect2D scissor = {{
                          0, // offset.x
                          0, // offset.y
                      },
                      {
                          renderTarget->GetWidth(),  // extent.width
                          renderTarget->GetHeight(), // extent.height
                      }};

  VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, // sType
      nullptr,                                               // pNext
      0,                                                     // flags,
      1,                                                     // viewportCount
      &viewport,                                             // pViewports
      1,                                                     // scissorCount
      &scissor                                               // pScissors
  };
  VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo =
      {
          VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, // sType
          nullptr,                                                    // pNext
          0,                                                          // flags
          VK_FALSE,                        // depthClampEnable
          VK_FALSE,                        // rasterizerDiscardEnable
          VK_POLYGON_MODE_FILL,            // pologonMode
          VK_CULL_MODE_NONE,               // cullMode
          VK_FRONT_FACE_COUNTER_CLOCKWISE, // frontFace
          VK_FALSE,                        // depthBiasEnable
          0.f,                             // depthBiasConstantFactor
          0.f,                             // depthBiasClamp
          0.f,                             // depthBiasSlopeFactor
          1.f,                             // lineWidth
      };
  VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, // sType
      nullptr,                                                  // pNext
      0,                                                        // flags
      VK_SAMPLE_COUNT_1_BIT, // rasterizationSamples
      VK_FALSE,              // sampleShadingEnable
      1.f,                   // minSampleShading
      nullptr,               // pSampleMask
      VK_FALSE,              // alphaToCoverageEnable
      VK_FALSE,              // alphaToOneEnable
  };

  VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO, // sType
      nullptr,                                                    // pNext
      0,                                                          // flags
      VK_TRUE,                     // depthTestEnable
      VK_TRUE,                     // depthWriteEnable
      VK_COMPARE_OP_LESS_OR_EQUAL, // depthCompareOp
      VK_FALSE,                    // depthBoundsTestEnable
      VK_FALSE,                    // stencilTestEnable
  };
  pipelineDepthStencilStateCreateInfo.minDepthBounds = 0.f;
  pipelineDepthStencilStateCreateInfo.maxDepthBounds = 1.f;

  VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState = {
      (VkBool32)(enableBlend ? VK_TRUE : VK_FALSE),       // blendEnable
      VK_BLEND_FACTOR_SRC_ALPHA,                          // srcColorBlendFactor
      VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,                // desColorBlendFace
      VK_BLEND_OP_ADD,                                    // colorBlendOp
      VK_BLEND_FACTOR_ONE,                                // srcAlphaBlendFactor
      VK_BLEND_FACTOR_ZERO,                               // dstAlphaBlendFactor
      VK_BLEND_OP_ADD,                                    // alphaBlendOp
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT // colorWriteMask
          |
          VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT};

  VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO, // sType
      nullptr,                                                  // pNext
      0,                                                        // flags
      VK_FALSE,                                                 // logicOpEnable
      VK_LOGIC_OP_COPY,                                         // logicOp
      1,
      &pipelineColorBlendAttachmentState,

  };
  std::vector<VkDynamicState> dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT};
  VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO, // sType
      nullptr,                                              // pNext
      0,                                                    // flags
      static_cast<uint32_t>(dynamicStates.size()),          // dynamicStateCount
      dynamicStates.data()                                  // pDynamicStates
  };

  VkGraphicsPipelineCreateInfo pipelineCreateInfo = {
      VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO, // sType
      nullptr,                                         // pNext
      0,                                               // flags
      static_cast<uint32_t>(stages.size()),            // stageCount
      stages.data(),                                   // pStages
      &pipelineVertexInputStateCreateInfo,             // pVertexInputState
      &pipelineInputAssemblyStateCreateInfo,           // pInputAssemblyState
      nullptr,                                         // pTessellationState
      &pipelineViewportStateCreateInfo,                // pViewportState
      &pipelineRasterizationStateCreateInfo,           // pRasterizationState
      &pipelineMultisampleStateCreateInfo,             // pMultisampleState
      &pipelineDepthStencilStateCreateInfo,            // pDepthStencilState
      &pipelineColorBlendStateCreateInfo,              // pColorBlendState
      &pipelineDynamicStateCreateInfo,                 // pDynamicState,
      m_PipelineLayout,                                // layout
      renderTarget->GetRenderPass(),                   // renderPass
      0,                                               // subpass
      VK_NULL_HANDLE,                                  // basePipelineHandle
      -1                                               // basePipelineIndex
  };
  VK_CHECK(vkCreateGraphicsPipelines(m_Device->GetDevice(), m_PipelineCache, 1,
                                     &pipelineCreateInfo, nullptr,
                                     &m_GraphicsPipeline));
}
void VulkanGraphicsPipeline::CreatePipelineCache() {
  VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO, // sType
      nullptr,                                      // pNext
      0,                                            // flags
      0,                                            // initialDataSize
      nullptr                                       // pInitialData
  };

  VK_CHECK(vkCreatePipelineCache(m_Device->GetDevice(),
                                 &pipelineCacheCreateInfo, nullptr,
                                 &m_PipelineCache));
}

void VulkanGraphicsPipeline::AddDescriptorSet(
    std::shared_ptr<VulkanDescriptorSet> descriptorSet) {
  m_DescriptorSets.push_back(std::move(descriptorSet));
}

void VulkanGraphicsPipeline::ClearDescriptorSets() { m_DescriptorSets.clear(); }

void VulkanGraphicsPipeline::BindPipeline(VkCommandBuffer commandBuffer) {
  vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    m_GraphicsPipeline);
}
VkPipeline VulkanGraphicsPipeline::GetGraphicsPipeline() const {
  return m_GraphicsPipeline;
}
VkPipelineLayout VulkanGraphicsPipeline::GetGraphicsPipelineLayout() const {
  return m_PipelineLayout;
}
void VulkanGraphicsPipeline::CreatePipelineLayout() {
  std::vector<VkDescriptorSetLayout> layouts;
  for (auto &set : m_DescriptorSets)
    layouts.push_back(set->GetDescriptorSetLayout());
  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO, // sType
      nullptr,                                       // pNext
      0,                                             // flags
      static_cast<uint32_t>(layouts.size()),         // setLayoutCount
      layouts.data(),                                // pSetLayouts
      (uint32_t)m_PushConstantRanges.size(),         // pushConstantRangeCount
      m_PushConstantRanges.data()                    // pPushConstantRanges
  };
  VK_CHECK(vkCreatePipelineLayout(m_Device->GetDevice(),
                                  &pipelineLayoutCreateInfo, nullptr,
                                  &m_PipelineLayout));
}
void VulkanGraphicsPipeline::AddDescriptorSet(
    std::vector<std::shared_ptr<VulkanDescriptorSet>> &descriptorSets) {
  for (auto &set : descriptorSets)
    m_DescriptorSets.push_back(set);
}
void VulkanGraphicsPipeline::AddShader(std::shared_ptr<VulkanShader> shader) {
  m_Shaders.push_back(std::move(shader));
}

void VulkanGraphicsPipeline::AddShaders(
    std::vector<std::shared_ptr<VulkanShader>> &shaders) {
  for (auto &shader : shaders) {
    m_Shaders.push_back(shader);
  }
}

void VulkanGraphicsPipeline::ClearShaders() { m_Shaders.clear(); }

void VulkanGraphicsPipeline::SetupInputState(std::vector<uint32_t> &strides,
                                             std::vector<uint32_t> &bindings,
                                             std::vector<uint32_t> &offsets,
                                             std::vector<VkFormat> &formats) {
  m_Strides = strides;
  m_Bindings = bindings;
  m_Offsets = offsets;
  m_Formats = formats;
}

void VulkanGraphicsPipeline::AddPushConstants(VkShaderStageFlags stage,
                                              uint32_t offset, uint32_t size) {
  m_PushConstantRanges.push_back({stage, offset, size});
}
