//===-- VulkanAPI/VulkanBuffer.cpp ----------------------------------------===//
//===----------------------------------------------------------------------===//
//
// This file implements the VulkanBuffer class
//
//===----------------------------------------------------------------------===//

#include "VulkanAPI/VulkanBuffer.h"
#include "VulkanAPI/VulkanBufferPool.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanHelper.h"
#include <cassert>

using namespace Ez;

VulkanBuffer::VulkanBuffer(std::shared_ptr<VulkanBufferPool> bufferPool,
                           uint64_t dataSize, void *data)
    : m_BufferPool(std::move(bufferPool)), m_SubBufferSize(dataSize) {
  m_BufferPool->AllocateSubBuffer(dataSize, data, m_BufferIndex,
                                  m_SubBufferOffset);
}

VulkanBuffer::~VulkanBuffer() {
  m_BufferPool->Deallocate(m_BufferIndex, m_SubBufferOffset, m_SubBufferSize);
}

VkPipelineVertexInputStateCreateInfo
VulkanBuffer::GetVertexInputState(uint32_t stride,
                                  std::vector<uint32_t> &pOffsets,
                                  std::vector<VkFormat> &pFormats) {
  assert(pOffsets.size() == pFormats.size());
  auto count = static_cast<uint32_t>(pOffsets.size());
  m_VertexInputBindingDescription = {
      0,                          // binding
      stride,                     // stride
      VK_VERTEX_INPUT_RATE_VERTEX // inputRate
  };
  m_VertexInputAttributeDescriptions.resize(count);
  for (uint32_t i = 0; i < count; i++) {
    m_VertexInputAttributeDescriptions[i] = {
        i, // location, default locations are from 0 to count - 1
        0, // binding, default binding is 0
        pFormats[i], pOffsets[i]};
  }
  VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo = {
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, // sType
      nullptr,                                                   // pNext
      0,                                                         // flags
      1,                                // vertexBindingDescriptionCount
      &m_VertexInputBindingDescription, // pVertexBindingDescriptions
      count,                            // vertexAttributeDescriptionCount
      m_VertexInputAttributeDescriptions.data(), // pVertexAttributeDescriptions
  };
  return pipelineVertexInputStateCreateInfo;
}

VkBuffer VulkanBuffer::GetBuffer() {
  return m_BufferPool->GetBuffer(m_BufferIndex);
}

uint64_t VulkanBuffer::GetMemoryOffset() { return m_SubBufferOffset; }

uint64_t VulkanBuffer::GetMemorySize() { return m_SubBufferSize; }

void VulkanBuffer::UpdateData(uint64_t offset, uint64_t dataSize, void *data) {
  m_BufferPool->CopyToDeviceMemory(m_BufferIndex, m_SubBufferOffset + offset,
                                   dataSize, data);
}
