#include "VulkanAPI/VulkanSwapchain.h"
#include "VulkanAPI/VulkanCommandBuffer.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanInstance.h"
#if !(defined(ANDROID) || defined(__ANDROID_API__))
#include <GLFW/glfw3.h>
#else
#include <android_native_app_glue.h>
#endif
#include <algorithm>
#include <memory>
using namespace Ez;

VulkanSwapchain::VulkanSwapchain(std::shared_ptr<VulkanInstance> instance,
                                 std::shared_ptr<VulkanDevice> device,
                                 uint32_t imageCount, void *window)
    : m_Instance(std::move(instance)), m_Device(std::move(device)),
      m_ImageCount(imageCount) {

  CreateWindowSurface(window);
  FindSurfaceInfo();
  FindPresentMode();
  CreateSwapChain();
  CreateSwapchainImageViews();
}

VulkanSwapchain::~VulkanSwapchain() {
  for (auto &imageView : m_SwapchainImageViews) {
    vkDestroyImageView(m_Device->GetDevice(), imageView, nullptr);
  }
  vkDestroySwapchainKHR(m_Device->GetDevice(), m_Swapchain, nullptr);
  vkDestroySurfaceKHR(m_Instance->GetInstance(), m_Surface, nullptr);
}

void VulkanSwapchain::CreateWindowSurface(void *window) {
#if defined(ANDROID) || defined(__ANDROID_API__)
  VkAndroidSurfaceCreateInfoKHR androidSurfaceCreateInfoKHR = {
      VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR, // sType
      nullptr,                                           // pNext
      0,                                                 // flags
      (ANativeWindow *)window,                           // window
  };
  VK_CHECK(vkCreateAndroidSurfaceKHR(m_Instance->GetInstance(),
                                     &androidSurfaceCreateInfoKHR, nullptr,
                                     &m_Surface));
  m_Width =
      static_cast<uint32_t>(ANativeWindow_getWidth((ANativeWindow *)window));
  m_Height =
      static_cast<uint32_t>(ANativeWindow_getHeight((ANativeWindow *)window));
#else
  glfwCreateWindowSurface(m_Instance->GetInstance(), (GLFWwindow *)window,
                          nullptr, &m_Surface);
  int width, height;
  glfwGetFramebufferSize((GLFWwindow *)window, &width, &height);
  m_Width = width;
  m_Height = height;
#endif
}

void VulkanSwapchain::FindSurfaceInfo() {
  uint32_t formatCount;
  vkGetPhysicalDeviceSurfaceFormatsKHR(m_Device->GetPhysicalDevice(), m_Surface,
                                       &formatCount, nullptr);
  std::vector<VkSurfaceFormatKHR> vkSurfaceFormats(formatCount);
  vkGetPhysicalDeviceSurfaceFormatsKHR(m_Device->GetPhysicalDevice(), m_Surface,
                                       &formatCount, &vkSurfaceFormats[0]);
  if (formatCount == 1 && vkSurfaceFormats[0].format == VK_FORMAT_UNDEFINED)
    m_Format = VK_FORMAT_B8G8R8A8_UNORM;
  else
    m_Format = vkSurfaceFormats[0].format;
  m_ColorSpace = vkSurfaceFormats[0].colorSpace;
  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_Device->GetPhysicalDevice(),
                                            m_Surface, &m_SurfaceCapabilities);
  VkBool32 surfaceSupport;
  vkGetPhysicalDeviceSurfaceSupportKHR(m_Device->GetPhysicalDevice(), 0,
                                       m_Surface, &surfaceSupport);
}
void VulkanSwapchain::FindPresentMode() {
  uint32_t presentModeCount = 0;
  vkGetPhysicalDeviceSurfacePresentModesKHR(
      m_Device->GetPhysicalDevice(), m_Surface, &presentModeCount, nullptr);
  std::vector<VkPresentModeKHR> presentModes(presentModeCount);
  vkGetPhysicalDeviceSurfacePresentModesKHR(m_Device->GetPhysicalDevice(),
                                            m_Surface, &presentModeCount,
                                            &presentModes[0]);
  if (std::find(presentModes.begin(), presentModes.end(),
                VK_PRESENT_MODE_FIFO_KHR) != presentModes.end())
    m_PresentMode = VK_PRESENT_MODE_FIFO_KHR;
  else
    m_PresentMode = presentModes[0];
}
void VulkanSwapchain::CreateSwapChain() {
  uint32_t queueFamily = 0;
  auto compositeAlpha = GetSupportedAlphaFlagBits();
  VkSwapchainCreateInfoKHR swapchainCreateInfo = {
      VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR, // sType
      nullptr,                                     // pNext
      0,                                           // flags
      m_Surface,                                   // surface
      m_ImageCount,                                // minImageCount
      m_Format,                                    // imageFormat
      m_ColorSpace,                                // imageColorSpace
      m_SurfaceCapabilities.currentExtent,         // imageExtent
      1,                                           // imageArrayLayers
      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,         // imageUsage
      VK_SHARING_MODE_EXCLUSIVE,                   // imageSharingMode
      1,                                           // queueFamilyIndexCount
      &queueFamily,                                // pQueueFamilyIndices
      VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,       // preTransform
      compositeAlpha,                              // compositeAlpha
      m_PresentMode,                               // presentMode
      VK_FALSE,                                    // clipped
      VK_NULL_HANDLE,                              // oldSwapchain
  };
  VK_CHECK(vkCreateSwapchainKHR(m_Device->GetDevice(), &swapchainCreateInfo,
                                nullptr, &m_Swapchain));
}
void VulkanSwapchain::CreateSwapchainImageViews() {
  uint32_t swapchainImageCount;
  vkGetSwapchainImagesKHR(m_Device->GetDevice(), m_Swapchain,
                          &swapchainImageCount, nullptr);
  m_SwapchainImages.resize(swapchainImageCount);
  VK_CHECK(vkGetSwapchainImagesKHR(m_Device->GetDevice(), m_Swapchain,
                                   &swapchainImageCount,
                                   &m_SwapchainImages[0]));
  m_SwapchainImageViews.resize(swapchainImageCount);
  for (uint32_t i = 0; i < swapchainImageCount; i++) {
    VkImageViewCreateInfo imageViewCreateInfo = {
        VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, // sType
        nullptr,                                  // pNext
        0,                                        // flags
        m_SwapchainImages[i],                     // image
        VK_IMAGE_VIEW_TYPE_2D,                    // viewType
        m_Format,                                 // format
        {
            VK_COMPONENT_SWIZZLE_IDENTITY, // components.r
            VK_COMPONENT_SWIZZLE_IDENTITY, // components.g
            VK_COMPONENT_SWIZZLE_IDENTITY, // components.b
            VK_COMPONENT_SWIZZLE_IDENTITY, // components.a
        },
        {
            VK_IMAGE_ASPECT_COLOR_BIT, // subresourceRange.aspectMask
            0,                         // subresourceRange.baseMipLevel
            1,                         // subresourceRange.levelCount
            0,                         // subresourceRange.baseArrayLayer
            1,                         // subresourceRange.layerCount
        }};
    VK_CHECK(vkCreateImageView(m_Device->GetDevice(), &imageViewCreateInfo,
                               nullptr, &m_SwapchainImageViews[i]));
  }
}

VkFormat VulkanSwapchain::GetSurfaceFormat() const { return m_Format; }

VkImageView VulkanSwapchain::GetImageView(uint32_t index) {
  return m_SwapchainImageViews[index];
}

uint32_t VulkanSwapchain::GetImageCount() const { return m_ImageCount; }
VkSurfaceCapabilitiesKHR VulkanSwapchain::GetSurfaceCapabilities() const {
  return m_SurfaceCapabilities;
}

uint32_t VulkanSwapchain::GetWidth() const { return m_Width; }

uint32_t VulkanSwapchain::GetHeight() const { return m_Height; }

VkSwapchainKHR VulkanSwapchain::GetSwapchain() const { return m_Swapchain; }

VkCompositeAlphaFlagBitsKHR VulkanSwapchain::GetSupportedAlphaFlagBits() {
  auto supportedCompositeAlpha = m_SurfaceCapabilities.supportedCompositeAlpha;
  if (supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
    return VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  if (supportedCompositeAlpha & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR)
    return VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR;
  if (supportedCompositeAlpha & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR)
    return VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR;
  if (supportedCompositeAlpha & VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR)
    return VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
}