#include "VulkanAPI/VulkanShader.h"
#include "Util/File.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"

namespace Ez {
VulkanShader::VulkanShader(std::shared_ptr<VulkanDevice> device,
                           VkShaderStageFlagBits shaderStageFlagBits,
                           const char *pathToFile)
    : m_Device(std::move(device)), m_ShaderStageFlagBits(shaderStageFlagBits) {
  auto spirv = File::ReadFile(pathToFile, true);
  CreateShaderModule(spirv);
}

VulkanShader::~VulkanShader() {
  vkDestroyShaderModule(m_Device->GetDevice(), m_Shadermodule, nullptr);
}

void VulkanShader::CreateShaderModule(std::vector<char> shader) {
  VkShaderModuleCreateInfo shaderModuleCreateInfo = {
      VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, // sType
      nullptr,                                     // pNext
      0,                                           // flags
      shader.size(),                               // codeSize
      (uint32_t *)shader.data(),                   // pCode
  };
  VK_CHECK(vkCreateShaderModule(m_Device->GetDevice(), &shaderModuleCreateInfo,
                                nullptr, &m_Shadermodule));
}
VkPipelineShaderStageCreateInfo
VulkanShader::GetPipelineShaderStateCreateInfo() {
  VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo{
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, // sType
      nullptr,                                             // pNext
      0,                                                   // flags
      m_ShaderStageFlagBits,                               // stage
      m_Shadermodule,                                      // module
      "main",                                              // pName
      nullptr, // pSpecializationInfo
  };
  return pipelineShaderStageCreateInfo;
}
}
