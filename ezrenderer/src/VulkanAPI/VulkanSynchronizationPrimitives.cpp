#include "VulkanAPI/VulkanSynchronizationPrimitives.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include <cassert>

using namespace Ez;

VulkanFence::VulkanFence(std::shared_ptr<VulkanDevice> device)
    : m_Device(std::move(device)) {
  VkFenceCreateInfo fenceCreateInfo = {
      VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, // sType
      nullptr,                             // pNext
      0,                                   // flags
  };
  VK_CHECK(vkCreateFence(m_Device->GetDevice(), &fenceCreateInfo, nullptr,
                         &m_Fence));
}

VulkanFence::~VulkanFence() {
  vkDestroyFence(m_Device->GetDevice(), m_Fence, nullptr);
}

VkFence VulkanFence::GetFence() const { return m_Fence; }

void VulkanFence::Reset() { vkResetFences(m_Device->GetDevice(), 1, &m_Fence); }

void VulkanFence::Wait() {
  vkWaitForFences(m_Device->GetDevice(), 1, &m_Fence, VK_TRUE, UINT64_MAX);
}

void VulkanFence::WaitFences(
    std::vector<std::unique_ptr<VulkanFence>> &fences) {
  auto fencesSize = fences.size();
  assert(fencesSize > 0);
  std::vector<VkFence> vkFences;
  for (auto &fence : fences)
    vkFences.push_back(fence->GetFence());
  vkWaitForFences(fences[0]->m_Device->GetDevice(), fencesSize, vkFences.data(),
                  VK_TRUE, UINT64_MAX);
}

VulkanSemaphore::VulkanSemaphore(std::shared_ptr<VulkanDevice> device)
    : m_Device(std::move(device)) {
  VkSemaphoreCreateInfo semaphoreCreateInfo = {
      VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, // sType
      nullptr,                                 // pNext
      0,                                       // flags
  };
  VK_CHECK(vkCreateSemaphore(m_Device->GetDevice(), &semaphoreCreateInfo,
                             nullptr, &m_Semaphore));
}

VulkanSemaphore::~VulkanSemaphore() {
  vkDestroySemaphore(m_Device->GetDevice(), m_Semaphore, nullptr);
}

VkSemaphore VulkanSemaphore::GetSemaphore() { return m_Semaphore; }
