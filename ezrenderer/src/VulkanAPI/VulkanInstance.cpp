#include "VulkanAPI/VulkanInstance.h"
#include "VulkanAPI/VulkanHelper.h"
#include <Util/Log.h>
#if !(defined(ANDROID) || defined(__ANDROID_API__))
#include <GLFW/glfw3.h>
#endif
#include <vector>

using namespace Ez;

VulkanInstance::VulkanInstance(bool enableValidation) {
  uint32_t platformExtCount;
  const char **ppPlatformExtNames;

  GetPlatformExtensions(platformExtCount, ppPlatformExtNames);

  std::vector<const char *> exts(ppPlatformExtNames,
                                 ppPlatformExtNames + platformExtCount);
  exts.push_back("VK_EXT_debug_report");
  std::vector<const char *> layerNames;
  if (enableValidation)
#if !(defined(ANDROID) || defined(__ANDROID_API__))
    layerNames =
        std::vector<const char *>{"VK_LAYER_LUNARG_standard_validation"};
#else
    layerNames = std::vector<const char *>{
        "VK_LAYER_GOOGLE_threading",
        "VK_LAYER_LUNARG_parameter_validation",
        "VK_LAYER_LUNARG_object_tracker",
        "VK_LAYER_LUNARG_core_validation",
        "VK_LAYER_LUNARG_swapchain",
        "VK_LAYER_GOOGLE_unique_objects",
    };
#endif
  VkApplicationInfo appInfo = {
      VK_STRUCTURE_TYPE_APPLICATION_INFO, // sType
      nullptr,                            // pNext
      "EzRenderer Vulkan",                // pApplicationName
      VK_MAKE_VERSION(0, 0, 1),           // applicationVersion
      "EzRenderer",                       // pEngineName
      VK_MAKE_VERSION(0, 0, 1),           // engineVersion
      VK_MAKE_VERSION(1, 0, 0)            // apiVersion
  };

  VkInstanceCreateInfo instanceCreateInfo = {
      VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,   // sType
      nullptr,                                  // pNext
      0,                                        // flags
      &appInfo,                                 // pApplicationInfo
      static_cast<uint32_t>(layerNames.size()), // enabledLayerCount
      layerNames.data(),                        // ppEnabledLayerNames
      static_cast<uint32_t>(exts.size()),       // enabledExtensionCount
      exts.data()                               // ppEnabledExtensionNames
  };
  VK_CHECK(vkCreateInstance(&instanceCreateInfo, nullptr, &m_Instance));
  SetDebugCallback(DebugReportCallback);
}

VulkanInstance::~VulkanInstance() {
  vkDestroyDebugReportCallbackEXT(m_Instance, m_DebugReportCallback, nullptr);
  vkDestroyInstance(m_Instance, nullptr);
}

void VulkanInstance::SetDebugCallback(
    PFN_vkDebugReportCallbackEXT debugReportCallback) {

  vkCreateDebugReportCallbackEXT =
      (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(
          m_Instance, "vkCreateDebugReportCallbackEXT");
  vkDestroyDebugReportCallbackEXT =
      (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(
          m_Instance, "vkDestroyDebugReportCallbackEXT");

  // Create the debug callback with desired settings
  VkDebugReportCallbackCreateInfoEXT debugReportCallbackCreateInfo{
      VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,                 // sType
      nullptr,                                                        // pNext
      VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT // flags
          |
          VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT,
      debugReportCallback, // pfnCallback
      nullptr,             // pUserData
  };

  vkCreateDebugReportCallbackEXT(m_Instance, &debugReportCallbackCreateInfo,
                                 nullptr, &m_DebugReportCallback);
}

VKAPI_ATTR VkBool32 VKAPI_CALL VulkanInstance::DebugReportCallback(
    VkDebugReportFlagsEXT msgFlags, VkDebugReportObjectTypeEXT objType,
    uint64_t srcObject, size_t location, int32_t msgCode,
    const char *pLayerPrefix, const char *pMsg, void *pUserData) {
  if (msgFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
    LOGE("VulkanInstance", "[%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  } else if (msgFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
    LOGW("VulkanInstance", "[%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  } else if (msgFlags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) {
    LOGW("VulkanInstance", "PERFORMANCE [%s] Code %d : %s", pLayerPrefix,
         msgCode, pMsg);
  } else if (msgFlags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) {
    LOGI("VulkanInstance", "[%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  } else if (msgFlags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) {
    LOGD("VulkanInstance", "[%s] Code %d : %s", pLayerPrefix, msgCode, pMsg);
  }

  // Returning false tells the layer not to stop when the event occurs, so
  // they see the same behavior with and without validation layers enabled.
  return VK_FALSE;
}

void VulkanInstance::GetPlatformExtensions(uint32_t &extCount,
                                           const char **&ppExtNames) {
#if defined(__ANDROID_API__) || defined(ANDROID)
  extCount = 3;
  ppExtNames = new const char *[extCount];
  ppExtNames[0] = "VK_KHR_surface";
  ppExtNames[1] = "VK_KHR_android_surface";
  ppExtNames[2] = "VK_EXT_debug_report";
#else
  ppExtNames = glfwGetRequiredInstanceExtensions(&extCount);
#endif
}
