#include "VulkanAPI/VulkanTexture.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"
#include "VulkanAPI/VulkanImagePool.h"
#include <Util/Texture.h>

using namespace Ez;

VulkanTexture::VulkanTexture(std::shared_ptr<VulkanDevice> device,
                             std::shared_ptr<VulkanImagePool> imagePool,
                             Texture *texture)
    : m_Device(std::move(device)), m_ImagePool(std::move(imagePool)) {
  m_Width = texture->GetX();
  m_Height = texture->GetY();
  ChooseFormat(texture->GetChannel());
  m_ImagePool->AllocateSubImage(
      m_Format, m_Width, m_Height, texture->GetChannel(),
      (void *)texture->GetData(), m_ImageIndex, m_OffsetX, m_OffsetY);
  uint32_t rightX = m_OffsetX + m_Width;
  uint32_t topY = m_OffsetY + m_Height;
  uint32_t imageWidth = m_ImagePool->GetImageWidth(m_Format, m_ImageIndex);
  uint32_t imageHeight = m_ImagePool->GetImageHeight(m_Format, m_ImageIndex);
  m_UV.resize(4);
  m_UV[0] = m_OffsetX * 1.f / imageWidth;
  m_UV[1] = m_OffsetY * 1.f / imageHeight;
  m_UV[2] = rightX * 1.f / imageHeight;
  m_UV[3] = topY * 1.f / imageHeight;
  m_Image = m_ImagePool->GetImage(m_Format, m_ImageIndex);
  CreateSampler();
  CreateImageView();
}

VulkanTexture::~VulkanTexture() {
  if (m_ImagePool)
    m_ImagePool->Deallocate(m_Format, m_ImageIndex, m_OffsetX, m_OffsetY,
                            m_Width, m_Height);
  vkDestroySampler(m_Device->GetDevice(), m_Sampler, nullptr);
  vkDestroyImageView(m_Device->GetDevice(), m_ImageView, nullptr);
}

void VulkanTexture::CreateSampler() {
  VkSamplerCreateInfo samplerCreateInfo = {
      VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO, // sType
      nullptr,                               // pNext
      0,                                     // flags
      VK_FILTER_LINEAR,                      // magFilter
      VK_FILTER_LINEAR,                      // minFilter
      VK_SAMPLER_MIPMAP_MODE_LINEAR,         // mipmapMode
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeU
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeV
      VK_SAMPLER_ADDRESS_MODE_REPEAT,        // addressModeW
      0.f,                                   // mipLodBias
      VK_FALSE,                              // anisotropyEnable
      1.f,                                   // maxAnisotropy
      VK_FALSE,                              // compareEnable
      VK_COMPARE_OP_ALWAYS,                  // compareOp
      0.f,                                   // minLod
      0.f,                                   // maxLod
      VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,    // borderColor
      VK_FALSE                               // unnomalizedCoordinates
  };
  if (m_Device->GetFeatures().samplerAnisotropy) {
    samplerCreateInfo.maxAnisotropy =
        m_Device->GetProperties().limits.maxSamplerAnisotropy;
    samplerCreateInfo.anisotropyEnable = VK_TRUE;
  }
  VK_CHECK(vkCreateSampler(m_Device->GetDevice(), &samplerCreateInfo, nullptr,
                           &m_Sampler));
}

void VulkanTexture::CreateImageView(VkImageViewType viewType) {
  VkImageViewCreateInfo imageViewCreateInfo = {
      VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, // sType
      nullptr,                                  // pNext
      0,                                        // flags
      m_Image,                                  // image
      viewType,                                 // viewType
      m_Format,                                 // format
      {
          // components
          VK_COMPONENT_SWIZZLE_R, // r
          VK_COMPONENT_SWIZZLE_G, // g
          VK_COMPONENT_SWIZZLE_B, // b
          VK_COMPONENT_SWIZZLE_A  // a
      },
      {
          // subresourceRange
          VK_IMAGE_ASPECT_COLOR_BIT, // aspectMask
          0,                         // baseMipLevel
          1,                         // levelCount
          0,                         // baseArrayLayer
          viewType == VK_IMAGE_VIEW_TYPE_CUBE ? (uint32_t)6
                                              : (uint32_t)1 // layerCount
      }};
  VK_CHECK(vkCreateImageView(m_Device->GetDevice(), &imageViewCreateInfo,
                             nullptr, &m_ImageView));
}

void VulkanTexture::ChooseFormat(int channels) {
  if (channels == 4)
    m_Format = VK_FORMAT_R8G8B8A8_UNORM;
  else if (channels == 1)
    m_Format = VK_FORMAT_R8_UNORM;
  // else if (channels == 4)
  // m_Format = VK_FORMAT_R8G8B8A8_UNORM;
  // else if (channels == 2)
  // m_Format = VK_FORMAT_R8G8B8A8_UNORM;
  // m_Format = VK_FORMAT_R8_UNORM;
}

VkSampler VulkanTexture::GetSampler() { return m_Sampler; }

VkImageView VulkanTexture::GetImageView() { return m_ImageView; }

std::vector<float> VulkanTexture::GetUV() { return m_UV; }
