#include "VulkanAPI/VulkanCommandPool.h"
#include "VulkanAPI/VulkanDevice.h"
#include "VulkanAPI/VulkanHelper.h"

namespace Ez {
VulkanCommandPool::VulkanCommandPool(std::shared_ptr<VulkanDevice> device,
                                     VkCommandPoolCreateFlags flags)
    : m_Device(std::move(device)), m_Flags(flags) {
  VkCommandPoolCreateInfo commandPoolCreateInfo{
      VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, // sType
      nullptr,                                    // pNext
      flags,                                      // flags
      0                                           // queueFamilyIndex
  };
  VK_CHECK(vkCreateCommandPool(m_Device->GetDevice(), &commandPoolCreateInfo,
                               nullptr, &m_CommnadPool));
}

VulkanCommandPool::~VulkanCommandPool() {
  vkDestroyCommandPool(m_Device->GetDevice(), m_CommnadPool, nullptr);
}

void VulkanCommandPool::ResetCommadPool() {
  if (m_Flags | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT)
    vkResetCommandPool(m_Device->GetDevice(), m_CommnadPool,
                       VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
}
}
