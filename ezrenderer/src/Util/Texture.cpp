#include "Util/Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "Util/File.h"
#include <Util/Log.h>
#include <stb/stb_image.h>

using namespace Ez;

Texture::Texture(const char *path) : m_UseSTB(true) {
  auto buffer = File::ReadFile(path, true);
  if (!stbi_is_hdr_from_memory((unsigned char *)buffer.data(), buffer.size()))
    m_pData =
        stbi_load_from_memory((unsigned char *)buffer.data(), buffer.size(),
                              &m_X, &m_Y, &m_Channel, STBI_rgb_alpha);
  else
    m_pfData =
        stbi_loadf_from_memory((unsigned char *)buffer.data(), buffer.size(),
                               &m_X, &m_Y, &m_Channel, STBI_rgb_alpha);
  // Channels num hack
  m_Channel = 4;
}

Texture::Texture(int width, int height, int channels, unsigned char *data)
    : m_X(width), m_Y(height), m_Channel(channels), m_pData(data),
      m_UseSTB(false) {}

Texture::~Texture() {
  if (m_UseSTB)
    if (m_pData)
      stbi_image_free(m_pData);
    else
      stbi_image_free(m_pfData);
}

int Texture::GetX() { return m_X; }

int Texture::GetY() { return m_Y; }

uint8_t Texture::GetChannel() { return m_Channel; }

unsigned char *Texture::GetData() { return m_pData; }

float *Texture::GetFloatData() { return m_pfData; }
