#include "Util/File.h"
#include "Util/Log.h"
#if defined(ANDROID) || defined(__ANDROID_API__)
#include "Util/JNIHelper.h"
#endif
#include <fstream>

using namespace Ez;

std::vector<char> File::ReadFile(const char *filePath, bool asBinary) {
#ifdef __ANDROID_API__
  return JNIHelper::Get()->ReadAssetFile(filePath, asBinary);
#else
  std::ifstream file(std::string("assets/") + std::string(filePath),
                     std::ios::ate | std::ios::binary);
  if (!file.is_open()) {
    LOGE("File", "%s %d: Failed to open file %s", __FILE__, __LINE__, filePath);
    throw std::runtime_error("Failed to open file " + std::string(filePath));
  }
  size_t fileSize = (size_t)file.tellg();
  if (!asBinary)
    fileSize++;
  std::vector<char> buffer(fileSize);
  file.seekg(0);
  file.read(buffer.data(), fileSize);
  file.close();
  if (!asBinary)
    // Add null character if the file is read as text file
    buffer.push_back('\0');
  return buffer;
#endif
}