#include "Util/JNIHelper.h"
#include "Util/Log.h"
#include <stdexcept>

JNIHelper *JNIHelper::_JNIHelper = nullptr;

JNIHelper::JNIHelper() {}

void JNIHelper::Init(android_app *app) {
  m_AndroidApp = app;
  m_AssetManager = m_AndroidApp->activity->assetManager;
}

void JNIHelper::SetAssetManager(AAssetManager *manager) {
  m_AssetManager = manager;
}

std::vector<char> JNIHelper::ReadAssetFile(const char *path, bool asBinary) {
  std::vector<char> buffer;
  AAsset *assetFile =
      AAssetManager_open(m_AssetManager, path, AASSET_MODE_BUFFER);
  if (!assetFile) {
    LOGE("JNIHelper", "%s %d: Failed to open file %s", __FILE__, __LINE__,
         path);
    throw std::runtime_error("Failed to open " + std::string(path));
  }
  uint8_t *data = (uint8_t *)AAsset_getBuffer(assetFile);
  auto size = AAsset_getLength(assetFile);
  if (data == NULL) {
    AAsset_close(assetFile);
    LOGE("JNIHelper", "%s %d: Failed to load file %s", __FILE__, __LINE__,
         path);
    throw std::runtime_error("Failed to load " + std::string(path));
  }
  buffer.resize(asBinary ? size : size + 1);
  buffer.assign(data, data + size);
  if (!asBinary)
    // Add null character if the file is read as text file
    buffer.push_back('\0');

  AAsset_close(assetFile);
  return buffer;
}

JNIHelper *JNIHelper::Get() {
  if (!_JNIHelper)
    _JNIHelper = new JNIHelper();
  return _JNIHelper;
}
