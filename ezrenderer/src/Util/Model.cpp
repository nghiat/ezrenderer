#include "Util/Model.h"
#include <Util/File.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glm/gtc/type_ptr.hpp>

using namespace Ez;

Model::Model(const char *path) {
  Assimp::Importer importer;
  auto buffer = File::ReadFile(path);
  auto scene = importer.ReadFileFromMemory(
      buffer.data(), buffer.size(), aiProcessPreset_TargetRealtime_MaxQuality);

  if (scene)
    LoadSceneMetadata(scene);
}

Model::~Model() {}

void Model::LoadSceneMetadata(const aiScene *scene) {
  m_IndicesOffset.resize(scene->mNumMeshes);
  m_IndicesLists.resize(scene->mNumMeshes);
  assert(scene->mNumMeshes > 0);
  for (int i = 0; i < scene->mNumMeshes; i++) {
    if (i == 0)
      m_IndicesOffset[i] = 0;
    else
      m_IndicesOffset[i] = m_NumVertices;
    m_NumVertices += scene->mMeshes[i]->mNumVertices;
  }
  m_Vertices.resize(m_NumVertices);
  for (int i = 0; i < scene->mNumMeshes; i++) {
    auto mesh = scene->mMeshes[i];
    for (int j = 0; j < mesh->mNumVertices; j++) {
      Vertex vertex = {};
      if (mesh->mColors[0])
        vertex.color = glm::make_vec4(&mesh->mColors[0][j].r);
      if (mesh->mNormals)
        vertex.normal = glm::make_vec3(&mesh->mNormals[j].x);
      vertex.pos = glm::make_vec3(&mesh->mVertices[j].x);
      m_Vertices[m_IndicesOffset[i] + j] = vertex;
    }

    for (int j = 0; j < 3; j++)
      m_IndicesLists[i][j] = std::vector<uint32_t>();
    for (int j = 0; j < mesh->mNumFaces; j++) {
      auto face = mesh->mFaces[j];
      switch (face.mNumIndices) {
      case 1:
        m_IndicesLists[i][0].push_back(face.mIndices[0] + m_IndicesOffset[i]);
        break;
      case 2:
        m_IndicesLists[i][1].push_back(face.mIndices[0] + m_IndicesOffset[i]);
        m_IndicesLists[i][1].push_back(face.mIndices[1] + m_IndicesOffset[i]);
        break;
      case 3:
        m_IndicesLists[i][2].push_back(face.mIndices[0] + m_IndicesOffset[i]);
        m_IndicesLists[i][2].push_back(face.mIndices[1] + m_IndicesOffset[i]);
        m_IndicesLists[i][2].push_back(face.mIndices[2] + m_IndicesOffset[i]);
        break;
        // We don't support any mode other than that
      }
    }
  }
  glm::mat4 identityMatrix = glm::mat4(1.f);
  SetupNode(scene->mRootNode, identityMatrix);
}

void Model::SetupNode(const aiNode *node, glm::mat4 &parentMatrix) {
  Node newNode;
  newNode.modelMatrix =
      glm::make_mat4(&node->mTransformation.a1) * parentMatrix;
  if (node->mNumMeshes) {
    for (int i = 0; i < node->mNumMeshes; i++) {
      newNode.indicesListIndices.push_back(node->mMeshes[i]);
    }
    m_NodeLists.push_back(newNode);
  }
  for (int i = 0; i < node->mNumChildren; i++)
    SetupNode(node->mChildren[i], newNode.modelMatrix);
}

std::vector<Vertex> Model::GetVertices() const { return m_Vertices; }

std::vector<std::array<std::vector<uint32_t>, 3>>
Model::GetIndiciesLists() const {
  return m_IndicesLists;
}
