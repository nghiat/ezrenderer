#include <Util/TextureCube.h>
#include <cstring>

using namespace Ez;

TextureCube::TextureCube(const char *path) : Texture(path) { ProcessTexture(); }

TextureCube::~TextureCube() {
  for (int i = 0; i < 6; i++)
    delete m_FacesData[i];
  delete m_FacesData;
}

float **TextureCube::GetFacesData() const { return m_FacesData; }

void TextureCube::ProcessTexture() {
  // Size of each pixel
  int elementSize = m_Channel * sizeof(float);

  auto data = m_pfData;
  auto texWidth = m_X;
  auto texHeight = m_Y;
  auto faceWidth = texWidth / 3;
  // faceHeight and faceWidth are the same but we use 2 vars to differentiate
  // direction
  // when copying data
  auto faceHeight = faceWidth;

  // Number of elements of each row of data
  auto rowNumElements = m_Channel * texWidth;

  m_FacesData = new float *[6];
  auto faceNumElements = faceWidth * faceWidth * m_Channel;
  for (int i = 0; i < 6; i++)
    m_FacesData[i] = new float[faceNumElements];

  auto faceRowSize = faceWidth * elementSize;
  auto faceRowNumElements = faceWidth * m_Channel;

  // We copy each row of each face to the faceData[i] pointer
  for (int i = 0; i < faceWidth; i++) {
    //    for (int j = 0; j < faceRowNumElements; j++) {
    //      m_FacesData[0][i * faceRowNumElements + j] = half(*(data +
    //      rowNumElements * (faceHeight + i) + 2 * faceRowNumElements + j));
    //      m_FacesData[1][i * faceRowNumElements + j] = half(*(data +
    //      rowNumElements * (faceHeight + i) + j));
    //      m_FacesData[2][i * faceRowNumElements + j] = half(*(data +
    //      faceRowNumElements + i * rowNumElements + j));
    //      m_FacesData[3][i * faceRowNumElements + j] = half(*(data +
    //      faceRowNumElements + (i + 2 * faceHeight) * rowNumElements + j));
    //      m_FacesData[4][i * faceRowNumElements + j] = half(*(data +
    //      faceRowNumElements + (i + faceHeight) * rowNumElements + j));
    //    }
    //    auto dataRow =
    //      faceRowNumElements + (4 * faceWidth - 1 - i) * rowNumElements;
    //    for (int j = 0; j < faceWidth; j++) {
    //      auto dataColumn = (faceWidth - 1 - j) * m_Channel;
    //      m_FacesData[5][i * faceRowNumElements + j * m_Channel] = half(*(data
    //      + dataRow + dataColumn));
    //      m_FacesData[5][i * faceRowNumElements + j * m_Channel + 1] =
    //      half(*(data + dataRow + dataColumn + 1));
    //      m_FacesData[5][i * faceRowNumElements + j * m_Channel + 2] =
    //      half(*(data + dataRow + dataColumn + 2));
    //      m_FacesData[5][i * faceRowNumElements + j * m_Channel + 3] =
    //      half(*(data + dataRow + dataColumn + 3));
    //    }
    // X+
    std::memcpy(m_FacesData[0] + i * faceRowNumElements,
                data + rowNumElements * (faceHeight + i) +
                    2 * faceRowNumElements,
                faceRowSize);
    // X-
    std::memcpy(m_FacesData[1] + i * faceRowNumElements,
                data + rowNumElements * (faceHeight + i), faceRowSize);
    // Y+
    std::memcpy(m_FacesData[2] + i * faceRowNumElements,
                data + faceRowNumElements + i * rowNumElements, faceRowSize);
    // Y-
    std::memcpy(m_FacesData[3] + i * faceRowNumElements,
                data + faceRowNumElements +
                    (i + 2 * faceHeight) * rowNumElements,
                faceRowSize);
    // Z+
    std::memcpy(m_FacesData[4] + i * faceRowNumElements,
                data + faceRowNumElements + (i + faceHeight) * rowNumElements,
                faceRowSize);
    // Z-, this one need to flip vertically and horizontally
    // so we have to copy each ELEMENT (usually 4bytes) manually
    auto dataRow =
        faceRowNumElements + (4 * faceWidth - 1 - i) * rowNumElements;
    for (int j = 0; j < faceWidth; j++) {
      auto dataColumn = (faceWidth - 1 - j) * m_Channel;
      std::memcpy(m_FacesData[5] + i * faceRowNumElements + j * m_Channel,
                  data + dataRow + dataColumn, elementSize);
    }
  }
  m_X /= 3;
  m_Y /= 4;
}
