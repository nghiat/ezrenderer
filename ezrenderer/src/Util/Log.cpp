#include "Util/Log.h"
#include <spdlog/spdlog.h>

using namespace Ez;

Log::Log(std::string tag) {
#if !(defined(ANDROID) || defined(__ANDROID_API__))
  m_Slog = spdlog::stdout_color_mt(std::string("spdlog/") + std::string(tag));
#else
  m_Slog = spdlog::android_logger(std::string("spdlog/") + std::string(tag));
#endif
}

Log::~Log() {}

template <typename... Args>
void Log::trace(const char *fmt, const Args &... args) {
  m_Slog->trace(fmt, args...);
}

template <typename... Args>
void Log::debug(const char *fmt, const Args &... args) {
  m_Slog->debug(fmt, args...);
}

template <typename... Args>
void Log::info(const char *fmt, const Args &... args) {
  m_Slog->info(fmt, args...);
}

template <typename... Args>
void Log::warn(const char *fmt, const Args &... args) {
  m_Slog->warn(fmt, args...);
}

template <typename... Args>
void Log::error(const char *fmt, const Args &... args) {
  m_Slog->error(fmt, args...);
}

template <typename... Args>
void Log::critical(const char *fmt, const Args &... args) {
  m_Slog->critical(fmt, args...);
}
