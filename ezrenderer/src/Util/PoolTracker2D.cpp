#include "Util/PoolTracker2D.h"

using namespace Ez;

PoolTracker2D::PoolTracker2D(uint32_t defaultWidth, uint32_t defaultHeight)
    : m_DefaultWidth(defaultWidth), m_DefaultHeight(defaultHeight) {}

PoolTracker2D::~PoolTracker2D() {}
bool PoolTracker2D::AllocateSubRegion(uint64_t &index, uint32_t &offsetX,
                                      uint32_t &offsetY, uint32_t &width,
                                      uint32_t &height) {
  // Check if a free region that can contains this new region
  bool foundRegion = false;
  // Infos of the smallest region possible
  uint64_t minSize = UINT64_MAX;
  // Index of metadata that contains the min region
  uint64_t metadataIndex;
  // Index of the min region
  uint64_t regionIndex;

  for (size_t i = 0; i < m_Metadata.size(); i++) {
    auto alignment = m_Metadata[i].alignment;
    for (size_t j = 0; j < m_Metadata[i].freeRegions.size(); j++) {
      // TODO alignment
      auto &region = m_Metadata[i].freeRegions[j];
      if (region.size < minSize && width <= region.width &&
          height <= region.height) {
        foundRegion = true;
        minSize = region.size;
        metadataIndex = i;
        regionIndex = j;
      }
    }
  }

  if (foundRegion) {
    auto &freeRegions = m_Metadata[metadataIndex].freeRegions;
    auto region = freeRegions[regionIndex];
    index = metadataIndex;
    offsetX = region.offsetX;
    offsetY = region.offsetY;
    Region2D newRegion{width * height, region.offsetX, region.offsetY, width,
                       height};
    freeRegions.erase(freeRegions.begin() + regionIndex);
    SplitRegions(freeRegions, region, newRegion);
    return true;
  }
  Metadata2D newMetadata;
  uint32_t newWidth = width > m_DefaultWidth ? width : m_DefaultWidth;
  uint32_t newHeight = height > m_DefaultHeight ? height : m_DefaultHeight;
  newMetadata.freeRegions = std::vector<Region2D>(0);
  Region2D bigRegion{newWidth * newHeight, 0, 0, newWidth, newHeight};
  Region2D smallRegion{width * height, 0, 0, width, height};
  SplitRegions(newMetadata.freeRegions, bigRegion, smallRegion);
  m_Metadata.push_back(newMetadata);
  width = newWidth;
  height = newHeight;
  index = static_cast<uint64_t>(m_Metadata.size() - 1);
  offsetX = 0;
  offsetY = 0;
  return false;
}

bool PoolTracker2D::SetLastMetadataAlignment(uint64_t alignment) {
  if (m_Metadata.size()) {
    auto &last = m_Metadata.back();
    last.alignment = alignment;
    return true;
  }
  return false;
}

void PoolTracker2D::Deallocate(uint64_t index, uint32_t offsetX,
                               uint32_t offsetY, uint32_t width,
                               uint32_t height) {
  auto &regions = m_Metadata[index].freeRegions;
  Region2D newRegion = {width * height, offsetX, offsetY, width, height};
  bool merged = false;
  for (size_t i = 0; i < regions.size(); i++) {
    auto region = m_Metadata[index].freeRegions[i];
    if (newRegion.width == region.width &&
        (region.offsetY + region.height == newRegion.offsetY ||
         newRegion.offsetY + newRegion.height == region.offsetY)) {
      merged = true;
      newRegion.height += region.height;
      newRegion.offsetY = newRegion.offsetY < region.offsetY ? newRegion.offsetY
                                                             : region.offsetY;
      regions.erase(regions.begin() + i);
      Deallocate(index, newRegion.offsetX, newRegion.offsetY, newRegion.width,
                 newRegion.height);
    } else if (newRegion.height == region.height &&
               (region.offsetX + region.width == newRegion.offsetX ||
                newRegion.offsetX + newRegion.width == region.offsetX)) {
      merged = true;
      newRegion.width += region.width;
      newRegion.offsetX = newRegion.offsetX < region.offsetX ? newRegion.offsetX
                                                             : region.offsetX;
      regions.erase(regions.begin() + i);
      Deallocate(index, newRegion.offsetX, newRegion.offsetY, newRegion.width,
                 newRegion.height);
    }
  }
  if (!merged) {
    newRegion.size = newRegion.width * newRegion.height;
    regions.push_back(newRegion);
  }
}

void PoolTracker2D::SplitRegions(std::vector<Region2D> &regions,
                                 Region2D bigRegion, Region2D smallRegion) {
  // if the new region is smaller than the found region, we have to add the
  // new free regions to the vector
  if (bigRegion.width > smallRegion.width ||
      bigRegion.height > smallRegion.height) {
    Region2D newRegions[2];
    if (bigRegion.width * smallRegion.height <
        bigRegion.height * smallRegion.width) {
      newRegions[0] = {
          bigRegion.width * (bigRegion.height - smallRegion.height),
          bigRegion.offsetX, bigRegion.offsetY + smallRegion.height,
          bigRegion.width, bigRegion.height - smallRegion.height};
      newRegions[1] = {(bigRegion.width - smallRegion.width) *
                           smallRegion.height,
                       bigRegion.offsetX + smallRegion.width, bigRegion.offsetY,
                       bigRegion.width - smallRegion.width, smallRegion.height};
    } else {
      newRegions[0] = {(bigRegion.width - smallRegion.width) * bigRegion.height,
                       bigRegion.offsetX + smallRegion.width, bigRegion.offsetY,
                       bigRegion.width - smallRegion.width, bigRegion.height};
      newRegions[1] = {
          smallRegion.width * (bigRegion.height - smallRegion.height),
          bigRegion.offsetX, bigRegion.offsetY + smallRegion.height,
          smallRegion.width, bigRegion.height - smallRegion.height};
    }
    for (int i = 0; i < 2; i++) {
      if (newRegions[i].width != 0 && newRegions[i].height != 0)
        regions.push_back(newRegions[i]);
    }
  }
}
