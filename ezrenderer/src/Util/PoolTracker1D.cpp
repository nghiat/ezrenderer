#include "Util/PoolTracker1D.h"
#include <algorithm>

using namespace Ez;

PoolTracker1D::PoolTracker1D(uint64_t defaultSize)
    : m_DefaultSize(defaultSize) {}

PoolTracker1D::~PoolTracker1D() {}

bool PoolTracker1D::AllocateSubRegion(uint64_t &size, uint64_t &index,
                                      uint64_t &offset) {
  // Check if the new region collided with another acquired region
  bool foundNewRegion = false;
  // Infos of the smallest region possible
  uint64_t minFreeSize = UINT64_MAX;
  uint64_t minFreeIndex;
  uint64_t minFreeOffset;

  for (size_t i = 0; i < m_Metadata.size(); i++) {
    auto alignment = m_Metadata[i].alignment;
    for (size_t j = 0; j < m_Metadata[i].acquriedRegions.size() - 1; j++) {
      auto region = m_Metadata[i].acquriedRegions[j];
      // We need to round up the size of the region to align with the memory
      if (alignment != 1 && region.second % alignment)
        region.second =
            (static_cast<uint64_t>(region.second * 1.f / alignment) + 1) *
            alignment;
      auto nextRegion = m_Metadata[i].acquriedRegions[j + 1];
      uint64_t freeSizeBetween =
          nextRegion.first - region.first - region.second;
      if (freeSizeBetween < minFreeSize && freeSizeBetween >= size) {
        foundNewRegion = true;
        minFreeSize = freeSizeBetween;
        minFreeIndex = i;
        minFreeOffset = region.first + region.second;
      }
    }
  }
  if (foundNewRegion) {
    index = minFreeIndex;
    offset = minFreeOffset;
    auto &metadata = m_Metadata[index];
    metadata.acquriedRegions.push_back(std::make_pair(offset, size));
    // We have to use stable sort because the first and the last element of a
    // metadata.acquiredRegions are (0, 0) and (totalSize, 0) which may not
    // preserve position with the second or the pre-last element
    std::stable_sort(metadata.acquriedRegions.begin(),
                     metadata.acquriedRegions.end(),
                     [](const std::pair<uint64_t, uint64_t> &p1,
                        const std::pair<uint64_t, uint64_t> &p2) {
                       return p1.first < p2.first;
                     });
    return true;
  }
  // If we can't find any region, we have to create new metadata
  Metadata1D newMetadata;
  uint64_t totalSize = size > m_DefaultSize ? size : m_DefaultSize;
  newMetadata.acquriedRegions.push_back(std::make_pair(0, 0));
  newMetadata.acquriedRegions.push_back(std::make_pair(0, size));
  newMetadata.acquriedRegions.push_back(std::make_pair(totalSize, 0));
  m_Metadata.push_back(newMetadata);
  index = static_cast<uint32_t>(m_Metadata.size() - 1);
  offset = 0;
  // Pass back the totalSize value to help allocate new "something"
  size = totalSize;
  return false;
}

bool PoolTracker1D::SetLastMetadataAlignment(uint64_t alignment) {
  if (m_Metadata.size()) {
    auto &last = m_Metadata.back();
    last.alignment = alignment;
    return true;
  }
  return false;
}

void PoolTracker1D::Deallocate(uint64_t index, uint64_t offset, uint64_t size) {
  auto &regions = m_Metadata[index].acquriedRegions;
  auto it =
      std::find(regions.begin(), regions.end(), std::make_pair(offset, size));
  if (it != regions.end())
    m_Metadata[index].acquriedRegions.erase(it);
}