﻿#include "Text/Font.h"
#include <hb-ft.h>

using namespace Ez;

slog FontLib::_Logger = NEWLOG("FontLib");

FontLib::FontLib() {
  auto error = FT_Init_FreeType(&m_Library);
  if (error) {
    _Logger->error("Can't not init freetype library (Code {})", error);
    throw "Can't not init freetype";
  }
}

FontLib::~FontLib() { FT_Done_FreeType(m_Library); }

FT_Library FontLib::GetFTLibrary() { return m_Library; }

slog Font::_Logger = NEWLOG("Font");

Font::Font(std::shared_ptr<FontLib> ftLib, const char *fontPath, uint32_t size,
           uint32_t dpi)
    : m_FTLib(std::move(ftLib)) {
  auto error = FT_New_Face(m_FTLib->GetFTLibrary(), fontPath, 0, &m_Face);
  if (error) {
    _Logger->error("Can't not create new face (Code {})", error);
    throw "Can't not create a new freetype face";
  }
  FT_Set_Char_Size(m_Face, size, size, dpi, dpi);
  m_HbFont = hb_ft_font_create_referenced(m_Face);
}

Font::~Font() { FT_Done_Face(m_Face); }

FT_Face Font::GetFace() { return m_Face; }

void Font::GetText(std::string s, std::string language, hb_script_t script,
                   hb_direction_t direction) {
  auto hbBuffer = hb_buffer_create();
  hb_buffer_set_language(
      hbBuffer, hb_language_from_string(language.c_str(), language.length()));
  hb_buffer_set_script(hbBuffer, script);
  hb_buffer_set_direction(hbBuffer, direction);
  hb_buffer_add_utf8(hbBuffer, s.c_str(), -1, 0, -1);
  hb_buffer_guess_segment_properties(hbBuffer);
  hb_shape(m_HbFont, hbBuffer, nullptr, 0);
  auto len = hb_buffer_get_length(hbBuffer);
  hb_glyph_info_t *info = hb_buffer_get_glyph_infos(hbBuffer, NULL);
  hb_glyph_position_t *pos = hb_buffer_get_glyph_positions(hbBuffer, NULL);
  for (auto i = 0; i < len; i++) {
  }
}
