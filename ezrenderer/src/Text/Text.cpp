﻿#include "Text/Text.h"
#include <Util/File.h>
#include <Util/Log.h>
#include <cassert>
#include <hb-ft.h>

using namespace Ez;

FT_Library Font::_FTLib = nullptr;

Font::Font(const char *fontPath, uint32_t size, uint32_t dpi) {
  // Init FT_Library if it is nullptr
  if (!_FTLib) {
    auto error = FT_Init_FreeType(&_FTLib);
    if (error) {
      LOGE("Text", "Can't not init freetype library (Code %d)", error);
      throw "Can't not init freetype";
    }
  }

  m_FileBuffer = File::ReadFile(fontPath, true);
  // Init new FT_Face
  auto error = FT_New_Memory_Face(
      _FTLib, reinterpret_cast<FT_Byte *>(m_FileBuffer.data()),
      m_FileBuffer.size(), 0, &m_FTFace);
  // auto error = FT_New_Face(_FTLib, fontPath, 0, &m_FTFace);
  if (error) {
    LOGE("Text", "Can't not create new face (Code %d)", error);
    throw "Can't not create a new freetype face";
  }
  FT_Set_Char_Size(m_FTFace, size, size, dpi, dpi);

  m_Height = m_FTFace->size->metrics.height / 64.f;
  // Init harfbuzz
  m_HBFont = hb_ft_font_create_referenced(m_FTFace);
  m_HBBuffer = hb_buffer_create();
}

Font::~Font() {
  hb_buffer_destroy(m_HBBuffer);
  hb_font_destroy(m_HBFont);
  FT_Done_Face(m_FTFace);
}

std::vector<Glyph> Font::GetGlyphs(std::string text, float x, float y,
                                   float maxWidth, hb_direction_t direction,
                                   hb_script_t script, const char *language) {
  std::vector<Glyph> result;
  hb_buffer_reset(m_HBBuffer);
  hb_buffer_set_direction(m_HBBuffer, direction);
  hb_buffer_set_script(m_HBBuffer, script);
  hb_buffer_set_language(m_HBBuffer,
                         hb_language_from_string(language, strlen(language)));
  hb_buffer_add_utf8(m_HBBuffer, text.c_str(), text.size(), 0, text.size());
  hb_shape(m_HBFont, m_HBBuffer, nullptr, 0);

  auto len = hb_buffer_get_length(m_HBBuffer);
  hb_glyph_info_t *info = hb_buffer_get_glyph_infos(m_HBBuffer, nullptr);
  hb_glyph_position_t *pos = hb_buffer_get_glyph_positions(m_HBBuffer, nullptr);

  for (unsigned int i = 0; i < len; i++) {
    uint32_t codepoint = info[i].codepoint;
    auto it = m_CachedGlyph.find(codepoint);
    if (it != m_CachedGlyph.end()) {
      result.push_back(it->second);
    } else {
      auto error = FT_Load_Glyph(m_FTFace, codepoint, FT_LOAD_RENDER);
      if (error) {
        LOGE("Text", "Can't not create glyph with codepoint %d (Error code %d)",
             codepoint, error);
        assert(error);
      }
      auto width = m_FTFace->glyph->bitmap.width;
      auto height = m_FTFace->glyph->bitmap.rows;
      auto buffer = std::vector<unsigned char>(width * height);
      memcpy(buffer.data(), m_FTFace->glyph->bitmap.buffer, width * height);
      Glyph newGlyph = {
          codepoint,                                             // codepoint
          info[i].cluster,                                       // cluster
          width,                                                 // width
          height,                                                // height
          pos[i].x_offset / 64.f + m_FTFace->glyph->bitmap_left, // xOffset
          pos[i].y_offset / 64.f + m_FTFace->glyph->bitmap_top,  // yOffset
          pos[i].x_advance / 64.f,                               // xAdvance
          pos[i].y_advance / 64.f,                               // yAdvance
          buffer                                                 // buffer
      };
      // Debug space
      if (!newGlyph.height) {
        newGlyph.width = newGlyph.xAdvance;
        newGlyph.height = 2;
      }
      m_CachedGlyph[codepoint] = newGlyph;
      result.push_back(newGlyph);
    }
  }

  float curX = x;
  float curY = y;
  uint32_t lastSpaceIndex = 0;
  for (int i = 0; i < len; i++) {
    Glyph &glyph = result[i];
    if (i && text[info[i].cluster] != ' ' &&
        curX + glyph.xAdvance > x + maxWidth) {
      curX = x;
      curY += m_Height;
    }
    glyph.xOffset += curX;
    glyph.yOffset = curY - glyph.yOffset;
    curX += glyph.xAdvance;
    curY += glyph.yAdvance;
    if (text[info[i].cluster] == ' ') {
      float wordWidth = 0;
      for (int j = i + 1; j < len; j++) {
        if (text[info[j].cluster] == ' ')
          break;
        wordWidth += result[j].xAdvance;
      }
      if (curX + wordWidth + glyph.xAdvance > x + maxWidth) {
        curX = x;
        curY += m_Height;
      }
    }
  }
  return result;
}