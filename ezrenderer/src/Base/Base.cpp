//===-- Window/Window.cpp - Implement the Window class --------------------===//
//===----------------------------------------------------------------------===//
//
// This file implements the Base class
//
//===----------------------------------------------------------------------===//

#include "Base/Base.h"
#include <chrono>
#include <iostream>

using namespace Ez;
#ifndef __ANDROID_API__
void SetupGLFWCallbacks() {
  auto mouseButtonCallback = [](GLFWwindow *window, int button, int action,
                                int mods) {
    base->MouseButtonCallback(static_cast<Base::Mouse>(button),
                              static_cast<Base::Action>(action));
  };

  auto cursorPosCallback = [](GLFWwindow *window, double xPos, double yPos) {
    base->PointerPosCallback(xPos, yPos);
  };

  auto scrollCallback = [](GLFWwindow *window, double xOffset, double yOffset) {
    base->ScrollCallback(xOffset, yOffset);
  };

  auto resizeCallback = [](GLFWwindow *window, int width, int height) {
    base->ResizeCallback(width, height);
  };

  glfwSetMouseButtonCallback(base->GetGLFWwindow(), mouseButtonCallback);
  glfwSetCursorPosCallback(base->GetGLFWwindow(), cursorPosCallback);
  glfwSetScrollCallback(base->GetGLFWwindow(), scrollCallback);
  glfwSetWindowSizeCallback(base->GetGLFWwindow(), resizeCallback);
}
#else
void Base::TouchCallback(Base::Action action) {}
#endif

Base::Base(Renderer renderer, int width, int height, std::string name)
    : m_Renderer(renderer), m_Width(width), m_Height(height), m_Name(name) {
#ifndef __ANDROID_API__
  CreateGLFWWindow();
  m_Window = m_GLFWWindow.get();
#else
#endif
}

Base::~Base() {}

void Base::Init() {
  m_StartTime = std::chrono::high_resolution_clock::now();
#ifndef __ANDROID_API__
  SetupGLFWCallbacks();
#else
#endif
}
#ifndef __ANDROID_API__
void Base::GLFWErrorCallback(int error, const char *description) {}

void Base::CreateGLFWWindow() {
  glfwSetErrorCallback(GLFWErrorCallback);
  if (!glfwInit()) {
    throw new std::runtime_error("Failed to initialize GLFW");
  }
  if (m_Renderer == Renderer::GL)
    SetupGLFWWindowGL();
  else if (m_Renderer == Renderer::VK)
    SetupGLFWWindowVK();
  if (!m_GLFWWindow) {
    throw new std::runtime_error("Failed to create GLFW window");
  }
  //  _windows.push_back(this);
}

void Base::SetupGLFWWindowGL() {
  for (int openGLVerIndex = m_OpenGLVersionListSize - 1; openGLVerIndex >= 0;
       openGLVerIndex--) {
    // Try to use the highest OpenGL version
    if (m_GLFWWindow) {
      m_OpenGLVersionMajor = m_OpenGLVersionList[2 * (openGLVerIndex + 1)];
      m_OpenGLVersionMinor = m_OpenGLVersionList[2 * (openGLVerIndex + 1) + 1];
      glfwMakeContextCurrent(m_GLFWWindow.get());
      if (gl3wInit()) {
        throw new std::runtime_error("Failed to init gl3w");
      }
      break;
    }
    int versionMajor = m_OpenGLVersionList[2 * openGLVerIndex];
    int versionMinor = m_OpenGLVersionList[2 * openGLVerIndex + 1];
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, versionMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, versionMinor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    m_GLFWWindow = std::shared_ptr<GLFWwindow>(
        glfwCreateWindow(m_Width, m_Height, m_Name.c_str(), nullptr, nullptr),
        [](GLFWwindow *w) { glfwDestroyWindow(w); });
  }
}

void Base::SetupGLFWWindowVK() {
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
  m_GLFWWindow = std::shared_ptr<GLFWwindow>(
      glfwCreateWindow(m_Width, m_Height, m_Name.c_str(), nullptr, nullptr),
      [](GLFWwindow *w) { glfwDestroyWindow(w); });
}

void Base::MouseButtonCallback(Mouse button, Action action) {}

void Base::Loop() {
  while (!glfwWindowShouldClose(m_GLFWWindow.get())) {
    m_CurrentTime = std::chrono::high_resolution_clock::now();
    Update();
    Draw();
    m_LastTime = m_CurrentTime;
    glfwSwapBuffers(m_GLFWWindow.get());
    glfwPollEvents();
  }
}
GLFWwindow *Base::GetGLFWwindow() { return m_GLFWWindow.get(); }
#endif

void Base::ResizeCallback(int width, int height) {}

void Base::PointerPosCallback(double xPos, double yPos) {}

void Base::ScrollCallback(double xOffset, double yOffset) {}