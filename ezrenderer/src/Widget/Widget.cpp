//===-- Widget/Widget.h - Widget class definition ---------------*- C++ -*-===//
//===----------------------------------------------------------------------===//
//
//  This file implements the Widget class
//
//===----------------------------------------------------------------------===//

#include "Widget/Widget.h"

using Ez::Widget;

Widget::Widget() : Widget(0, 0, 0, 0) {}

Widget::Widget(int topLeftX, int topLeftY, int width, int height)
    : m_TopLeftX(topLeftX), m_TopLeftY(m_TopLeftY), m_Width(width),
      m_Height(height) {
  _Widgets.push_back(this);
}

int Widget::GetHeight() const { return m_Height; }

void Widget::SetHeight(int Height) { m_Height = Height; }

int Widget::GetWidth() const { return m_Width; }

void Widget::SetWidth(int Width) { m_Width = Width; }

int Widget::GetTopLeftY() const { return m_TopLeftY; }

void Widget::SetTopLeftY(int TopLeftY) { m_TopLeftY = TopLeftY; }

int Widget::GetTopLeftX() const { return m_TopLeftX; }

void Widget::SetTopLeftX(int TopLeftX) { m_TopLeftX = TopLeftX; }

bool Ez::Widget::IsInside(int x, int y) {
  if (x >= m_TopLeftX && x <= m_TopLeftX + m_Width && y >= m_TopLeftY &&
      y <= m_TopLeftY + m_Height)
    return true;
  return false;
}
