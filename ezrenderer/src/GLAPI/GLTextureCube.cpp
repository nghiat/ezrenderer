#include "GLAPI/GLTextureCube.h"
#include "Util/Log.h"
#include "Util/TextureCube.h"
#include <cstring>
#include <vector>

using namespace Ez;

GLTextureCube::GLTextureCube(TextureCube *texture) {
  ChooseFormat(texture->GetChannel());
  glGenTextures(1, &m_Texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_Texture);

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  for (int i = 0; i < 6; i++) {
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA16F,
                 texture->GetX(), texture->GetY(), 0, m_Format, GL_FLOAT,
                 texture->GetFacesData()[i]);
  }
  glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
}

GLTextureCube::~GLTextureCube() {}

GLuint GLTextureCube::GetTexture() const { return m_Texture; }

void GLTextureCube::ChooseFormat(int channels) {
  if (channels == 4)
    m_Format = GL_RGBA;
  if (channels == 1)
    m_Format = GL_RED;
}