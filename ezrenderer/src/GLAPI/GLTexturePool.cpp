#include "GLAPI/GLTexturePool.h"

using namespace Ez;

GLTexturePool::GLTexturePool() {}

GLTexturePool::~GLTexturePool() {
  for (auto &textureType : m_BigTexturesMap) {
    for (auto &texture : textureType.second.textures)
      glDeleteTextures(1, &texture);
  }
}

GLuint GLTexturePool::GetTexture(GLuint format, uint64_t index) {
  return m_BigTexturesMap[format].textures[index];
}

uint32_t GLTexturePool::GetTextureWidth(GLuint format, uint64_t index) {
  return m_BigTexturesMap[format].dimensions[index].first;
}

uint32_t GLTexturePool::GetTextureHeight(GLuint format, uint64_t index) {
  return m_BigTexturesMap[format].dimensions[index].second;
}

void GLTexturePool::CreateTexture2D(GLenum format, uint32_t width,
                                    uint32_t height, GLuint &texture) {
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format,
               GL_UNSIGNED_BYTE, nullptr);
}

void GLTexturePool::AllocateSubTexture(GLenum format, uint32_t width,
                                       uint32_t height, void *data,
                                       uint64_t &textureIndex,
                                       uint32_t &offsetX, uint32_t &offsetY) {
  // We add 1 more row and column to the subtexture to prevent some wired
  // artifacts
  FindSubTextureRegion(format, width + 1, height + 1, textureIndex, offsetX,
                       offsetY);
  if (data)
    CopyDataToTexture(format, textureIndex, offsetX, offsetY, width, height,
                      data);
}

void GLTexturePool::Deallocate(GLenum format, uint64_t textureIndex,
                               uint32_t offsetX, uint32_t offsetY,
                               uint32_t width, uint32_t height) {
  m_BigTexturesMap[format].poolTracker->Deallocate(textureIndex, offsetX,
                                                   offsetY, width, height);
}

void GLTexturePool::FindSubTextureRegion(GLenum format, uint32_t width,
                                         uint32_t height,
                                         uint64_t &textureIndex,
                                         uint32_t &offsetX, uint32_t &offsetY) {
  uint64_t size = width * height;
  if (m_BigTexturesMap.find(static_cast<uint32_t>(format)) ==
      m_BigTexturesMap.end()) {
    m_BigTexturesMap.insert(std::make_pair(
        format, BigTextures{std::vector<GLuint>(0),
                            std::vector<std::pair<uint32_t, uint32_t>>(0),
                            std::make_unique<PoolTracker2D>(
                                DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT)}));
  }
  auto result = m_BigTexturesMap[format].poolTracker->AllocateSubRegion(
      textureIndex, offsetX, offsetY, width, height);
  if (!result) {
    GLuint newTexture;
    CreateTexture2D(format, width, height, newTexture);
    m_BigTexturesMap[format].textures.push_back(newTexture);
    m_BigTexturesMap[format].dimensions.push_back(
        std::make_pair(width, height));
  }
}

void GLTexturePool::CopyDataToTexture(GLenum format, uint64_t textureIndex,
                                      uint32_t offsetX, uint32_t offsetY,
                                      uint32_t width, uint32_t height,
                                      void *data) {
  glBindTexture(GL_TEXTURE_2D, m_BigTexturesMap[format].textures[textureIndex]);
  glTexSubImage2D(GL_TEXTURE_2D, 0, offsetX, offsetY, width, height, format,
                  GL_UNSIGNED_BYTE, data);
}
