#include "GLAPI/GLBufferPool.h"
#include "Util/PoolTracker1D.h"

using namespace Ez;

GLBufferPool::GLBufferPool(GLenum target) : m_Target(target) {
  if (m_Target == GL_ARRAY_BUFFER)
    m_PoolTracker = std::make_unique<PoolTracker1D>(DEFAULT_BUFFER_SIZE);
  else
    m_PoolTracker =
        std::make_unique<PoolTracker1D>(DEFAULT_UNIFORM_BUFFER_SIZE);
}

GLBufferPool::~GLBufferPool() {
  for (auto &buffer : m_BigBuffers)
    glDeleteBuffers(1, &buffer);
}

void GLBufferPool::AllocateSubBuffer(uint64_t size, void *data,
                                     uint64_t &bufferIndex,
                                     uint64_t &subBufferOffset) {
  FindBufferSubRegion(size, bufferIndex, subBufferOffset);
  if (data)
    CopyToDeviceMemory(bufferIndex, subBufferOffset, size, data);
}

void GLBufferPool::Deallocate(uint64_t bufferIndex, uint64_t subBufferOffset,
                              uint64_t subBufferSize) {
  m_PoolTracker->Deallocate(bufferIndex, subBufferOffset, subBufferSize);
}

void GLBufferPool::CreateBuffer(uint64_t size, GLuint &bufferObject) {
  glGenBuffers(1, &bufferObject);
  glBindBuffer(m_Target, bufferObject);
  glBufferData(m_Target, static_cast<GLsizeiptr>(size), nullptr,
               GL_STATIC_DRAW);
}

void GLBufferPool::FindBufferSubRegion(uint64_t size, uint64_t &bufferIndex,
                                       uint64_t &subBufferOffset) {
  auto result =
      m_PoolTracker->AllocateSubRegion(size, bufferIndex, subBufferOffset);
  if (!result) {
    GLuint newBuffer;
    CreateBuffer(size, newBuffer);
    if (m_Target == GL_UNIFORM_BUFFER) {
      GLint alignment;
      glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &alignment);
      m_PoolTracker->SetLastMetadataAlignment(alignment);
    }
    m_BigBuffers.push_back(newBuffer);
  }
}

GLuint GLBufferPool::GetBufferObject(uint64_t bufferIndex) {
  return m_BigBuffers[bufferIndex];
}

void GLBufferPool::CopyToDeviceMemory(uint64_t bufferIndex,
                                      uint64_t subBufferOffset,
                                      uint64_t dataSize, void *data) {
  glBindBuffer(m_Target, m_BigBuffers[bufferIndex]);
  glBufferSubData(m_Target, subBufferOffset, dataSize, data);
  glBindBuffer(m_Target, 0);
}
