//===-- GLAPI/Shader.cpp - Implement the GLShader class -------------------===//
//===----------------------------------------------------------------------===//
//
// This file implements the GLShader class
//
//===----------------------------------------------------------------------===//

#include "GLAPI/GLShader.h"
#include "GLAPI/GLBuffer.h"
#include <Util/File.h>
#include <Util/Log.h>
#include <sstream>

using std::string;
using namespace Ez;

GLuint GLShader::_BindingPoint = 0;

Ez::GLShader::GLShader() {}

Ez::GLShader::GLShader(const char *vertexFile, const char *fragmentFile) {
  LoadShaderFromFile(GL_VERTEX_SHADER, vertexFile);
  LoadShaderFromFile(GL_FRAGMENT_SHADER, fragmentFile);
  LinkProgram();
}

Ez::GLShader::~GLShader() { glDeleteProgram(m_Program); }

void Ez::GLShader::LoadShaderFromString(GLenum TYPE, const char *shaderString) {
  string shader(shaderString);
  std::stringstream ss;
  GLint majorVersion;
  GLint minorVersion;
  glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
  glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
#if !(defined(__ANDROID_API_) || defined(ANDROID))
  ss << "#version " << majorVersion << minorVersion << 0 << std::endl;
  shader = ss.str() + shader;
#else
  ss << "#version " << majorVersion << minorVersion << 0 << " es" << std::endl;
  if (TYPE == GL_FRAGMENT_SHADER)
    ss << "precision highp float;" << std::endl;
  shader = ss.str() + shader;
#endif
  GLuint shaderObject = glCreateShader(TYPE);
  const GLchar *source = shader.c_str();
  glShaderSource(shaderObject, 1, &source, 0);
  glCompileShader(shaderObject);

  GLint isCompiled = 0;
  glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &isCompiled);
  if (!isCompiled) {
    GLint maxLength = 0;
    glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &maxLength);

    GLchar *infoLog = new GLchar[maxLength];
    glGetShaderInfoLog(shaderObject, maxLength, &maxLength, infoLog);
    LOGE("GLShader", "%s\nError: %s", shader.c_str(), infoLog);
    glDeleteShader(shaderObject);
  }
  m_Shaders.push_back(shaderObject);
}

void Ez::GLShader::LoadShaderFromFile(GLenum TYPE, const char *fileName) {
  auto shaderString = File::ReadFile(fileName, false);
  LoadShaderFromString(TYPE, (const char *)(shaderString.data()));
}

void Ez::GLShader::LinkProgram() {
  m_Program = glCreateProgram();
  for (GLuint s : m_Shaders)
    glAttachShader(m_Program, s);
  glLinkProgram(m_Program);
  GLint isLinked = 0;
  glGetProgramiv(m_Program, GL_LINK_STATUS, &isLinked);
  if (!isLinked) {
    GLint maxLength = 0;
    glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &maxLength);

    GLchar *infoLog = new GLchar[maxLength];
    glGetProgramInfoLog(m_Program, maxLength, &maxLength, infoLog);
    LOGE("GLShader", "Linking error: %s", infoLog);
    glDeleteProgram(m_Program);
  }
  for (GLuint s : m_Shaders)
    glDetachShader(m_Program, s);
}

GLuint Ez::GLShader::GetUniform(const char *name) {
  // if (m_Uniforms.count(name) == 0) {
  return glGetUniformLocation(m_Program, name);
  /*  m_Uniforms[name] = result;
  }
  else {
    result = m_Uniforms[name];
  }*/
}

void GLShader::BindBuffer(std::shared_ptr<GLBuffer> buffer,
                          const char *uniformBlockName) {
  auto index = glGetUniformBlockIndex(m_Program, uniformBlockName);
  glUniformBlockBinding(m_Program, index, _BindingPoint);
  glBindBufferRange(GL_UNIFORM_BUFFER, _BindingPoint, buffer->GetBuffer(),
                    static_cast<GLintptr>(buffer->GetMemoryOffset()),
                    static_cast<GLsizeiptr>(buffer->GetMemorySize()));
  m_UniformBuffers.push_back(std::move(buffer));
  _BindingPoint++;
}
