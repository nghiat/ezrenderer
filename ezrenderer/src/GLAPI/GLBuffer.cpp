#include "GLAPI/GLBuffer.h"
#include "GLAPI/GLBufferPool.h"

using namespace Ez;

GLBuffer::GLBuffer(std::shared_ptr<GLBufferPool> bufferPool, uint64_t dataSize,
                   void *data)
    : m_BufferPool(std::move(bufferPool)), m_SubBufferSize(dataSize) {
  m_BufferPool->AllocateSubBuffer(dataSize, data, m_BufferIndex,
                                  m_SubBufferOffset);
}
GLBuffer::~GLBuffer() {
  m_BufferPool->Deallocate(m_BufferIndex, m_SubBufferOffset, m_SubBufferSize);
}

void GLBuffer::UpdateData(uint64_t offset, uint64_t dataSize, void *data) {
  m_BufferPool->CopyToDeviceMemory(m_BufferIndex, m_SubBufferOffset + offset,
                                   dataSize, data);
}
uint64_t GLBuffer::GetMemoryOffset() const { return m_SubBufferOffset; }

uint64_t GLBuffer::GetMemorySize() const { return m_SubBufferSize; }

GLuint GLBuffer::GetBuffer() const {
  return m_BufferPool->GetBufferObject(m_BufferIndex);
}
