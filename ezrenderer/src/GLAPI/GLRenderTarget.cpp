#include "GLAPI/GLRenderTarget.h"

using namespace Ez;

GLRenderTarget::GLRenderTarget(int width, int height, int depth)
    : m_Width(width), m_Height(height), m_Depth(depth) {
  glGenFramebuffers(1, &m_Framebuffer);
  glGenTextures(1, &m_ColorAttachment);
  glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);
  glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, GL_FALSE, GL_RGBA,
               GL_HALF_FLOAT, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         m_ColorAttachment, 0);

  if (depth == 24) {
    glGenRenderbuffers(1, &m_DepthAttachment);
    glBindRenderbuffer(GL_RENDERBUFFER, m_DepthAttachment);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, m_DepthAttachment);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}

GLRenderTarget::~GLRenderTarget() {}

void GLRenderTarget::Resize(int w, int h) {
  glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);
  glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, GL_FALSE, GL_RGBA,
               GL_HALF_FLOAT, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         m_ColorAttachment, 0);
  if (glIsRenderbuffer(m_DepthAttachment)) {
    glBindRenderbuffer(GL_RENDERBUFFER, m_DepthAttachment);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, w, h);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, m_DepthAttachment);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLRenderTarget::BindFramebuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, m_Framebuffer);
  glViewport(0, 0, m_Width, m_Height);
}

void GLRenderTarget::BindColorTexture() {
  glBindTexture(GL_TEXTURE_2D, m_ColorAttachment);
}

void GLRenderTarget::UnbindColorTexture() { glBindTexture(GL_TEXTURE_2D, 0); }

int GLRenderTarget::GetWidth() { return m_Width; }

int GLRenderTarget::GetHeight() { return m_Height; }