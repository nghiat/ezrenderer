#include "GLAPI/GLTexture.h"
#include "GLAPI/GLTexturePool.h"
#include <Util/Texture.h>

using namespace Ez;

GLTexture::GLTexture(std::shared_ptr<GLTexturePool> texturePool,
                     Texture *texture)
    : m_TexturePool(std::move(texturePool)) {
  m_Width = texture->GetX();
  m_Height = texture->GetY();
  ChooseFormat(texture->GetChannel());
  m_TexturePool->AllocateSubTexture(m_Format, m_Width, m_Height,
                                    texture->GetData(), m_TextureIndex,
                                    m_OffsetX, m_OffsetY);
  uint32_t rightX = m_OffsetX + m_Width;
  uint32_t topY = m_OffsetY + m_Height;
  uint32_t imageWidth =
      m_TexturePool->GetTextureWidth(m_Format, m_TextureIndex);
  uint32_t imageHeight =
      m_TexturePool->GetTextureHeight(m_Format, m_TextureIndex);
  m_UV.resize(4);
  m_UV[0] = m_OffsetX * 1.f / imageWidth;
  m_UV[1] = m_OffsetY * 1.f / imageHeight;
  m_UV[2] = rightX * 1.f / imageHeight;
  m_UV[3] = topY * 1.f / imageHeight;
}

GLTexture::~GLTexture() {
  m_TexturePool->Deallocate(m_Format, m_TextureIndex, m_OffsetX, m_OffsetY,
                            m_Width, m_Height);
}

GLuint GLTexture::GetTexture() const {
  return m_TexturePool->GetTexture(m_Format, m_TextureIndex);
}

std::vector<float> GLTexture::GetUV() const { return m_UV; }

void GLTexture::ChooseFormat(int channels) {
  if (channels == 4)
    m_Format = GL_RGBA;
  if (channels == 1)
    m_Format = GL_RED;
}
