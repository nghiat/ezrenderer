#ifndef EZRENDERER_VULKAN_VULKANSHADER_H
#define EZRENDERER_VULKAN_VULKANSHADER_H

#include "vulkan.h"
#include <memory>
#include <vector>
namespace Ez {
class VulkanDevice;

class VulkanShader {
public:
  VulkanShader(std::shared_ptr<VulkanDevice> device,
               VkShaderStageFlagBits shaderStageFlagBits,
               const char *pathToFile);
  ~VulkanShader();
  void CreateShaderModule(std::vector<char> shader);
  VkPipelineShaderStageCreateInfo GetPipelineShaderStateCreateInfo();

private:
  VkShaderModule m_Shadermodule;
  std::shared_ptr<VulkanDevice> m_Device;
  VkShaderStageFlagBits m_ShaderStageFlagBits;
};
}

#endif // EZRENDERER_VULKAN_VULKANSHADER_H
