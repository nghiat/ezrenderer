#pragma once
#include "vulkan.h"
#include <memory>
#include <vector>

namespace Ez {
class VulkanDevice;

class VulkanFence {
public:
  VulkanFence(std::shared_ptr<VulkanDevice> device);
  ~VulkanFence();
  VkFence GetFence() const;
  void Reset();
  void Wait();
  static void WaitFences(std::vector<std::unique_ptr<VulkanFence>> &fences);

private:
  std::shared_ptr<VulkanDevice> m_Device;
  VkFence m_Fence;
};

class VulkanSemaphore {
public:
  VulkanSemaphore(std::shared_ptr<VulkanDevice> device);
  ~VulkanSemaphore();
  VkSemaphore GetSemaphore();

private:
  std::shared_ptr<VulkanDevice> m_Device;
  VkSemaphore m_Semaphore;
};
}
