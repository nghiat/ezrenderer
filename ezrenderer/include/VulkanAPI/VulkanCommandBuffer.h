#ifndef EZRENDERER_VULKAN_VULKANCOMMANDBUFFER_H
#define EZRENDERER_VULKAN_VULKANCOMMANDBUFFER_H

#include "vulkan.h"
#include <memory>
#include <vector>

namespace Ez {
class VulkanCommandPool;
class VulkanDevice;

class VulkanCommandBuffer {
public:
  VulkanCommandBuffer(
      std::shared_ptr<VulkanDevice> device,
      std::shared_ptr<VulkanCommandPool> &commandPool,
      VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);

  ~VulkanCommandBuffer();

  /**
   * Get the command buffer
   * @return The VkCommandBuffer object
   */
  VkCommandBuffer GetCommandBuffer();

  /**
   * Begin recording commands
   * @param flags
   * @param inheritanceInfo
   */
  void Begin(VkCommandBufferUsageFlags flags = 0,
             VkCommandBufferInheritanceInfo *inheritanceInfo = nullptr);

  /**
   * Stop recording commands
   */
  void End();
  void Reset();

private:
  VkCommandBuffer m_CommandBuffer;

  std::shared_ptr<VulkanDevice> m_Device;

  std::weak_ptr<VulkanCommandPool> m_CommandPool;
};
}

#endif // EZRENDERER_VULKAN_VULKANCOMMANDBUFFER_H
