#ifndef EZRENDERER_VULKAN_VULKANGRAPHICSPIPELINE_H
#define EZRENDERER_VULKAN_VULKANGRAPHICSPIPELINE_H

#include "vulkan.h"
#include <functional>
#include <memory>
#include <vector>

namespace Ez {
class VulkanDevice;
class VulkanSwapchain;
class VulkanCommandBuffer;
class VulkanDescriptorSet;
class VulkanShader;
class VulkanBuffer;
class VulkanRenderTarget;

class VulkanGraphicsPipeline {
public:
  VulkanGraphicsPipeline(std::shared_ptr<VulkanDevice> device);
  ~VulkanGraphicsPipeline();
  void CreateGraphicsPipeline(VkPrimitiveTopology topology,
                              std::unique_ptr<VulkanRenderTarget> &renderTarget,
                              bool enableBlend = false);
  void AddDescriptorSet(std::shared_ptr<VulkanDescriptorSet> descriptorSet);
  void AddDescriptorSet(
      std::vector<std::shared_ptr<VulkanDescriptorSet>> &descriptorSets);
  void ClearDescriptorSets();

  void AddShader(std::shared_ptr<VulkanShader> shader);
  void AddShaders(std::vector<std::shared_ptr<VulkanShader>> &shaders);
  void ClearShaders();

  void BindPipeline(VkCommandBuffer commandBuffer);
  VkPipeline GetGraphicsPipeline() const;
  VkPipelineLayout GetGraphicsPipelineLayout() const;

  /**
   * The number of bindings is equal to the size of strides vector
   * @param bindings vector of bindings, each must be < strides.size()
   * @param offsets vector of offsets
   * @param formats vector of formats
   */
  void SetupInputState(std::vector<uint32_t> &strides,
                       std::vector<uint32_t> &bindings,
                       std::vector<uint32_t> &offsets,
                       std::vector<VkFormat> &formats);
  void AddPushConstants(VkShaderStageFlags stage, uint32_t offset,
                        uint32_t size);

private:
  std::shared_ptr<VulkanDevice> m_Device;
  std::vector<std::shared_ptr<VulkanDescriptorSet>> m_DescriptorSets;
  std::vector<std::shared_ptr<VulkanShader>> m_Shaders;
  // The distance in bytes between two consecutive elements within the buffer
  std::vector<uint32_t> m_Strides;
  // Offsets of attributes relative to the start of an element
  std::vector<uint32_t> m_Offsets;
  // The sizes and types of the vertex attributes data
  std::vector<VkFormat> m_Formats;
  // The bindings of the vertex attributes data
  std::vector<uint32_t> m_Bindings;
  std::vector<VkPushConstantRange> m_PushConstantRanges;
  uint32_t m_FramebufferCount;
  std::vector<VkFramebuffer> m_Framebuffers;
  std::vector<std::unique_ptr<VulkanCommandBuffer>>
      m_RecordDrawingCommmandBuffers;

  VkPipelineCache m_PipelineCache = VK_NULL_HANDLE;
  VkPipeline m_GraphicsPipeline = VK_NULL_HANDLE;
  VkPipelineLayout m_PipelineLayout = VK_NULL_HANDLE;

  void CreatePipelineCache();
  void CreatePipelineLayout();
};
}

#endif // EZRENDERER_VULKAN_VULKANGRAPHICSPIPELINE_H
