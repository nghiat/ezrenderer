//
// Created by nghia on 2/1/17.
//

#ifndef EZRENDERER_VULKAN_VULKANINSTANCE_H
#define EZRENDERER_VULKAN_VULKANINSTANCE_H

#include "vulkan.h"
namespace Ez {
class VulkanInstance {
public:
  /**
   * @param enableValidation Enable validation layer
   */
  VulkanInstance(bool enableValidation);

  ~VulkanInstance();

  /**
   * Get the VkInstance object
   * @return
   */
  VkInstance GetInstance() { return m_Instance; }

  /**
   * Set debug callback (usally is a logging callback)
   * @param debugReportCallback
   */
  void SetDebugCallback(PFN_vkDebugReportCallbackEXT debugReportCallback);

private:
  /**
   *
   */
  VkInstance m_Instance;
  VkDebugReportCallbackEXT m_DebugReportCallback;

  /**
   * Get required instance extensions for each platform
   * @param extCount nullptr, number of extensions will overwrite this
   * @param ppExtNames nullptr, names of extensions will overwrite this
   */
  void GetPlatformExtensions(uint32_t &extCount, const char **&ppExtNames);

  static VKAPI_ATTR VkBool32 VKAPI_CALL DebugReportCallback(
      VkDebugReportFlagsEXT msgFlags, VkDebugReportObjectTypeEXT objType,
      uint64_t srcObject, size_t location, int32_t msgCode,
      const char *pLayerPrefix, const char *pMsg, void *pUserData);

  PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT;
  PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT;
};
}
#endif // EZRENDERER_VULKAN_VULKANINSTANCE_H
