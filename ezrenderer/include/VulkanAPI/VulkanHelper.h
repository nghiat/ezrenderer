//
// Created by nghia on 2/1/17.
//

#ifndef EZRENDERER_VULKAN_VULKANHELPER_H
#define EZRENDERER_VULKAN_VULKANHELPER_H

#include "VulkanAPI/vulkan.h"

#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)
#define S__FILE__ S_(__FILE__)

#define VK_CHECK(result)                                                       \
  Ez::VulkanHelper::CheckVkResult(result, S__LINE__, S__FILE__)

namespace Ez {
class VulkanHelper {
public:
  static void CheckVkResult(VkResult result, const char *line = nullptr,
                            const char *file = nullptr);

private:
};
}

#endif // EZRENDERER_VULKAN_VULKANHELPER_H
