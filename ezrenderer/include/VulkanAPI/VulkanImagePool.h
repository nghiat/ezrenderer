#pragma once

#include "vulkan.h"
#include <memory>
#include <unordered_map>
#include <vector>

#define DEFAULT_IMAGE_WIDTH 8192
#define DEFAULT_IMAGE_HEIGHT 8192

#define DEFAULT_IMAGE_SIZE 67108864 /*8192*8192*/

namespace Ez {
class VulkanDevice;
class VulkanCommandPool;
class VulkanCommandBuffer;
class VulkanBufferPool;
class VulkanFence;
class PoolTracker2D;

struct BigImages {
  std::vector<VkImage> images;
  std::vector<VkDeviceMemory> memories;
  std::vector<bool> isInit;
  // each pair contains width and height of sub image
  std::vector<std::pair<uint32_t, uint32_t>> dimensions;
  std::unique_ptr<PoolTracker2D> poolTracker;
};

class VulkanImagePool {
  friend class VulkanTexture;
  friend class VulkanTextureCube;

public:
  VulkanImagePool(std::shared_ptr<VulkanDevice> device,
                  std::shared_ptr<VulkanCommandPool> commandPool);
  ~VulkanImagePool();
  VkImage GetImage(VkFormat format, uint64_t index);
  uint32_t GetImageWidth(VkFormat format, uint64_t index);
  uint32_t GetImageHeight(VkFormat format, uint64_t index);

private:
  std::shared_ptr<VulkanDevice> m_Device;
  std::shared_ptr<VulkanCommandPool> m_CommandPool;
  std::unique_ptr<VulkanCommandBuffer> m_CommandBuffer;
  std::unique_ptr<VulkanFence> m_Fence;

  std::unordered_map<uint32_t, BigImages> m_BigImagesMap;
  // Staging buffer to copy data from host visible to device local memory
  VkBuffer m_StagingBuffer;

  // Memory bound to the staging buffer
  VkDeviceMemory m_StagingMemory;
  static void CreateImage2D(const std::shared_ptr<VulkanDevice> &device,
                            VkFormat format, uint32_t width, uint32_t height,
                            VkImage &image, VkDeviceMemory &memory,
                            uint64_t &alignment, VkImageCreateFlags flags = 0);
  void AllocateSubImage(VkFormat format, uint32_t width, uint32_t height,
                        uint8_t channels, void *data, uint64_t &imageIndex,
                        uint32_t &offsetX, uint32_t &offsetY);
  void Deallocate(VkFormat format, uint64_t imageIndex, uint32_t offsetX,
                  uint32_t offsetY, uint32_t width, uint32_t height);
  void FindSubImageRegion(VkFormat format, uint32_t width, uint32_t height,
                          uint64_t &imageIndex, uint32_t &offsetX,
                          uint32_t &offsetY);
  void CopyBufferToImage(VkFormat format, uint64_t imageIndex, uint32_t offsetX,
                         uint32_t offsetY, uint32_t width, uint32_t height,
                         uint8_t channels, void *data);

  static void
  TransitionImageLayout(std::unique_ptr<VulkanCommandBuffer> &commandBuffer,
                        VkImage &image, VkImageLayout oldLayout,
                        VkImageLayout newLayout,
                        VkImageSubresourceRange &subresourceRange);
};
}
