#ifndef EZRENDERER_VULKAN_VULKANDESCRIPTORSET_H
#define EZRENDERER_VULKAN_VULKANDESCRIPTORSET_H

#include "vulkan.h"
#include <memory>
#include <unordered_map>
#include <vector>

namespace Ez {
class VulkanDevice;
class VulkanBuffer;
class VulkanTexture;
class VulkanRenderTarget;

class VulkanDescriptorSet {
public:
  VulkanDescriptorSet(
      std::shared_ptr<VulkanDevice> device,
      std::vector<VkDescriptorSetLayoutBinding> &layoutBindings);
  ~VulkanDescriptorSet();
  void SetBuffer(uint32_t binding, uint32_t arrayIndex,
                 std::unique_ptr<VulkanBuffer> &buffer,
                 bool isDynamicUniform = false);
  void SetTexture(uint32_t binding, uint32_t arrayIndex,
                  VulkanTexture *texture);
  void SetRenderTarget(uint32_t binding, uint32_t arrayIndex,
                       std::unique_ptr<Ez::VulkanRenderTarget> &rt);
  VkDescriptorSetLayout GetDescriptorSetLayout();
  VkDescriptorSet GetDescriptorSet();

private:
  std::shared_ptr<VulkanDevice> m_Device;
  // We use <<uint32_t, uint32_t> instead of <VkDescriptorType, uint32_t>
  // because
  // there are duplications in VkDescriptorType enum
  std::unordered_map<uint32_t, uint32_t> m_CountForTypes;
  VkDescriptorSetLayout m_DescriptorSetLayout;
  VkDescriptorPool m_DescriptorPool;
  VkDescriptorSet m_DescriptorSet;
  void CreateDescriptorSetLayout(
      std::vector<VkDescriptorSetLayoutBinding> &layoutBindings);
  void CreateDescriptorPool();
  void CreateDescriptorSet();
};
}

#endif // EZRENDERER_VULKAN_VULKANDESCRIPTORSET_H
