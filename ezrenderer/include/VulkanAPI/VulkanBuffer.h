//===-- VulkanAPI/VulkanBuffer.h ----------------------------------* C++ -*-===/
//===-----------------------------------------------------------------------===/
//
// This file contains the declaration of the VulkanBuffer class, which uses a
// VulkanBufferPool to allocate and push data to GPU then get the info of the
// data back to fedd to a pipeline
//
//===-----------------------------------------------------------------------===/

#ifndef EZRENDERER_VULKAN_VULKANVERTEXBUFFER_H
#define EZRENDERER_VULKAN_VULKANVERTEXBUFFER_H

#include "vulkan.h"
#include <memory>
#include <vector>

namespace Ez {

class VulkanBufferPool;
class VulkanBuffer {
public:
  /**
   * Use a buffer pool to push data to GPU then get to info back
   * @param dataSize The size of data
   * @param data The data
   */
  VulkanBuffer(std::shared_ptr<VulkanBufferPool> bufferPool, uint64_t dataSize,
               void *data);

  /**
   * Send the info back to the buffer pool and let the pool do the deallocation
   */
  ~VulkanBuffer();

  /**
   * Get VkPipelineVertexInputStateCreateInfo to feed into
   * VulkanGraphicsPipeline
   * @param stride The distance in bytes between two
   * consecutive elements within the buffer.
   * @param count Number of attributes
   * @param pOffsets Offset of each attribute
   * @param pFormats Format of each attribute
   */
  VkPipelineVertexInputStateCreateInfo
  GetVertexInputState(uint32_t stride, std::vector<uint32_t> &pOffsets,
                      std::vector<VkFormat> &pFormats);

  void UpdateData(uint64_t offset, uint64_t dataSize, void *data);
  VkBuffer GetBuffer();
  uint64_t GetMemoryOffset();
  uint64_t GetMemorySize();

private:
  std::shared_ptr<VulkanBufferPool> m_BufferPool;
  uint64_t m_BufferIndex;
  uint64_t m_SubBufferOffset;
  uint64_t m_SubBufferSize;
  VkVertexInputBindingDescription m_VertexInputBindingDescription;
  std::vector<VkVertexInputAttributeDescription>
      m_VertexInputAttributeDescriptions;
};
}

#endif // EZRENDERER_VULKAN_VULKANVERTEXBUFFER_H