#pragma once
#include "VulkanAPI/vulkan.h"
#include <memory>
#include <vector>

namespace Ez {
class VulkanDevice;
class VulkanImagePool;
class Texture;

class VulkanTexture {
public:
  VulkanTexture(){};
  VulkanTexture(std::shared_ptr<VulkanDevice> device,
                std::shared_ptr<VulkanImagePool> imagePool, Texture *texture);
  ~VulkanTexture();
  VkSampler GetSampler();
  VkImageView GetImageView();
  /*
   * Return leftX, bottomY, rightX, topY
   */
  std::vector<float> GetUV();

protected:
  std::shared_ptr<VulkanDevice> m_Device;
  std::shared_ptr<VulkanImagePool> m_ImagePool = nullptr;
  VkFormat m_Format;
  VkImage m_Image;
  uint32_t m_Width;
  uint32_t m_Height;
  uint64_t m_ImageIndex;
  uint32_t m_OffsetX;
  uint32_t m_OffsetY;
  VkSampler m_Sampler;
  VkImageView m_ImageView;
  std::vector<float> m_UV;
  void CreateSampler();
  void CreateImageView(VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D);
  void ChooseFormat(int channels);
};
}
