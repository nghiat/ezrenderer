//===-- VulkanAPI/VulkanBufferPool.h ------------------------------* C++ -*-===/
//===-----------------------------------------------------------------------===/
//
// This file contains the declaration of the VulkanBufferPool class, which
// creates many large chunks of memory to allocate smaller buffers from that
//
//===-----------------------------------------------------------------------===/
#pragma once

#include "vulkan.h"
#include <memory>
#include <vector>

#define DEFAULT_BUFFER_SIZE 134217728
#define SMALL_BUFFER_SIZE 4194304
#define DEFAULT_STAGING_SIZE 5242880

namespace Ez {
class VulkanDevice;
class VulkanCommandPool;
class VulkanCommandBuffer;
class VulkanBuffer;
class VulkanFence;
class PoolTracker1D;

class VulkanBufferPool {
  friend class VulkanBuffer;
  friend class VulkanImagePool;
  friend class VulkanTextureCube;

public:
  VulkanBufferPool(std::shared_ptr<VulkanDevice> device,
                   std::shared_ptr<VulkanCommandPool> commandPool);
  /**
   * Create a buffer pool that use only host memory
   * Usually used for uniform buffer
   * @param device
   * @return
   */
  VulkanBufferPool(std::shared_ptr<VulkanDevice> device);
  ~VulkanBufferPool();

private:
  std::shared_ptr<VulkanDevice> m_Device;
  std::shared_ptr<VulkanCommandPool> m_CommandPool;
  std::unique_ptr<VulkanCommandBuffer> m_CommandBuffer;
  std::unique_ptr<VulkanFence> m_Fence;

  std::unique_ptr<PoolTracker1D> m_PoolTracker;
  bool m_UseDeviceMemory = false;
  // Big buffers
  std::vector<VkBuffer> m_BigBuffers;

  // Memories bound to the m_BigBuffers
  std::vector<VkDeviceMemory> m_BigMemories;

  // Staging buffer to copy data from host visible to device local memory
  VkBuffer m_StagingBuffer = VK_NULL_HANDLE;

  // Memory bound to the staging buffer
  VkDeviceMemory m_StagingMemory = VK_NULL_HANDLE;

  void AllocateSubBuffer(uint64_t size, void *data, uint64_t &bufferIndex,
                         uint64_t &subBufferOffset);

  /**
   * Make the region in a buffer available to use
   * @param bufferIndex
   * @param subBufferOffset
   * @param subBufferSize
   */
  void Deallocate(uint64_t bufferIndex, uint64_t subBufferOffset,
                  uint64_t subBufferSize);
  /**
   * Create a buffer, device memory then bind them together
   * @param size Size of the buffer
   * @param usage Usage flags to create the buffer
   * @param properties Memory properties used to get memory index
   * @param[out] buffer The buffer to be created
   * @param[out] deviceMemory Device memory to be created
   * @param[out] alignment Alignment of the memory
   */
  static void CreateBuffer(const std::shared_ptr<VulkanDevice> &device,
                           uint64_t size, VkBufferUsageFlags usage,
                           VkMemoryPropertyFlags properties, VkBuffer &buffer,
                           VkDeviceMemory &deviceMemory, uint64_t &alignment);
  /**
 * Find region that can fit the data
 * If no region found, allocate a new big buffer
 * @param size Size of the data
 * @param[out] bufferIndex The index of a big buffer
 * @param[out] subBufferOffset The offset of the region
 */
  void FindBufferSubRegion(uint64_t size, uint64_t &bufferIndex,
                           uint64_t &subBufferOffset);

  /**
   * Copy data from host visible to device local memory
   * @param bufferIndex The index of destination buffer
   * @param subBufferOffset The offset the region
   * @param dataSize The size of the data
   * @param data The data
   */
  void CopyToDeviceMemory(uint64_t bufferIndex, uint64_t subBufferOffset,
                          uint64_t dataSize, void *data);

  VkBuffer GetBuffer(uint64_t bufferIndex);
};
}