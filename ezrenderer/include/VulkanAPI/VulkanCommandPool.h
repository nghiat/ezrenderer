#ifndef EZRENDERER_VULKAN_VULKANCOMMANDPOOL_H
#define EZRENDERER_VULKAN_VULKANCOMMANDPOOL_H
#include "vulkan.h"
#include <memory>
namespace Ez {
class VulkanDevice;

class VulkanCommandPool {
public:
  VulkanCommandPool(std::shared_ptr<VulkanDevice> device,
                    VkCommandPoolCreateFlags flags =
                        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
  ~VulkanCommandPool();
  VkCommandPool GetCommandPool() { return m_CommnadPool; };
  void ResetCommadPool();

private:
  VkCommandPool m_CommnadPool;
  std::shared_ptr<VulkanDevice> m_Device;
  VkCommandPoolCreateFlags m_Flags;
};
}

#endif // EZRENDERER_VULKAN_VULKANCOMMANDPOOL_H
