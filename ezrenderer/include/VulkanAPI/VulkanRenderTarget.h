#pragma once

#include "vulkan.h"
#include <functional>
#include <memory>
#include <vector>

namespace Ez {
class VulkanDevice;
class VulkanSwapchain;
class VulkanCommandPool;
class VulkanCommandBuffer;
class VulkanSemaphore;

class VulkanRenderTarget {
public:
  /**
   * Create main render target used with swapchains
   * We use swapchain image views to create framebuffers attached to this
   * renderpass
   */
  VulkanRenderTarget(std::shared_ptr<VulkanDevice> device,
                     std::shared_ptr<VulkanSwapchain> swapchain,
                     std::shared_ptr<VulkanCommandPool> &commandPool);
  /**
   * Create a render target with separate imageview
   */
  VulkanRenderTarget(std::shared_ptr<VulkanDevice> device,
                     std::shared_ptr<VulkanCommandPool> &commandPool,
                     uint32_t width, uint32_t height);
  ~VulkanRenderTarget();

  void SetDrawCallback(std::function<void(VkCommandBuffer commandBuffer)> f);
  VkRenderPass GetRenderPass() const;
  // Only use this on main RenderTarget
  void Draw();
  VkImage GetImage();
  VkImageView GetImageView();
  VkSampler GetSampler();
  uint32_t GetWidth();
  uint32_t GetHeight();
  void AddNextRendertarget(VulkanRenderTarget *rt);

private:
  std::shared_ptr<VulkanDevice> m_Device;
  std::shared_ptr<VulkanSwapchain> m_Swapchain;
  std::shared_ptr<VulkanCommandPool> m_CommandPool;
  // Will be rendered after this
  std::vector<VulkanRenderTarget *> m_NextRendertargets;
  bool m_UseSwapchain = false;
  uint32_t m_Width;
  uint32_t m_Height;
  VkRenderPass m_RenderPass;

  std::vector<VkFramebuffer> m_Framebuffers;
  std::vector<std::unique_ptr<VulkanCommandBuffer>> m_CommandBuffers;
  VkImage m_ColorImage;

  VkDeviceMemory m_ColorImageMemory;

  VkImageView m_ColorImageView;

  VkImage m_DepthStencilImage;
  VkDeviceMemory m_DepthStencilImageMemory;
  VkImageView m_DepthStencilImageView;

  VkQueue m_Queue;
  std::unique_ptr<VulkanSemaphore> m_ImageAvailableSemaphore;
  std::unique_ptr<VulkanSemaphore> m_RenderFinishedSemaphore;
  // Used for non-swapchain render target
  std::unique_ptr<VulkanSemaphore> m_SeparateSemaphore;

  std::function<void(VkCommandBuffer commandBuffer)> m_Drawcallback;
  uint32_t m_CurrentImageIndex;

  VkSampler m_Sampler = VK_NULL_HANDLE;

  void CreateRenderpass(VkFormat format);
  // We need a separate method because both kinds of render target need same
  // depth/stencil image
  void CreateDepthStencilImageAndView(VkFormat depthFormat, uint32_t width,
                                      uint32_t height);
  // Return semaphore of the final dependency
  std::vector<VkSemaphore>
  CreateSubmitsChain(std::vector<VulkanRenderTarget *> &rts);
};
}
