//
// Created by nghia on 2/1/17.
//

#ifndef EZRENDERER_VULKAN_VULKANSWAPCHAIN_H
#define EZRENDERER_VULKAN_VULKANSWAPCHAIN_H

#include "vulkan.h"
#include <memory>
#include <vector>

namespace Ez {
class VulkanInstance;
class VulkanDevice;
class VulkanCommandBuffer;
class VulkanSemaphore;

class VulkanSwapchain {
public:
  VulkanSwapchain(std::shared_ptr<VulkanInstance> instance,
                  std::shared_ptr<VulkanDevice> device, uint32_t imageCount,
                  void *window);
  ~VulkanSwapchain();

  VkFormat GetSurfaceFormat() const;
  VkImageView GetImageView(uint32_t index);
  uint32_t GetImageCount() const;
  VkSurfaceCapabilitiesKHR GetSurfaceCapabilities() const;
  void Draw(std::vector<std::unique_ptr<VulkanCommandBuffer>> &commandBuffers);
  uint32_t GetWidth() const;
  uint32_t GetHeight() const;
  VkSwapchainKHR GetSwapchain() const;

private:
  uint32_t m_Width;
  uint32_t m_Height;
  VkSurfaceKHR m_Surface;

  VkFormat m_Format;

  VkColorSpaceKHR m_ColorSpace;

  VkSurfaceCapabilitiesKHR m_SurfaceCapabilities;

  VkPresentModeKHR m_PresentMode;

  VkSwapchainKHR m_Swapchain;

  uint32_t m_ImageCount;

  std::vector<VkImage> m_SwapchainImages;
  std::vector<VkImageView> m_SwapchainImageViews;

  std::shared_ptr<VulkanInstance> m_Instance;
  std::shared_ptr<VulkanDevice> m_Device;

  void CreateWindowSurface(void *window);
  void FindSurfaceInfo();
  void FindPresentMode();
  void CreateSwapChain();
  void CreateSwapchainImageViews();
  VkCompositeAlphaFlagBitsKHR GetSupportedAlphaFlagBits();
};
}
#endif // EZRENDERER_VULKAN_VULKANSWAPCHAIN_H
