//
// Created by nghia on 2/1/17.
//

#ifndef EZRENDERER_VULKAN_VULKANDEVICE_H
#define EZRENDERER_VULKAN_VULKANDEVICE_H

#include "vulkan.h"
#include <memory>

namespace Ez {
class VulkanInstance;
class VulkanDevice {
public:
  /**
   * Create a VkDevice object
   * @param instance
   * @param mainPhysicalDeviceIndex index of the physical device used to create
   * a VkDevice object
   */
  VulkanDevice(std::shared_ptr<VulkanInstance> instance,
               uint32_t mainPhysicalDeviceIndex = 0);

  ~VulkanDevice();

  /**
   *
   * @param queueFamilyIndex
   * @param queueIndex
   * @return A VkQueue
   */
  VkQueue GetDeviceQueue(uint32_t queueFamilyIndex, uint32_t queueIndex);

  VkDevice GetDevice() { return m_Device; }

  VkPhysicalDevice GetPhysicalDevice() { return m_MainPhysicalDevice; };

  /**
   * @param typeBits
   * @param propertyFlags
   * @return index of the memory type if found, -1 if not found
   */
  uint32_t GetMemoryTypeIndex(uint32_t typeBits,
                              VkMemoryPropertyFlags propertyFlags);
  VkPhysicalDeviceFeatures GetFeatures() const;
  VkPhysicalDeviceProperties GetProperties() const;
  VkFormat GetDepthFormat();

private:
  VkPhysicalDevice m_MainPhysicalDevice;

  VkPhysicalDeviceMemoryProperties m_MainPhysicalDeviceMemoryProperties;

  VkPhysicalDeviceFeatures m_MainPhysicalDeviceFeatures;

  VkPhysicalDeviceProperties m_MainPhysicalDeviceProperties;

  VkDevice m_Device;

  std::shared_ptr<VulkanInstance> m_Instance;

  VkFormat m_DepthFormat = VK_FORMAT_UNDEFINED;

  /**
   * Find main physical deivce with a given index
   * If the index exceeds the max physical device count, the first physical
   * device will be chosen
   * @param index
   */
  void SetMainPhysicalDevice(uint32_t index);

  void ChooseDepthFormat();

  /**
   * Create VkDevice object using the main physical device
   */
  void CreateLogicalDevice();
};
}
#endif // EZRENDERER_VULKAN_VULKANDEVICE_H
