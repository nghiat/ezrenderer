//
// Created by nghia on 1/31/17.
//

#ifndef EZRENDERER_VULKAN_VULKAN_H
#define EZRENDERER_VULKAN_VULKAN_H

#if defined(ANDROID) || defined(__ANDROID_API__)
#include "vulkan_wrapper.h"
#else
#include <vulkan/vulkan.h>
#endif
#endif // EZRENDERER_VULKAN_VULKAN_H
