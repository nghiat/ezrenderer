#pragma once
#include "VulkanTexture.h"
#include "vulkan.h"
#include <memory>

namespace Ez {
class VulkanDevice;
class VulkanImagePool;
class VulkanCommandPool;
class VulkanCommandBuffer;
class TextureCube;

class VulkanTextureCube : public VulkanTexture {
public:
  VulkanTextureCube(std::shared_ptr<VulkanDevice> device,
                    std::shared_ptr<VulkanCommandPool> commandPool,
                    TextureCube *texture);
  ~VulkanTextureCube();

private:
  VkDeviceMemory m_ImageMemory;
  uint8_t m_Channels;
  void CopyDataToImage(std::unique_ptr<VulkanCommandBuffer> &commandBuffer,
                       TextureCube *texture);
};
}