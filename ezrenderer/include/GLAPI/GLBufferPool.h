#pragma once

#include "GL.h"
#include <memory>
#include <vector>

#define DEFAULT_BUFFER_SIZE (134217728 / 8)
#define DEFAULT_UNIFORM_BUFFER_SIZE 4194304

namespace Ez {
class PoolTracker1D;

class GLBufferPool {
  friend class GLBuffer;

public:
  GLBufferPool(GLenum target = GL_ARRAY_BUFFER);
  ~GLBufferPool();

private:
  GLenum m_Target;
  std::unique_ptr<PoolTracker1D> m_PoolTracker;
  std::vector<GLuint> m_BigBuffers;
  void AllocateSubBuffer(uint64_t size, void *data, uint64_t &bufferIndex,
                         uint64_t &subBufferOffset);

  void Deallocate(uint64_t bufferIndex, uint64_t subBufferOffset,
                  uint64_t subBufferSize);

  void CreateBuffer(uint64_t size, GLuint &bufferObject);

  void FindBufferSubRegion(uint64_t size, uint64_t &bufferIndex,
                           uint64_t &subBufferOffset);

  GLuint GetBufferObject(uint64_t bufferIndex);

  void CopyToDeviceMemory(uint64_t bufferIndex, uint64_t subBufferOffset,
                          uint64_t dataSize, void *data);
};
}