//===-- GLAPI/GLShader.h - GLShader class definition ------------*- C++ -*-===//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the GLShader class, which helps loading
// GLSL shaders
//===----------------------------------------------------------------------===//

#ifndef EZRENDERER_SHADER_H
#define EZRENDERER_SHADER_H
#include "GL.h"
#include <memory>
#include <vector>

namespace Ez {
class GLBuffer;
/**
 * Shader pass to this class should not have #version preprocessor
 * This class willl add necessary preprocessors to the shaders to preserve cross
 * platform compatibility
 */
class GLShader {
public:
  // Create an empty instance
  GLShader();

  // Create an instance loading vertex and fragment shaders from files
  GLShader(const char *vertexFile, const char *fragmentFile);

  ~GLShader();

  // Load shader from string
  void LoadShaderFromString(GLenum TYPE, const char *shaderString);

  void LoadShaderFromFile(GLenum TYPE, const char *fileName);

  // Link all shaders to create a program
  void LinkProgram();

  void UseProgram() { glUseProgram(m_Program); }

  void StopProgram() { glUseProgram(0); }

  GLuint GetProgram() const { return m_Program; }

  GLuint GetUniform(const char *name);

  /**
   * Bind a GLBuffer object and a uniform block to a same binding point
   */
  void BindBuffer(std::shared_ptr<GLBuffer> buffer,
                  const char *uniformBlockName);

private:
  static GLuint _BindingPoint;
  GLuint m_Program;
  std::vector<GLuint> m_Shaders;
  std::vector<std::shared_ptr<GLBuffer>> m_UniformBuffers;

  // Store found uniform locations
  // std::unordered_map<const char*, GLint> m_Uniforms;
};

#endif // EZRENDERER_SHADER_H
}
