#pragma once

#include "GL.h"
#include <cstdint>
#include <memory>
#include <vector>

namespace Ez {
class Texture;
class GLTexturePool;

class GLTexture {
public:
  GLTexture(std::shared_ptr<GLTexturePool> texturePool, Texture *texture);
  ~GLTexture();
  GLuint GetTexture() const;
  /*
  * Return leftX, bottomY, rightX, topY
  */
  std::vector<float> GetUV() const;

private:
  std::shared_ptr<GLTexturePool> m_TexturePool;
  GLenum m_Format;
  uint32_t m_Width;
  uint32_t m_Height;
  uint64_t m_TextureIndex;
  uint32_t m_OffsetX;
  uint32_t m_OffsetY;
  std::vector<float> m_UV;

  /**
  * Pick the texture format based on the number of channels
  * @param channels number of channels of the texture
  */
  void ChooseFormat(int channels);
};
}
