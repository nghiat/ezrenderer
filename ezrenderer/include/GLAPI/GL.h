#ifndef EZRENDERER_GL_H
#define EZRENDERER_GL_H

#if defined(ANDROID) || defined(__ANDROID_API__)
#include <GLES3/gl3.h>
#else
#include <GL/gl3w.h>
#endif
#endif // EZRENDERER_GL_H
