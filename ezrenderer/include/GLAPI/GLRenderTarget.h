#pragma once

#include "GL.h"

namespace Ez {
// Create class to group framebuffer object, texture object and render buffer
// object
class GLRenderTarget {
public:
  GLRenderTarget(int width, int height, int depth);
  ~GLRenderTarget();
  void Resize(int w, int h);
  void BindFramebuffer();
  void BindColorTexture();
  void UnbindColorTexture();
  int GetWidth();
  int GetHeight();

private:
  int m_Width;
  int m_Height;
  int m_Depth;
  GLuint m_Framebuffer;
  GLuint m_ColorAttachment;
  GLuint m_DepthAttachment;
};
}
