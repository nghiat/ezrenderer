#pragma once

#include "GL.h"
#include <memory>

namespace Ez {
class GLBufferPool;
class GLBuffer {
public:
  GLBuffer(std::shared_ptr<GLBufferPool> bufferPool, uint64_t dataSize,
           void *data);
  ~GLBuffer();
  void UpdateData(uint64_t offset, uint64_t dataSize, void *data);
  GLuint GetBuffer() const;
  uint64_t GetMemoryOffset() const;
  uint64_t GetMemorySize() const;

private:
  std::shared_ptr<GLBufferPool> m_BufferPool;
  uint64_t m_BufferIndex;
  uint64_t m_SubBufferOffset;
  uint64_t m_SubBufferSize;
};
}
