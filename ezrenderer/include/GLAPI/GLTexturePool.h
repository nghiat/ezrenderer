#pragma once

#include "GL.h"
#include "Util/PoolTracker2D.h"
#include <memory>
#include <unordered_map>
#include <vector>

#define DEFAULT_IMAGE_WIDTH 8192
#define DEFAULT_IMAGE_HEIGHT 8192

#define DEFAULT_IMAGE_SIZE 67108864 /*8192*8192*/

namespace Ez {
struct BigTextures {
  std::vector<GLuint> textures;
  // each pair contains width and height of sub texture
  std::vector<std::pair<GLenum, uint32_t>> dimensions;
  std::unique_ptr<PoolTracker2D> poolTracker;
};

class GLTexturePool {
  friend class GLTexture;

public:
  GLTexturePool();
  ~GLTexturePool();
  GLuint GetTexture(GLuint format, uint64_t index);
  uint32_t GetTextureWidth(GLuint format, uint64_t index);
  uint32_t GetTextureHeight(GLuint format, uint64_t index);

private:
  std::unordered_map<GLenum, BigTextures> m_BigTexturesMap;

  void CreateTexture2D(GLenum format, uint32_t width, uint32_t height,
                       GLuint &texture);

  void AllocateSubTexture(GLenum format, uint32_t width, uint32_t height,
                          void *data, uint64_t &textureIndex, uint32_t &offsetX,
                          uint32_t &offsetY);

  void Deallocate(GLenum format, uint64_t textureIndex, uint32_t offsetX,
                  uint32_t offsetY, uint32_t width, uint32_t height);

  void FindSubTextureRegion(GLenum format, uint32_t width, uint32_t height,
                            uint64_t &textureIndex, uint32_t &offsetX,
                            uint32_t &offsetY);

  void CopyDataToTexture(GLenum format, uint64_t textureIndex, uint32_t offsetX,
                         uint32_t offsetY, uint32_t width, uint32_t height,
                         void *data);
};
}
