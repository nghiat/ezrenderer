#pragma once

#include "GL.h"

namespace Ez {
class TextureCube;

class GLTextureCube {
public:
  GLTextureCube(TextureCube *texture);
  ~GLTextureCube();
  GLuint GetTexture() const;

private:
  GLuint m_Texture;

  GLuint m_Format;

  /**
   * Pick the texture format based on the number of channels
   * @param channels number of channels of the texture
   */
  void ChooseFormat(int channels);
};
}
