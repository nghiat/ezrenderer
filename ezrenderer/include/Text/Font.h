#ifndef EZRENDERER_VULKAN_FREETYPELIBRARY_H
#define EZRENDERER_VULKAN_FREETYPELIBRARY_H

#include <ft2build.h>
#include FT_FREETYPE_H
#include <Util/Log.h>
#include <hb-ft.h>
#include <hb.h>

namespace Ez {
class FontLib {
public:
  FontLib();
  ~FontLib();
  FT_Library GetFTLibrary();

private:
  FT_Library m_Library;

private:
  static slog _Logger;
};

class Font {
public:
  Font(std::shared_ptr<FontLib> ftLib, const char *fontPath, uint32_t size,
       uint32_t dpi);
  // TODO FT_New_Memory_Face with safer memory management
  // Font(std::shared_ptr<FontLib> ftLib, )
  ~Font();
  FT_Face GetFace();
  void GetText(std::string s, std::string language = "en",
               hb_script_t script = HB_SCRIPT_COMMON,
               hb_direction_t direction = HB_DIRECTION_LTR);

private:
  std::shared_ptr<FontLib> m_FTLib;
  FT_Face m_Face;
  hb_font_t *m_HbFont;
  static slog _Logger;
};
}

#endif // EZRENDERER_VULKAN_FREETYPELIBRARY_H
