#ifndef EZRENDERER_VULKAN_FREETYPELIBRARY_H
#define EZRENDERER_VULKAN_FREETYPELIBRARY_H

#include <ft2build.h>
#include FT_FREETYPE_H
#include <hb-ft.h>
#include <hb.h>
#include <string>
#include <unordered_map>
#include <vector>

namespace Ez {

struct Glyph {
  uint32_t codepoint;
  uint32_t cluster;
  uint32_t width;
  uint32_t height;
  float xOffset;
  float yOffset;
  float xAdvance;
  float yAdvance;
  std::vector<unsigned char> buffer;
};

class Font {
public:
  Font(const char *fontPath, uint32_t size, uint32_t dpi);
  ~Font();
  std::vector<Glyph> GetGlyphs(std::string text, float x, float y,
                               float maxWidth,
                               hb_direction_t direction = HB_DIRECTION_LTR,
                               hb_script_t script = HB_SCRIPT_LATIN,
                               const char *language = "en");

private:
  static FT_Library _FTLib;
  std::unordered_map<uint32_t, Glyph> m_CachedGlyph;
  FT_Face m_FTFace;
  hb_font_t *m_HBFont;
  hb_buffer_t *m_HBBuffer;
  std::vector<char> m_FileBuffer;
  float m_Height;
  float m_Width;
};
}

#endif // EZRENDERER_VULKAN_FREETYPELIBRARY_H
