//===-- Window/Window.h - Window class definition ---------------*- C++ -*-===//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the Base class, which is the high
// level class to create cross-platform window
//
// Cross-platform window is basically an OOP wrapper of GLFW
//===----------------------------------------------------------------------===//
#pragma once

#ifndef ANDROID
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#else
#include <android_native_app_glue.h>
#endif
#include <chrono>
#include <memory>
#include <string>
#include <vector>

namespace Ez {
class Base {
public:
#ifndef __ANDROID_API__
  enum Mouse { LEFT = GLFW_MOUSE_BUTTON_LEFT, RIGHT = GLFW_MOUSE_BUTTON_RIGHT };
  enum Action { PRESS = GLFW_PRESS, RELEASE = GLFW_RELEASE };
#else
  enum Action { UP = 0, DOWN, MOVE };
#endif
  enum class Renderer { VK, GL, GLES };
  Base(Renderer renderer) : m_Renderer(renderer){};

  Base(Renderer renderer, int width, int height, std::string name);
  virtual ~Base();
  /// Display the window
  /// All update and draw functions run in here
  virtual void Init();
  virtual void Update() = 0;
  virtual void Draw() = 0;
  void Loop();
  bool IsInitDone() { return m_IsInitDone; }

  int GetOpenGLVersionMajor() const { return m_OpenGLVersionMajor; }
  int GetOpenGLVersionMinor() const { return m_OpenGLVersionMinor; }
#ifdef __ANDROID_API__
  void SetAndroidApp(android_app *app) {
    m_App = app;
    m_Window = app->window;
  }
  virtual void TouchCallback(Action action);
#else
  GLFWwindow *GetGLFWwindow();
  virtual void MouseButtonCallback(Mouse button, Action action);
#endif
  virtual void PointerPosCallback(double xPos, double yPos);
  virtual void ScrollCallback(double xOffset, double yOffset);
  virtual void ResizeCallback(int width, int height);

protected:
  Renderer m_Renderer;
  int m_Width = 800;
  int m_Height = 800;
  std::string m_Name = "Untitled";
  int m_OpenGLVersionMajor = 0;
  int m_OpenGLVersionMinor = 0;
  bool m_IsInitDone = false;
  void *m_Window;
  std::chrono::time_point<std::chrono::high_resolution_clock> m_StartTime;
  std::chrono::time_point<std::chrono::high_resolution_clock> m_LastTime;
  std::chrono::time_point<std::chrono::high_resolution_clock> m_CurrentTime;
  template <typename T> uint64_t GetElasedTime();
  template <typename T> uint64_t GetFrameTime();
#if __ANDROID_API__
  android_app *m_App;
#else
  std::shared_ptr<GLFWwindow> m_GLFWWindow;
  const int m_OpenGLVersionListSize = 7;
  const int m_OpenGLVersionList[14] = {3, 3, 4, 0, 4, 1, 4,
                                       2, 4, 3, 4, 4, 4, 5};

  static void GLFWErrorCallback(int error, const char *description);
  void CreateGLFWWindow();
  void SetupGLFWWindowGL();
  void SetupGLFWWindowVK();
#endif
  //  static std::vector<Base*> _windows;
};
}

template <typename T> uint64_t Ez::Base::GetElasedTime() {
  auto deltaTime = std::chrono::duration_cast<T>(
      std::chrono::high_resolution_clock::now() - m_StartTime);
  return deltaTime.count();
}

template <typename T> uint64_t Ez::Base::GetFrameTime() {
  m_CurrentTime = std::chrono::high_resolution_clock::now();
  auto deltaTime = std::chrono::duration_cast<T>(m_CurrentTime - m_LastTime);
  m_LastTime = m_CurrentTime;
  return deltaTime.count();
}

extern Ez::Base *base;
