//===-- Util/PoolTracker1D.h --------------------------------------* C++ -*-===/
//===-----------------------------------------------------------------------===/
//
// This file contains the declaration of the PoolTracker1D class. We use this
// class to save the metadata dynamic allocation of something
//
//===-----------------------------------------------------------------------===/
#pragma once
#include <cstdint>
#include <vector>

namespace Ez {
struct Metadata1D {
  // Vector of acquired regions of something
  // Each pair represents offset and size of each region
  // It should be sorted
  std::vector<std::pair<uint64_t, uint64_t>> acquriedRegions;
  // Alignemnt of the memory
  uint64_t alignment = 1;
};

class PoolTracker1D {
public:
  PoolTracker1D(uint64_t defaultSize);
  ~PoolTracker1D();
  /**
  * Allocate new region
  * @param[in|out] size Size of the region, if the function return false, you
  * have to allocate a new "something" with the size
  * @param[out] index The buffer index
  * @param[out] offset The offset of the region
  * @return false if the region does not fit in existing regions, and we have to
  * create new big region to contian it
  */
  bool AllocateSubRegion(uint64_t &size, uint64_t &index, uint64_t &offset);

  /**
   * We can't set the alignment when create new metadata, so we use this after
   * AllocateSubRegion returns false and create new "something"
   * @param alignment The alignment of new "something"
   * @return false if there is no metadata, true otherwise
   */
  bool SetLastMetadataAlignment(uint64_t alignment);

  void Deallocate(uint64_t index, uint64_t offset, uint64_t size);

private:
  uint64_t m_DefaultSize;
  std::vector<Metadata1D> m_Metadata;
};
}
