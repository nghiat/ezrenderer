#pragma once

#include <array>
#include <assimp/scene.h>
#include <glm/glm.hpp>
#include <vector>

namespace Ez {
struct Vertex {
  glm::vec4 color;
  glm::vec3 pos;
  glm::vec3 normal;
};

struct Node {
  /**
   * Each node has it own transformation matrix
   */
  glm::mat4 modelMatrix;
  /**
   * The scene will contains list of indices for each mesh in a node
   * The variable represent the index in the indices list
   */
  std::vector<uint64_t> indicesListIndices;
};

class Model {
public:
  Model(const char *path);
  ~Model();
  std::vector<Vertex> GetVertices() const;
  std::vector<std::array<std::vector<uint32_t>, 3>> GetIndiciesLists() const;

private:
  /**
   * Load metadata about the scene like number of vertices, indices
   * The metadata is used for the creation of vertices and indices vector
   * (Because the push_back may be expensive for a vertor with a very large
   * size)
   */
  void LoadSceneMetadata(const aiScene *scene);
  void SetupNode(const aiNode *node, glm::mat4 &parentMatrix);
  std::vector<Vertex> m_Vertices;
  /**
   * Assimp saves vertices group in each mesh
   * We save vertices in vector so the indices to each vertex has to add an
   * offset to it
   * E.g. a index refers to a vertex i of mesh n become i + m_IndicesOffset[n]
   * where m_IndicesOffset[n] is sum of the number of vertices from mesh 0 ->
   * n-1
   */
  std::vector<uint64_t> m_IndicesOffset;
  /**
   * Each indicesList contains 3 arrays of indices for 3 draw modes
   * 0: points
   * 1: lines
   * 2: triangles
   */
  std::vector<std::array<std::vector<uint32_t>, 3>> m_IndicesLists;

  std::vector<Node> m_NodeLists;
  uint64_t m_NumVertices = 0;
};
}
