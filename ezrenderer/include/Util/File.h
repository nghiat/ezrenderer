#ifndef EZRENDERER_VULKAN_FILE_H
#define EZRENDERER_VULKAN_FILE_H

#include <string>
#include <vector>

namespace Ez {
class File {
public:
  // All file paths are relative to the assets directory
  static std::vector<char> ReadFile(const char *filePath, bool asBinary = true);

private:
};
}

#endif // EZRENDERER_VULKAN_FILE_H
