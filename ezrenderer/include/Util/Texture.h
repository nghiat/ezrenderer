#pragma once
#include <cstdint>

namespace Ez {
class Texture {
public:
  Texture(const char *path);
  Texture(int width, int height, int channels, unsigned char *data);
  ~Texture();
  int GetX();
  int GetY();

  uint8_t GetChannel();
  unsigned char *GetData();
  float *GetFloatData();

protected:
  int m_X;
  int m_Y;
  int m_Channel;
  unsigned char *m_pData = nullptr;
  // HDR image uses floating point data
  float *m_pfData = nullptr;
  bool m_UseSTB;
};
}
