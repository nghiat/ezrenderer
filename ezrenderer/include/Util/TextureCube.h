#pragma once

#include "Texture.h"

namespace Ez {
class TextureCube : public Texture {
public:
  /**
  *Load texture cube from combined texture
  * This constructor will only suite texture with outline line this
  * | stb origin from here
  * v
  *        ______
  *       |      |
  *       |  +Y  |
  * ______|______|______
  *|      |      |      |
  *|  -X  |  +Z  |  +X  |
  *|______|______|______|
  *       |      |
  *       |  -Y  |
  *       |______|
  *       |      |
  *       |  -Z  |
  *       |______|
  *
  */
  TextureCube(const char *path);
  ~TextureCube();
  float **GetFacesData() const;

private:
  /**
  * Process each face of a combined texture
  * We copy each face of the texture to a separate pointer
  */
  void ProcessTexture();
  float **m_FacesData;
};
}