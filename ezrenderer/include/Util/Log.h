#pragma once

#ifdef ANDROID
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, __VA_ARGS__)
#else
#include <cstdarg>
#include <stdio.h>
enum Priority {
  LEVEL_VERBOSE = 0,
  LEVEL_DEBUG,
  LEVEL_INFORMATION,
  LEVEL_WARNING,
  LEVEL_ERROR
};
inline void log(Priority level, const char *tag, const char *fmt, ...) {
  va_list args;
  switch (level) {
  case LEVEL_VERBOSE:
    printf("*VERBOSE* %s: ", tag);
    break;
  case LEVEL_DEBUG:
    printf("*DEBUG* %s: ", tag);
    break;
  case LEVEL_INFORMATION:
    printf("*INFORMATION* %s: ", tag);
    break;
  case LEVEL_WARNING:
    printf("*WARNING* %s: ", tag);
    break;
  case LEVEL_ERROR:
    printf("*ERROR* %s: ", tag);
    break;
  }
  va_start(args, fmt);
  vprintf(fmt, args);
  printf("\n");
};
#define LOGV(...) log(LEVEL_VERBOSE, __VA_ARGS__)
#define LOGD(...) log(LEVEL_DEBUG, __VA_ARGS__)
#define LOGI(...) log(LEVEL_INFORMATION, __VA_ARGS__)
#define LOGW(...) log(LEVEL_WARNING, __VA_ARGS__)
#define LOGE(...) log(LEVEL_ERROR, __VA_ARGS__)
#endif // ANDROID
