//===-- Util/PoolTracker2D.h --------------------------------------* C++ -*-===/
//===-----------------------------------------------------------------------===/
//
// This file contains the declaration of the PoolTracker2D class. We use this
// class to save the metadata dynamic allocation of 2 dimentional memory (like
// big texture)
//
//===-----------------------------------------------------------------------===/

#pragma once
#include <glm/vec2.hpp>
#include <vector>

namespace Ez {
struct Region2D {
  uint64_t size; /*width * height precalculated*/
  uint32_t offsetX;
  uint32_t offsetY;
  uint32_t width;
  uint32_t height;
};

struct Metadata2D {
  // Vector of free regions (top->bottom, left->right)
  std::vector<Region2D> freeRegions;
  uint64_t alignment = 1;
};

class PoolTracker2D {
public:
  PoolTracker2D(uint32_t defaultWidth, uint32_t defaultHeight);
  ~PoolTracker2D();
  bool AllocateSubRegion(uint64_t &index, uint32_t &offsetX, uint32_t &offsetY,
                         uint32_t &width, uint32_t &height);
  /**
  * We can't set the alignment when create new metadata, so we use this after
  * AllocateSubRegion returns false and create new "something"
  * @param alignment The alignment of new "something"
  * @return false if there is no metadata, true otherwise
  */
  bool SetLastMetadataAlignment(uint64_t alignment);
  void Deallocate(uint64_t index, uint32_t offsetX, uint32_t offsetY,
                  uint32_t width, uint32_t height);
  ;

private:
  uint32_t m_DefaultWidth;
  uint32_t m_DefaultHeight;
  std::vector<Metadata2D> m_Metadata;
  void SplitRegions(std::vector<Region2D> &regions, Region2D bigRegion,
                    Region2D smallRegion);
};
}
