//
// Created by nghia on 1/19/17.
//

#ifndef ANDROID_VULKAN_JNIHELPER_H
#define ANDROID_VULKAN_JNIHELPER_H

#include "android_native_app_glue.h"
#include <string>
#include <vector>

class JNIHelper {
public:
  static JNIHelper *Get();
  void Init(android_app *app);
  void SetAssetManager(AAssetManager *manager);
  std::vector<char> ReadAssetFile(const char *path, bool asBinary);
  AAssetManager *GetAssetManager() { return m_AssetManager; }

private:
  JNIHelper();
  static JNIHelper *_JNIHelper;
  android_app *m_AndroidApp;
  AAssetManager *m_AssetManager = nullptr;
};

#endif // ANDROID_VULKAN_JNIHELPER_H
