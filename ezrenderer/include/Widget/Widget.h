//===-- Widget/Widget.h - Widget class definition ---------------*- C++ -*-===//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the Widget class, which is the base
// abstract class for all UI elements
//
//===----------------------------------------------------------------------===//

#ifndef EZRENDERER_WIDGET_H
#define EZRENDERER_WIDGET_H
#include <vector>

namespace Ez {

// Widget coordinates take origin at top left corner and positive x and y axes
// point right and down
class Widget {
public:
  Widget();
  Widget(int topLeftX, int topLeftY, int width, int height);

  int GetTopLeftX() const;
  void SetTopLeftX(int GetTopLeftX);

  int GetTopLeftY() const;
  void SetTopLeftY(int GetTopLeftY);

  int GetWidth() const;
  void SetWidth(int GetWidth);

  int GetHeight() const;
  void SetHeight(int GetHeight);

private:
  int m_TopLeftX;
  int m_TopLeftY;
  int m_Width;
  int m_Height;
  std::vector<Widget *> _Widgets;
  virtual void Draw() = 0;
  virtual void Update() = 0;
  virtual bool IsInside(int x, int y);
};
}

#endif // EZRENDERER_WIDGET_H
