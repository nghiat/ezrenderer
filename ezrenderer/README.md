EzRenderer is the rendering library used to easily create graphics application on multiple platforms

EzRenderer currently supports OpenGL(ES) and Vulkan on Windows, Linux, and Android

Status:

| API       | Windows        | Linux | Android |
|-----------|----------------|-------|---------|
| OpenGL    |        v       |   v   |    x    |
| OpenGL ES |        x       |   x   |    v    |
| Vulkan    | v (not tested) |   v   |    v    |

Requirements:
- Cmake (> 3.2)

- OpenGL > 3.3 (pretty much any recent graphics card supports it)

- OpenGL ES 3.0 (recent andorid phones)

- Vulkan:

    - On Windows, only recent graphics cards from NVIDIA and AMD with supported drivers are supported

    - On Linux, only recent graphics cards from NVIDIA and AMD with proprietary drivers and Intel HD
   from Haswell processor or later are supported

    - LunarG SDK are required on both Windows and Linux (https://vulkan.lunarg.com)

    - On Android, only android N with appropriate GPU and driver are supported (Only adrendo 530 from Snapdragon 820 
    is supported right now)
  
Compile:

- Use Cmake to compile project

~~- Cmake options:~~

~~  - GL_BACKEND: Use OpenGL, OpenGL ES as rendering API~~

~~  - VK_BACKEND: Use Vulkan as rendering API~~

TODO:

- Add sample project

- Abstract OpenGL and Vulkan

- Create UI framework

...