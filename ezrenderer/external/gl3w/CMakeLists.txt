cmake_minimum_required(VERSION 3.1.3 FATAL_ERROR)

project(gl3w)

set(CMAKE_VERBOSE_MAKEFILE false)
set(CMAKE_POSITION_INDEPENDENT_CODE ON) # -fPIC

set(SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/src)
set(HEADER_DIR ${CMAKE_CURRENT_LIST_DIR}/include)

set(HEADER_FILES
	"${HEADER_DIR}/GL/gl3w.h"
	"${HEADER_DIR}/GL/glcorearb.h"
)

set(SOURCE_FILES
	"${SOURCE_DIR}/gl3w.c"
)

# add and depend on OpenGL
find_package(OpenGL REQUIRED)
set(EXTERNAL_INCLUDE_DIRS ${EXTERNAL_INCLUDE_DIRS} ${OPENGL_INCLUDE_DIR})
set(EXTERNAL_LIBRARIES ${EXTERNAL_LIBRARIES} ${OPENGL_LIBRARIES})

# add command to create the header and source files
add_custom_command(
	OUTPUT
		"${SOURCE_DIR}/gl3w.c"
		"${HEADER_DIR}/GL/gl3w.h"
		"${HEADER_DIR}/GL/glcorearb.h"
	COMMAND python ${CMAKE_CURRENT_LIST_DIR}/gl3w_gen.py
	DEPENDS ${CMAKE_CURRENT_LIST_DIR}/gl3w_gen.py
	WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
)

# add pseudo target that depends on the generated files
#add_custom_target(
#	gl3w_gen ALL
#	DEPENDS
#		"${SOURCE_DIR}/gl3w.c"
#		"${HEADER_DIR}/GL/gl3w.h"
#		"${HEADER_DIR}/GL/glcorearb.h"
#)

# create gl3w target
add_library(gl3w
    "${SOURCE_DIR}/gl3w.c"
    "${HEADER_DIR}/GL/gl3w.h"
    "${HEADER_DIR}/GL/glcorearb.h"
)

# make gl3w target depend on the generator target
#add_dependencies(gl3w gl3w_gen)

# let remote project know about source and header files
#target_sources(gl3w INTERFACE ${SOURCE_FILES})
target_include_directories(gl3w PUBLIC
	$<BUILD_INTERFACE:${HEADER_DIR}>
	$<INSTALL_INTERFACE:include>
)
target_include_directories(gl3w INTERFACE ${EXTERNAL_INCLUDE_DIRS})
# let remote project know which libraries need to be linked
if(UNIX)
target_link_libraries(gl3w INTERFACE ${EXTERNAL_LIBRARIES} dl)
elseif(WIN32)
target_link_libraries(gl3w INTERFACE ${EXTERNAL_LIBRARIES})
endif()

set(MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
set(BUILD_CMAKE_DIR "${CMAKE_CURRENT_BINARY_DIR}/cmake")

# export targets for remote projects (i.e. make find_package(gl3w) work)
configure_file(
	"${MODULE_PATH}/gl3w-config.cmake"
	"${BUILD_CMAKE_DIR}/gl3w-config.cmake"
	COPYONLY
)

export(
	TARGETS gl3w
	FILE "${BUILD_CMAKE_DIR}/gl3w-targets.cmake"
)

export(PACKAGE gl3w)
