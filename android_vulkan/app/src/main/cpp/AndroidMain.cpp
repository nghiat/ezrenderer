// Copyright 2016 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDItIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "App.h"
#include "Util/JNIHelper.h"
#include "VulkanAPI/vulkan.h"
#include <android/log.h>
#include <android_native_app_glue.h>

Ez::Base *base;
// Process the next main command.
void handle_cmd(android_app *app, int32_t cmd) {
  switch (cmd) {
  case APP_CMD_INIT_WINDOW: {
    // The window is being shown, get it ready.
    JNIHelper::Get()->Init(app);
    auto width = ANativeWindow_getWidth(app->window);
    auto height = ANativeWindow_getHeight(app->window);
    base = new App(width, height, "EzRenderer Vulkan");
    if (InitVulkan()) {
      base->SetAndroidApp(app);
      base->Init();
    }
    break;
  }
  case APP_CMD_TERM_WINDOW:
    // The window is being hidden or closed, clean it up.

    break;
  default:
    __android_log_print(ANDROID_LOG_INFO, "App", "event not handled: %d", cmd);
  }
}

int32_t handle_event(struct android_app *app, AInputEvent *event) {
  int32_t eventType = AInputEvent_getType(event);
  switch (eventType) {
  case AINPUT_EVENT_TYPE_MOTION:
    switch (AInputEvent_getSource(event)) {
    case AINPUT_SOURCE_TOUCHSCREEN:
      int action = AKeyEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK;
      switch (action) {
      case AMOTION_EVENT_ACTION_DOWN:
        base->TouchCallback(Ez::Base::DOWN);
        return 1;
        break;
      case AMOTION_EVENT_ACTION_UP:
        base->TouchCallback(Ez::Base::UP);
        return 1;
        break;
      case AMOTION_EVENT_ACTION_MOVE:
        base->PointerPosCallback(AMotionEvent_getX(event, 0),
                                 AMotionEvent_getY(event, 0));
        return 1;
        break;
      }
    }
  }
  return 0;
}

void android_main(struct android_app *app) {
  // Magic call, please ignore it (Android specific).
  app_dummy();

  // Set the callback to process system events
  app->onAppCmd = handle_cmd;
  app->onInputEvent = handle_event;
  // Used to poll the events in the main loop
  int events;
  android_poll_source *source;

  // Main loop
  do {
    if (ALooper_pollAll((base && base->IsInitDone()) ? 1 : 0, nullptr, &events,
                        (void **)&source) >= 0) {
      if (source != NULL)
        source->process(app, source);
    }

    // render if vulkan is ready
    if (base && base->IsInitDone()) {
      base->Draw();
      base->Update();
    }
  } while (app->destroyRequested == 0);
}
