#pragma once

#include "UniformBufferObject.h"
#include "VulkanAPI/VulkanImagePool.h"
#include <Base/Base.h>
#include <VulkanAPI/VulkanBuffer.h>
#include <VulkanAPI/VulkanBufferPool.h>
#include <VulkanAPI/VulkanCommandPool.h>
#include <VulkanAPI/VulkanDescriptorSet.h>
#include <VulkanAPI/VulkanDevice.h>
#include <VulkanAPI/VulkanGraphicsPipeline.h>
#include <VulkanAPI/VulkanInstance.h>
#include <VulkanAPI/VulkanRenderTarget.h>
#include <VulkanAPI/VulkanShader.h>
#include <VulkanAPI/VulkanSwapchain.h>
#include <VulkanAPI/VulkanTextureCube.h>
#include <memory>

class App : public Ez::Base {

public:
  App(int width, int heigth, std::string title)
      : Ez::Base(Ez::Base::Renderer::VK, width, heigth, title){};
  ~App();

  void Init() override;
  void Update() override;
  void Draw() override;

  void InitTexture();
  void InitShaders();
  void InitObjects();
  void InitPipelines();

  void DrawSkybox(VkCommandBuffer commandBuffer);
  void DrawModel(VkCommandBuffer commandBuffer);
  void DownSample4x(VkCommandBuffer commandBuffer);
  void ExtractHightlight(VkCommandBuffer commandBuffer);
  void Downsample2x(VkCommandBuffer, int level);
  void TempBlur(VkCommandBuffer, int level);
  void Blur(VkCommandBuffer, int level);
  void GaussianCompose(VkCommandBuffer cb);
  void GlareCompose(VkCommandBuffer cb);
  void ToneMapping(VkCommandBuffer cb);
  void UpdateModelUniforms();
  void DrawQuad(VkCommandBuffer &commandBuffer) const;

  void TouchCallback(Action action) override;
  void PointerPosCallback(double xPos, double yPos) override;
  void ScrollCallback(double xOffset, double yOffset) override;
  void ResizeCallback(int width, int height) override;

private:
  std::shared_ptr<Ez::VulkanInstance> m_Instance;
  std::shared_ptr<Ez::VulkanDevice> m_Device;
  std::shared_ptr<Ez::VulkanSwapchain> m_Swapchain;
  std::shared_ptr<Ez::VulkanCommandPool> m_CommandPool;
  std::shared_ptr<Ez::VulkanBufferPool> m_BufferPool;
  std::shared_ptr<Ez::VulkanBufferPool> m_UniformBufferPool;
  std::unique_ptr<Ez::VulkanTextureCube> m_SkyboxTexture;

  ModelVertUBO m_ModelVertUBO;
  ModelFragUBO m_ModelFragUBO;
  BlurFragUBO m_BlurFragUBO;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_ModelShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_SkyboxShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_DownsampleShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_Downsample4xShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_ExtractHLShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_BlurShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_GaussianComposeShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_GlareComposeShaders;
  std::vector<std::shared_ptr<Ez::VulkanShader>> m_ToneMappingShaders;

  glm::vec3 m_Eye, m_CenterLook, m_Up, m_RotationAxis1, m_RotationAxis2;
  glm::mat4 m_ModelMatrix, m_ViewMatrix, m_PerspectiveMatrix, m_MVPMatrix;

  bool m_IsMouseClicked = false;
  double m_MouseX = 0.0;
  double m_MouseY = 0.0;
  float m_Exposure = 5.f;
  uint8_t m_NumModelsRow = 8;
  uint8_t m_NumModelsCol = 8;
  // 3.0f / bloom width
  float m_S[4] = {0.42f, 0.25f, 0.15f, 0.10f};
  SkyboxVertUBO m_SkyboxVertUBO;
  Downsample4xVertUBO m_Downsample4xVertUBO;
  ExtractHightLightFragUBO m_ExtractHightLightFragUBO;
  GaussianFragUBO m_GaussianFragUBO;
  GlareFragUBO m_GlareFragUBO;
  ToneMappingFragUBO m_ToneMappingFragUBO;

  std::unique_ptr<Ez::VulkanBuffer> m_ModelBuffer;
  std::vector<std::unique_ptr<Ez::VulkanBuffer>> m_ModelIndexBuffers;
  // Keep number of vertices of each index buffer after deleting the model
  std::vector<size_t> m_ModelIndicesNums;

  std::unique_ptr<Ez::VulkanBuffer> m_SkyboxVertexBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_SkyboxIndexBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_ModelVertUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_ModelFragUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_SkyboxVertUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_Downsample4xVertUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_ExtractHightLightFragUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_GaussianFragUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_GlareFragUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_ToneMappingFragUBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_QuadBuffer;
  std::unique_ptr<Ez::VulkanBuffer> m_QuadUVBuffer;

  std::uint32_t m_NumLevels = 4;
  std::unique_ptr<Ez::VulkanRenderTarget> m_MainRT;
  std::unique_ptr<Ez::VulkanRenderTarget> m_SceneRT;
  std::unique_ptr<Ez::VulkanRenderTarget> m_FinalRT;
  std::unique_ptr<Ez::VulkanRenderTarget> m_Downsample4xRT;
  std::vector<std::unique_ptr<Ez::VulkanRenderTarget>> m_ComposeRTs;
  std::vector<std::unique_ptr<Ez::VulkanRenderTarget>> m_BlurRTs;
  std::vector<std::unique_ptr<Ez::VulkanRenderTarget>> m_TempRTs;
  std::unique_ptr<Ez::VulkanRenderTarget> m_GaussianRT;

  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_SkyboxPipeline;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_ModelPipeline;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_Downsample4xPipeline;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_ExtractHightLightPipeline;
  std::vector<std::unique_ptr<Ez::VulkanGraphicsPipeline>> m_TempBlurPipelines;
  std::vector<std::unique_ptr<Ez::VulkanGraphicsPipeline>> m_BlurPipelines;
  std::vector<std::unique_ptr<Ez::VulkanGraphicsPipeline>>
      m_DownsamplePipelines;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_GaussianComposePipeline;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_GlareComposePipeline;
  std::unique_ptr<Ez::VulkanGraphicsPipeline> m_ToneMappingPipeline;
  ;

  std::shared_ptr<Ez::VulkanDescriptorSet> m_SkyboxDescriptorSet;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_ModelDescriptorSet;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_Downsample4xDescriptorSet;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_ExtractHightLightDescriptorSet;
  std::vector<std::shared_ptr<Ez::VulkanDescriptorSet>>
      m_TempBlurDescriptorSets;
  std::vector<std::shared_ptr<Ez::VulkanDescriptorSet>> m_BlurDescriptorSets;
  std::vector<std::shared_ptr<Ez::VulkanDescriptorSet>>
      m_DownsampleDescriptorSets;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_GaussianComposeDescriptorSet;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_GlareComposeDescriptorSet;
  std::shared_ptr<Ez::VulkanDescriptorSet> m_ToneMappingDescriptorSet;
};
