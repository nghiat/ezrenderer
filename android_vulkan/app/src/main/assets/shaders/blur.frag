#version 450
#extension GL_ARB_separate_shader_objects : enable
layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (std140, push_constant) uniform UBlurFrag {
    vec2 uDir;
    float uResolution;
    float uRadius;
    float uS;
} ubo;

layout (binding = 1) uniform sampler2D uSampler;

float gaussian(float x, float sd){
	return exp(-(x*sd) * (x*sd));
}

void main(){
	vec4 sum = vec4(0.0);
	float blur = 1 / ubo.uResolution;
	float hstep = ubo.uDir.x;
	float vstep = ubo.uDir.y;
	float gauSum = 0.0;
	for(int i = 0; i < 2 * ubo.uRadius + 1; i++){
		gauSum += gaussian(i - ubo.uRadius, ubo.uS);
	}
    outColor = vec4(sum.rgb, 1.0);
    sum+= texture(uSampler, fragUV) * gaussian(0, ubo.uS) / gauSum;
    for(int i = 1; i <= ubo.uRadius; i++){
    	sum += texture(uSampler, vec2(fragUV.x - i*blur*hstep, fragUV.y - i*blur*vstep)) * gaussian(i, ubo.uS) / gauSum;
    	sum += texture(uSampler, vec2(fragUV.x + i*blur*hstep, fragUV.y + i*blur*vstep)) * gaussian(i, ubo.uS) / gauSum;
    }
    outColor = vec4(sum.rgb, 1.0);
}
