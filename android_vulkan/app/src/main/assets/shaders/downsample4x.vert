#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aUV;

layout (std140, binding = 0) uniform UDownsample4xVert {
    vec2 uTexelSize;
} ubo;

layout (location = 0) out vec2 fragUV[4];

void main(){
	fragUV[0] = aUV;
	fragUV[1] = aUV + vec2(ubo.uTexelSize.x, 0.0);
	fragUV[2] = aUV + vec2(ubo.uTexelSize.x, ubo.uTexelSize.y);
	fragUV[3] = aUV + vec2(0.0, ubo.uTexelSize.y);
	gl_Position = vec4(aPos, 1.0);
}
