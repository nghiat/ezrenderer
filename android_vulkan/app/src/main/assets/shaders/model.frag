#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (std140, binding = 1) uniform UModelFrag {
    vec4 uPhong;
    vec3 uEye;
    float uFactor;
    vec3 uLightPos;
    int uMode;
    vec3 uLightIntensities; //light color
    int uEvMapping;
} ubo;

layout(binding = 2) uniform samplerCube uSkybox;

layout (location = 0) in IFrag {
    vec3 fragPos;
    vec3 fragNormal;
    vec4 fragColor;
    vec3 fragReflect;
    vec3 fragRefract;
};

layout (location = 0) out vec4 outColor;

float my_fresnel(vec3 I, vec3 N, float power,  float scale,  float bias)
{
    return max(0.0, min(1, bias + scale * pow(1.0 + dot(I, N), power)));
}

void main(){
	vec3 reflectColor = texture(uSkybox, fragReflect).rgb;
	vec3 refractColor = texture(uSkybox, fragRefract).rgb;
	vec3 tranformedVector = fragNormal;
	vec3 surfaceToLight = normalize(fragPos - ubo.uLightPos);
	vec3 surfaceToEye = normalize(fragPos - ubo.uEye);
	float specular = pow(max(0.0, -dot(reflect(surfaceToLight, tranformedVector), surfaceToEye)), ubo.uPhong.w) * ubo.uPhong.z;
	float diffuse = max(0.0,dot(surfaceToLight, tranformedVector)) * ubo.uPhong.y;
	float ambient = ubo.uPhong.x;
	if(ubo.uEvMapping == 0 || ubo.uMode == 0){
		outColor = vec4(fragColor.rgb * (diffuse + ambient) + specular * ubo.uLightIntensities, fragColor.a);
	}
	else{
		switch (ubo.uMode){
			case 1: outColor = vec4(mix(fragColor.rgb, reflectColor, ubo.uFactor), 1.0); break;
			case 2: outColor = vec4(mix(fragColor.rgb, refractColor, ubo.uFactor), 1.0); break;
			case 3: outColor = vec4(mix(refractColor, reflectColor, my_fresnel(surfaceToLight, tranformedVector, 5.0, 1.0, 0.1)), 1.0); break;
		}
	}
}
